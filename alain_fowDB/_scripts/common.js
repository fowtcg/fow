$(document).ready(function () {
	
	// BIND HIDE FUNCTION TO SECTION TITLES
	//--------------------------------------------------------------
	$(".sections").each( function() {
		var hide = $(".hide", this),
				content = $(".content", this);
		if (hide) {
			hide.on("click", function () {
				content.toggleClass("hidden");
			});
		}
	});
	
	
	// SPOILER - HIDE SETS
	//--------------------------------------------------------------
	$(".spoiler").each(function () {
		var hide = $(".spoiler_hide", $(this));
		if (hide) {
			hide.bind("click", function () {
				$(".spoiler_content", $(this).parent().parent()).toggleClass("hidden");
			});
		}
	});
	
	
	// CHANGE LANGUAGE
	//--------------------------------------------------------------
	$(".lang").on("click", function() {
		var lang_previous = $(".langSelect").first().data("lang"),
				lang_selected = $(this).data("lang"),
				url = window.location.href,
				url_hash = "";
		
		// If url has hashes, save them for later, then delete them
		if (url.indexOf("#") > 0) {
			url_hash = url.substring(url.indexOf("#"));
			url = url.replace(url_hash, "");
		}
		
		// Change language only if the new language is different from the previous
		if (lang_previous != lang_selected) {
			// If URL already has a lang parameter, change it
			if (url.indexOf("lang=") > 0) {
				url = url.replace("lang=" + lang_previous, "lang=" + lang_selected);
			}
			// Else, add it to URL
			else {
				// If no parameters, add lang as first one
				if (url.indexOf("?") > 0) {
					url = url + "&lang=" + lang_selected
				}
				else {
					url = url + "?lang=" + lang_selected
				}
			}
			
			// Change url, PHP will get the lang parameter and change the language
			window.location.href = url + url_hash;
		}
	});
	
	
	// CLOSE NOTIFICATION - ACCEPT COOKIES
	//--------------------------------------------------------------
	$(".notification_close").on("click", function (e) {
		e.preventDefault();
		
		$(this).parent().remove();
		$.post(
			"_configuration/accept_cookies.php",
			{
				"action": "accept cookies"
			}
		);
	});
});