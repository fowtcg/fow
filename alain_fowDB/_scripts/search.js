$(document).ready(function () {
	
	// Language
	//--------------------------------------------------------------
	var results = "results",
			numxrow_error = "numxrow_error";
	
	switch ($(".langSelect")[0].innerHTML) {
		case 'English':
			results = 'Results';
			numxrow_error = "Don't you think numxrow cards per row are too much?";
			break;
		case 'Italiano':
			results = 'Risultati';
			numxrow_error = "Non credi che numxrow carte per riga siano troppe?";
			break;
	}
	
	// Prevent empty search form inputs to be sent
	//--------------------------------------------------------------
	$("#form_search").on("submit", function () {
		if ($("input[name='atk']").val() == "") {
			$("select[name='atk-operator']").remove();
		}
		if ($("input[name='def']").val() == "") {
			$("select[name='def-operator']").remove();
		}
		if ($("select[name='setcode']").val() == "0") {
			$("select[name='setcode']").remove();
		}
		$("input", $(this)).each(function () {
			if (!$(this).val()) {
				$(this).remove();
			}
		});
	});
	
	
	// Hide filters as default when viewer is empty
	//--------------------------------------------------------------
	if ($(".card").length > 0) {
		$("#filters .hide").trigger("click");
		if (window.innerWidth < 752 /*47em*/) {
			$("#options .hide").trigger("click");
		}
	}
	
	
	// Show/Hide syntax help
	//--------------------------------------------------------------
	$("#help").on("click", function () {
		var syntax = $("#syntax");
		switch ( syntax.css("display") ) {
			case 'none':
				syntax.css("display", "block");
				break;
			case 'block':
				syntax.css("display", "none");
				break;
		}
	});
	
	
	// Select checkbox when clicking on adjacent element
	//--------------------------------------------------------------
	$(".checklabel").on("click", function () {
		var checkbox = $(this).prev();
		checkbox.trigger("click");
	});
	
	
	// FORM RESET
	//--------------------------------------------------------------
	$("#form_reset").on("click", function () {
		
		// Reset format
		$("input[name='format']").each(function () {
			$(this).val() == 'New Frontiers' ? $(this).prop("checked", true) : $(this).prop("checked", false);
		});
		
		// Reset selected set
		$("option", $("select[name='setcode']")).each(function () {
			$(this).val() == '0' ? $(this).prop("selected", true) : $(this).prop("selected", false);
		});
		//$("option[name='set_default']").prop("checked", true);
		
		// Reset checkboxes
		$("input[type='checkbox']", $("#form_search")).prop("checked", false);
		
		// Reset inputs
		$("input[type='text']").val('');
		$("input[type='number']").val('');
		
		// Reset ATK and DEF
		$("option", $("select[name='atk-operator']")).each(function () {
			$(this).val() == 'equals' ? $(this).prop("selected", true) : $(this).prop("selected", false);
		});
		$("option", $("select[name='def-operator']")).each(function () {
			$(this).val() == 'equals' ? $(this).prop("selected", true) : $(this).prop("selected", false);
		});
		
		// Reset order by
		$("option", $("select[name='orderby']")).each(function () {
			$(this).val() == 'setnum' ? $(this).prop("selected", true) : $(this).prop("selected", false);
		});
	});
	
	
	// OPTION - CHECK FOR SPOILER
	//--------------------------------------------------------------
	var spoiler = false;
	if ($("#viewer .title")[0].innerHTML.indexOf("Spoiler") < 0) {spoiler = true;}
	
	
	// OPTION - CARDS PER ROW
	//--------------------------------------------------------------
	
	// Change cards per row when clicking (-)
	$("#opt_b_numxrow_minus").on("click", function () {
		var numxrow_input = parseInt($("#opt_i_numxrow").val()) - 1;
		if (numxrow_input > 0) {
			$("#opt_i_numxrow").val(numxrow_input);
		}
		else {
			$("#opt_i_numxrow").val(1);
		}
		fitCards();
	});
	
	// Change cards per row when clicking (+)
	$("#opt_b_numxrow_plus").on("click", function () {
		var numxrow_input = $("#opt_i_numxrow").val();
		$("#opt_i_numxrow").val( parseInt(numxrow_input) + 1 );
		fitCards();
	});

	// Change cards per row when hitting Enter key
	$('#opt_i_numxrow').keyup(function (e) {
     if (e.keyCode == 13) {
			fitCards();
		} 
  });
	

	// Fit cards on screen after a search
	//--------------------------------------------------------------
	if ($("#viewer").length > 0) {
		fitCards();
	}
	
	
	// OPTION - INFO
	//--------------------------------------------------------------
	
	$("#opt_i_info").click(function () {
		if ($(this).is(":checked")) {
			
			// Clear other inputs
			if ($("#opt_i_select").is(":checked")) {
				$("#opt_i_select").trigger("click");
			}
			
			$(".card").each(function() {
				$(this).addClass("pointer"); // Show pointer
				$(this).bind("click", function () {
					var code = $("img", $(this)).data("code").replace(" ", "+"),
					url = window.location.href.substring(0, window.location.href.indexOf("?"));
					url += "?p=card" + "&code=" + code;
					window.open(url, "_blank");
				});
			});
		}
		else if ($(this).is(":not(:checked)")) {
			$(".card").each(function() {
				$(this).removeClass("pointer"); // Show pointer
				$(this).unbind("click");
			});
		}
	});
	
	
	
	// OPTION - ZOOM
	//--------------------------------------------------------------
	
	$("#opt_i_zoom").click(function () {
		if ($(this).is(":checked")) {
			
			// Clear other inputs
			if ($("#opt_i_info").is(":checked")) {
				$("#opt_i_info").trigger("click");
			}
			if ($("#opt_i_select").is(":checked")) {
				$("#opt_i_select").trigger("click");
			}
			
			$(".card").each(function() {
				$(this).addClass("pointer"); // Show pointer
				var image_path = $("img", $(this)).attr("src").replace("thumbs", "cards"),
						cardname = $("img", $(this)).attr("alt");
			$("a", $(this)).attr({
					"href": image_path,
					"data-lightbox": "cards",
					"data-title": cardname
				});
			});
		}
		else if ($(this).is(":not(:checked)")) {
			$(".card").each(function() {
				$("a", $(this)).removeAttr("href");
				$("a", $(this)).removeAttr("data-lightbox");
				$("a", $(this)).removeAttr("data-title");
			});
		}
	});
	
	// Enable as default
	$("#opt_i_zoom").trigger("click");
	
	
	// OPTION - MISSING CARDS (SPOILER)
	//--------------------------------------------------------------
	$("#opt_i_missing").click(function () {
		if ($(this).is(":checked")) {
			$(".card_missing").show();
		}
		else {
			$(".card_missing").hide();
		}
	});
	
	
	// OPTION - SELECTION
	//--------------------------------------------------------------
	
	$("#opt_i_select").click(function () {
		
		// Selection activated
		if ($(this).is(":checked")) {
			
			// Clear other inputs
			if ($("#opt_i_info").is(":checked")) {
				$("#opt_i_info").trigger("click");
			}
			
			// Select a card when clicked
			$(".card").each(function () {
				$(this).addClass("pointer"); // Show pointer
				$(this).bind("click", function () {
					$(this).toggleClass("selected");
				});
			});
			
			// Buttons
			// -----------------------------
			
			// Select all
			$("#opt_b_select_selAll").bind("click", function () {
				$(".card").addClass("selected");
			});
			
			// Deselect all
			$("#opt_b_select_deselAll").bind("click", function () {
				$(".card").removeClass("selected");
			});
			
			// Show only selected
			$("#opt_b_select_showSel").bind("click", function () {
				$(".card:not(.selected)", $("#viewer .content")).hide();
				if (!spoiler) {
					$("#viewer .title")[0].innerHTML = results + " (" + $(".selected").length + ")";
				}
			});
			
			// Show all
			$("#opt_b_select_showAll").bind("click", function () {
				$(".card").show();
				if (!spoiler) {
					$("#viewer .title")[0].innerHTML = results + " (" + $(".card").length + ")";
				}
			});
			
			// Custom search
			$("#opt_b_select_custom").bind("click", function () {
				
				// Get language and url of index.php
				var lang = $(".langSelect")[0].innerHTML.toLowerCase(),
						url = window.location.href.substring(0, window.location.href.indexOf("?"));
		
				// Add initial parameters to url
				url += "?" + "p=customsearch";
				
				$(".selected img").each(function () {
					url += "&id[]=" + $(this).data("id");
				});
				
				window.open(url, '_blank');
			});
		}
		
		// Selection deactivated
		else if ($(this).is(":not(:checked)")) {
			
			// Show hidden cards
			$(".card:not(.selected)", $("#viewer .content")).show();
			
			// Deselect every card
			$(".card").each(function () {
				$(this).removeClass("pointer"); // Show pointer
				if ($(this).hasClass("selected")) {
					$(this).removeClass("selected");
				}
				// Remove select-on-click ability
				$(this).unbind("click");
			});
			
			// Deactivate buttons
			$("#opt_b_select_selAll").unbind("click");
			$("#opt_b_select_deselAll").unbind("click");
			$("#opt_b_select_showSel").unbind("click");
			$("#opt_b_select_showAll").unbind("click");
			if (!spoiler) {
					$("#viewer .title")[0].innerHTML = results + " (" + $(".card").length + ")";
			}
		}
	});
	
	
	// ==============================
	// FUNCTIONS
	// ==============================
	
	// Fit the cards per row
	//--------------------------------------------------------------
	function fitCards() {
		
		var viewer = $("#viewer").css("width").substring(0, $("#viewer").css("width").length - 2);
				card = 200, // Arbitrary dimension of readable card in pixel
				numxrow_input = new Number($("#opt_i_numxrow").val());
				numxrow = Math.floor(viewer/card);
				
		// If input is present, save it as numxrow. If not, print default numxrow into input
		numxrow_input > 0 ? numxrow = numxrow_input : $("#opt_i_numxrow").val(numxrow);
		
		// Calculate card width
		var width = 100 / numxrow;

		if (numxrow > 20) {
			// Too many cards per row
			alert(numxrow_error.replace("numxrow", numxrow));
			numxrow = 20;
			$("#opt_i_numxrow").val(numxrow);
		}
		else {
			// Apply width to cards
			$(".card").css("width", width + "%");
		}
	}
});