$(document).ready(function () {
	
	// CALCULATE
	// ------------------------------------------------------------
	$("#hypcalc_calculate").on("click", function () {
		var pop_n = parseInt($("#pop_n").val()),
				pop_k = parseInt($("#pop_k").val()),
				smp_n = parseInt($("#smp_n").val()),
				smp_k = $("#smp_k").val();
		
		// k with hyphen
		if (smp_k.indexOf("-") > -1) {
			var k1 = parseInt( smp_k.substring( 0, smp_k.indexOf("-") ) ),
					k2 = parseInt( smp_k.substring( smp_k.indexOf("-") + 1 ) );
					
			$("#hypcalc_output").html(((hdf(pop_n,pop_k,smp_n,k2,true) - hdf(pop_n,pop_k,smp_n,k1,true) + hdf(pop_n,pop_k,smp_n,k1,false))*100).toFixed(2) + "%");
		}
		// k without hyphen
		else {
			smp_k = parseInt(smp_k);
			$("#hypcalc_output").html( (hdf(pop_n,pop_k,smp_n,smp_k,false)*100).toFixed(2) + "%");
		}
	});
	
	
	// CALCULATE on hitting Enter in every input
	// ------------------------------------------------------------
	$('input', $("#hypcalc_inputs")).keyup(function (e) {
     if (e.keyCode == 13) {
			$("#hypcalc_calculate").trigger("click");
		}
	});
	
	
	// EXAMPLE 1
	// ------------------------------------------------------------
	$("#hypcalc_ex1").on("click", function () {
		$("#pop_n").val(40);
		$("#pop_k").val(4);
		$("#smp_n").val(5);
		$("#smp_k").val("1-4");
		$("#hypcalc_calculate").trigger("click");
	});
	
	// EXAMPLE 2
	// ------------------------------------------------------------
	$("#hypcalc_ex2").on("click", function () {
		$("#pop_n").val(40);
		$("#pop_k").val(4);
		$("#smp_n").val(10);
		$("#smp_k").val("1-4");
		$("#hypcalc_calculate").trigger("click");
	});
	
	// EXAMPLE 3
	// ------------------------------------------------------------
	$("#hypcalc_ex3").on("click", function () {
		$("#pop_n").val(10);
		$("#pop_k").val(6);
		$("#smp_n").val(4);
		$("#smp_k").val("2-4");
		$("#hypcalc_calculate").trigger("click");
	});
	
	// EXAMPLE 4
	// ------------------------------------------------------------
	$("#hypcalc_ex4").on("click", function () {
		$("#pop_n").val(10);
		$("#pop_k").val(1);
		$("#smp_n").val(4);
		$("#smp_k").val("1");
		$("#hypcalc_calculate").trigger("click");
	});
	
	
	
	// Calculate binomial coefficient
	function binomial (a,b) {
		var temp1=1,
				temp2=1;
		if(a !== 0) {
			for(var i=a; i>=a-b+1; i--) {temp1*=i}
			for(var j=b; j>0; j--) {temp2*=j}
			return parseInt(temp1/temp2);
		}
	}
	 
	// Hypergeometric distribution function
	function hdf (N,K,n,k,cumulative) {
		N = parseInt(N);
		K = parseInt(K);
		n = parseInt(n);
		k = parseInt(k);
		cumulative = Boolean(cumulative);
		var result = 0;
		
		if (cumulative) {
			for (var i=0; i<=k; i++) {
				result += binomial(K,i)*binomial(N-K,n-i)/binomial(N,n);
			}
		}
		else {
			result = binomial(K,k)*binomial(N-K,n-k)/binomial(N,n);
		}
		if(isNaN(result) || result < 0) {result = 0;}
		return result;
	}
	
});