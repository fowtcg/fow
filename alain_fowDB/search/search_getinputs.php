<?php

if (!empty($_GET['q'])) {
	
	// GET SEARCH QUERY (only searchbar)
	// --------------------------------------------------

	// Sanitize raw query, escaping html entities,
	// This will be rewritten as sticky info in the search bar
	$q_sanitize = htmlentities($_GET['q']);
	
	// Preserve just the ampersand as a logic operator
  $_GET['q'] = str_replace(array("&amp;", "&", "&#38;"),":andoperator:", $_GET['q']);
	
	// Cancel troublesome characters (quotes, <, >)
	$table = get_html_translation_table(HTML_SPECIALCHARS, ENT_QUOTES);
	$q = str_replace($table, "", htmlspecialchars($_GET['q'], ENT_QUOTES, 'UTF-8') );
	
	// Get keywords
	$keywords = explode(" ", $q);
	
	// Replace custom syntax symbols
	// Replace [_ and _] in [[ and ]] to preserve underscore, than replace back
	// two str_replace are faster than a regex
	$keywords = str_replace(array("[_", "_]", "_", ":andoperator:"), array("[[", "]]", " ", "%"), $keywords);
	$keywords = str_replace(array("[[", "]]"), array("[_", "_]"), $keywords);
	
	
	// BUILD INDIVIDUAL FILTERS
	// --------------------------------------------------
	
	// Open the logical block of query terms in SQL
	$sql_q = "(";
	
	// Build LIKE clause
	$cardname = "";
	$cardcode = "";
	$cardtext = "";
	$subtype_race = "";
	
	// Get keywords count
	$last = count($keywords) - 1;
	
	for ($i = 0; $i <= $last; $i++) {
		$keyword = $keywords[$i];
		if ($i != $last) {
			$cardname .= "cardname LIKE \"%{$keyword}%\" OR ";
			$cardcode .= "cardcode LIKE \"%{$keyword}%\" OR ";
			$cardtext .= "cardtext LIKE \"%{$keyword}%\" OR ";
			$subtype_race .= "subtype_race LIKE \"%{$keyword}%\" OR ";
		}
		else { // Last keyword or just one keyword entered by user
			$cardname .= "cardname LIKE \"%{$keyword}%\"";
			$cardcode .= "cardcode LIKE \"%{$keyword}%\"";
			$cardtext .= "cardtext LIKE \"%{$keyword}%\"";
			$subtype_race .= "subtype_race LIKE \"%{$keyword}%\"";
		}
	}
	
	
	// BUILD FINAL SQL CLAUSE FOR SEARCH TERMS
	// --------------------------------------------------
	
	if (!empty($_GET['subtype_race'])) {
		
		// If subtype_race is set into filters,
		// that filters overrides query terms in subtype_race field
		$sql_q .= $cardname . " OR " . 
							$cardcode . " OR " . 
							$cardtext . ")";
	}
	else {
		$sql_q .= $cardname . " OR " . 
							$cardcode . " OR " . 
							$cardtext . " OR " . 
							$subtype_race . ")";
	}
}
else {
	// If no query terms, bypass them in SQL
	$sql_q = " TRUE ";
}




// ACQUIRE FILTERS
// --------------------------------------------------

// Open the logical block of filters in SQL
$sql_f = "";

// Get all filter parameters
$filters = array();
foreach ($_GET as $key => $value) {
	
	// If the parameter has more than one value (es.: attribute[]=w&attribute[]=u)
	if (is_array($value)) {
		foreach ($value as $subkey => $subvalue) {
			$filters[$key][$subkey] = htmlspecialchars($subvalue, ENT_QUOTES, 'UTF-8');
		}
	}
	// If parameter has single value
	else {
		$filters[$key] = htmlspecialchars($_GET[$key], ENT_QUOTES, 'UTF-8');
	}
}

// FILTER --- FORMAT
if (isset($filters['format'])) {
	switch ($filters['format']) {
		case "New Frontiers":
			$sql_f = "block IN(2,3)";
			break;
		case "Origin":
			$sql_f = "block IN(1,2)";
			break;
		case "Valhalla":
			$sql_f = "block = 1";
			break;
	}
}
else {
	$sql_f = "TRUE";
}

// FILTER --- SPOILER
if (isset($filters['spoiler'])) {
	
	// If user selected a spoiler setcode, delete it
	if (isset($filters['setcode']) AND in_array($filters['setcode'], $spoiler_sets_setcodes)) {
		unset($filters['setcode']);
	}
	else {
		$sql_f .= " AND NOT(setcode IN (\"" . implode("\",\"", $spoiler_sets_setcodes) . "\"))";
	}
}

// FILTER --- SET CODE
if (isset($filters['setcode']) && $filters['setcode'] != "0") {
		$sql_f .= " AND setcode = \"" . $filters['setcode'] . "\"";
}

// FILTER --- NUMBER
if (isset($filters['cardnum']) && (int) $filters['cardnum'] != 0) {
		$sql_f .= " AND cardnum = " . $filters['cardnum'];
}

// FILTER --- SUBTYPE OR RACE
if (isset($filters['subtype_race']) && !empty($filters['subtype_race'])) {
	$sql_f .= " AND subtype_race LIKE \"%" . $filters['subtype_race'] . "%\"";
}

// FILTER ---  ATTRIBUTE
if (isset($filters['attributes'])) {
	
	$sql_f .= " AND (";
	$temp = $filters['attributes']; $last = count($temp) - 1;
	
	for ($i = 0; $i <= $last; $i++) {
		if ($i != $last) {
			$sql_f .= "attribute LIKE \"%" . $temp[$i] . "%\" OR ";
		}
		else {
			$sql_f .= "attribute LIKE \"%" . $temp[$i] . "%\"";
		}
	}
	
	$sql_f .= ")";
}

// FILTER --- TYPE
if (isset($filters['cardtype'])) {
	
	$sql_f .= " AND (";
	$temp = $filters['cardtype'];
	$last = count($temp) - 1;
	
	for ($i = 0; $i <= $last; $i++) {
		if ($i != $last) {
			$sql_f .= "cardtype = \"" . $temp[$i] . "\" OR ";
		}
		else {
			$sql_f .= "cardtype = \"" . $temp[$i] . "\"";
		}
	}
	
	$sql_f .= ")";
}

// FILTER --- FREE COST
if (isset($filters['freecost'])) {
	$sql_f .= " AND (";
	$temp = $filters['freecost'];
	$last = count($temp) - 1;
	
	for ($i = 0; $i <= $last; $i++) {
		if ($i != $last) {
			$sql_f .= "freecost = " . $temp[$i] . " OR ";
		}
		else {
			$sql_f .= "freecost = " . $temp[$i];
		}
	}
	
	$sql_f .= ")";
}

// FILTER --- TOTAL COST
if (isset($filters['totalcost'])) {
	
	$sql_f .= " AND (";
	$temp = $filters['totalcost'];
	$last = count($temp) - 1;
	
	for ($i = 0; $i <= $last; $i++) {
		if ($i != $last) {
			$sql_f .= "totalcost = " . $temp[$i] . " OR ";
		}
		else {
			$sql_f .= "totalcost = " . $temp[$i];
		}
	}
	
	$sql_f .= ")";
}

// FILTER --- ATTACK
if (isset($filters['atk']) && (int) $filters['atk'] != 0) {
	$sql_f .= " AND atk " .
		atkdef_operator($filters['atk-operator']) .
		" " .
		$filters['atk'];
}

// FILTER --- DEFENSE
if (isset($filters['def']) && (int) $filters['def'] != 0) {
	
	$sql_f .= " AND def " .
	atkdef_operator($filters['def-operator']) .
	" " .
	$filters['def'];
}

// FILTER --- RARITY
if (isset($filters['rarity'])) {
	
	$sql_f .= " AND (";
	$temp = $filters['rarity']; $last = count($temp) - 1;
	
	for ($i = 0; $i <= $last; $i++) {
		if ($i != $last) {
			$sql_f .= "rarity = \"" . $temp[$i] . "\" OR ";
		}
		else {
			 $sql_f .= "rarity = \"" . $temp[$i] . "\"";
		}
	}
	
	$sql_f .= ")";
}


// Prevent magic stones in the results
// unless setcode, cardnum, cardtype or rarity is set
if (
	!isset($filters['setcode']) AND
	!isset($filters['cardnum']) AND
	!isset($filters['cardtype']) AND
	!isset($filters['rarity'])
) {
	$sql_f .= " AND NOT(cardtype = 'Magic Stone')";
}


// FILTER --- ORDER RESULTS (user-defined by menu)
if (isset($filters['orderby'])) {

	isset($filters['order_dir'])
		? $dir = " DESC"
		: $dir = "";

	switch($filters['orderby']) {
		case 'attribute':
			$orderby = " ORDER BY FIELD(attribute, 'w', 'r', 'u', 'g', 'b', 'v')" . $dir . ", id";
			break;
		case 'rarity':
			$orderby = " ORDER BY FIELD(rarity, 'c', 'u', 'r', 'sr', 'pr', 's')" . $dir . ", id";
			break;
		case 'cardtype':
			// Generate list of types
			$types_list = "";
			foreach ($types as $type => $value) { $types_list .= "'" . $type . "', "; }
			// Truncate the last three characters
			$types_list = substr($types_list, 0, -2);
			
			$orderby = " ORDER BY FIELD(cardtype, " . $types_list . ")" . $dir . ", id";
			break;
		
		// For setcode, number, subtype_race, totalcost, atk and def
		// there's no need for a custom ORDER BY clause
		default:
			$orderby = " ORDER BY " . $filters['orderby'] . $dir . ", id";
			break;
	}
}
else {
	$orderby = "";
}

	
// FUNCTION
// This function returns the correct symbol from its textual representation
function atkdef_operator ($operator) {
	if ($operator == 'lessthan') { return '<'; }
	if ($operator == 'equals') { return '='; }
	if ($operator == 'greaterthan') { return '>'; }
}