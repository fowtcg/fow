<?php

// Get all filters and search terms
include 'search/search_getinputs.php';

// Connect to the database
include '_includes/db_connect.php';

// Avoid missing cards
$noMissing = "NOT (image_path = \"cards/missing.jpg\")";

// Build SQL statement
$sql = "SELECT cards_basic.id as id, cardcode, cardname, setcode, image_path, thumb_path " .
	"FROM cards_basic INNER JOIN cards_" . $_SESSION['lang'] .
			" ON cards_basic.id = cards_" . $_SESSION['lang'] .".id " .
	"WHERE " . $sql_f . " AND " . $sql_q . " AND " . $noMissing . $orderby;

// Try executing the query on db
try {
	$query = $pdo->query($sql);
	$result = $query->fetchAll();
}
catch (PDOException $e) {
	$error = UI_ERROR_DB . $e->getMessage();
	include '_template/error.php';
	exit();
}

// If there's some result, write info into $cards[] array
if (!empty($result)) {
	foreach($result as $row) {
		$cards[] = array (
			// $cards holds all columns fetched from db
			'id' => $row['id'],
			'cardcode' => $row['cardcode'],
			'cardname' => $row['cardname'],
			'setcode' => $row['setcode'],
			'image_path' => $row['image_path'],
			'thumb_path' => $row['thumb_path']
		);
	}
	$results = true;
}
else {
	$cards = array();
	$notif = UI_VIEWER_NORESULTS;
	$results = false;
}

// View results
include 'search/search.php';
exit();