<html>
	<head>
		<!-- Open Graph Project meta tags -->
		<?php $page = UI_HEADER_NAVLINK_SEARCH; include '_includes/ogp.php'; ?>
		
		<!-- Title -->
		<title><?=$ogp['title']?></title>

		<!-- Meta tags -->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Scripts -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="_scripts/common.js"></script>
		<script src="_scripts/search.js"></script>

		<!-- Style -->
		<link href="_styles/reset.css" rel="stylesheet" type="text/css">
		<link href="_styles/style.css" rel="stylesheet" type="text/css">
		
		<?php if ($results)
			// Load the plugin only if results
			echo '<!-- LIGHTBOX ZOOM PLUGIN -->' . 
					 '<script src="_scripts/lightbox/js/lightbox.min.js" charset="utf-8"></script>' . 
					 '<link href="_scripts/lightbox/css/lightbox.css" type="text/css" rel="stylesheet" title="Featherlight Styles" />';
		?>

		<!-- Favicon -->
		<link rel="icon" href="_images/icons/favicon.png" type="image/png"/>
	</head>
	
	<body>
		<?php
			echo '<div id="wrapper">';
			
				// Header
				include '_includes/header.php';
				
				// Content
				echo '<div id="content">';
				
					// Searchbox and Filters
					include 'search/search.html.php';
					
					// Search Results
					if ($results) {
						
						// Options
						echo '<div id="floatleft">';
							include 'search/options.html.php';
						echo '</div>';
						
						// Viewer
						echo '<div id="floatright">';
							include 'search/viewer.html.php';
						echo '</div>';
						
					}
					
				echo '</div>';
				
			// Footer
			include '_includes/footer.php';
		
			// Close wrapper
			echo '</div>';
		?>
	</body>
</html>