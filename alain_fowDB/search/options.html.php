<div id="options" class="sections">
	<div class="header">
		<a class="hide">
			<img src="_images/icons/hide.png" />
			<h1 class="title"><?=UI_OPTIONS_TITLE?></h1>
		</a>
	</div>
	<div class="content">
	
		<!-- Cards per row -->
		<div class="option" id="option_numxrow">
			<h2 class="title"><?=UI_OPTIONS_NUMXROW_TITLE?></h2>
			<div class="controls"><!--
				--><button class="opt_b" id="opt_b_numxrow_minus">-</button><!--
				--><input class="opt_i" id="opt_i_numxrow" name="opt_i_numxrow" /><!--
				--><button class="opt_b" id="opt_b_numxrow_plus">+</button><!--
			--></div>
		</div>
		
		<hr>
		
		<!-- Display info on click -->
		<div class="option" id="option_info">
			<h2 class="title">Info</h2>
			<div class="controls"><!--
				--><input class="opt_i" type="checkbox" id="opt_i_info" name="opt_i_info" /><!--
				--><span class="checklabel"><?=UI_OPTIONS_INFO_OPEN?></span><!--
			--></div>
		</div>
		
		<hr>
		
		<!-- Display card image on click -->
		<div class="option" id="option_zoom">
			<h2 class="title">Zoom</h2>
			<div class="controls"><!--
				--><input class="opt_i" type="checkbox" id="opt_i_zoom" name="opt_i_zoom" /><!--
				--><span class="checklabel"><?=UI_OPTIONS_ZOOM_ZOOMONCLICK?></span><!--
			--></div>
		</div>
		
		<hr>
		
		<?php if($is_spoiler): ?>
			<!-- Show missing cards as covered cards -->
			<div class="option" id="option_zoom">
				<h2 class="title"><?=UI_OPTIONS_MISSING_TITLE?></h2>
				<div class="controls"><!--
					--><input class="opt_i" type="checkbox" id="opt_i_missing" name="opt_i_missing" /><!--
					--><span class="checklabel"><?=UI_OPTIONS_MISSING?></span><!--
				--></div>
			</div>
			
			<hr>
		<?php endif; ?>
		
		<!-- Select cards on click -->
		<div class="option" id="option_select">
			<h2 class="title"><?=UI_OPTIONS_SELECT_TITLE?></h2>
			<div class="controls"><!--
				--><input class="opt_i" type="checkbox" id="opt_i_select" name="opt_i_select" /><!--
				--><span class="checklabel"><?=UI_OPTIONS_SELECT_SELCARDS?></span><!--
				--><button class="opt_b" id="opt_b_select_selAll"><?=UI_OPTIONS_SELECT_SELALL?></button><!--
				--><button class="opt_b" id="opt_b_select_deselAll"><?=UI_OPTIONS_SELECT_DESELALL?></button><!--
				--><button class="opt_b" id="opt_b_select_showSel"><?=UI_OPTIONS_SELECT_SHOWSEL?></button><!--
				--><button class="opt_b" id="opt_b_select_showAll"><?=UI_OPTIONS_SELECT_SHOWALL?></button><!--
				--><button class="opt_b" id="opt_b_select_custom"><?=UI_OPTIONS_SELECT_SAVESEL?></button><!--
			--></div>
		</div>
		
		<hr>
		
	</div>
</div>