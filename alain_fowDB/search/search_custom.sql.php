<?php

// Connect to DB
include '_includes/db_connect.php';

// Build sql
if (isset($_GET['id'])) {
	$sql = "SELECT cards_basic.id as id, cardcode, cardname, setcode, image_path, thumb_path " .
	"FROM cards_basic INNER JOIN cards_" . $_SESSION['lang'] .
			" ON cards_basic.id = cards_" . $_SESSION['lang'] .".id " .
	"WHERE cards_basic.id IN(" . implode(",", $_GET['id']) . ")";
}
else {
	$sql = "SELECT * FROM cards_basic WHERE FALSE";
}

// Try executing the query on db
try {
	// Execute query on db
	$query = $pdo->query($sql);
	
	// Fetch results from db
	$result = $query->fetchAll();
}
catch (PDOException $e) {
	$error = UI_ERROR_DB . " - " . $e->getMessage();
	include '_template/error.php';
	exit();
}

if (!empty($result)) {
	foreach($result as $row) {
		$cards[] = array (
			// $cards holds all columns fetched from db
			'id' => $row['id'],
			'cardcode' => $row['cardcode'],
			'cardname' => $row['cardname'],
			'setcode' => $row['setcode'],
			'thumb_path' => $row['thumb_path'],
			'image_path' => $row['image_path']
		);
	}
	$results = true;
}
else {
	$cards = array();
	$notif = UI_VIEWER_NORESULTS;
	$results = false;
}

// View results
include 'search/search.php';
exit();