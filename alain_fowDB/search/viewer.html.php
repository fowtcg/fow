<div id="viewer" class="sections">
	<div class="header">
		<h1 class="title"><?=UI_VIEWER_TITLE?> (<?php echo count($cards); ?>)</h1>
	</div>
	<div class="content"><!--
		<?php if(!empty($cards)): ?>
			<?php
				// Get spoiler setcodes
				$spoiler_setcodes = array();
				foreach ($spoiler_sets as $spoiler_set) {
					$spoiler_setcodes[] = $spoiler_set['setcode'];
				}
			?>
			<?php foreach ($cards as $card): ?>
				--><div class="card<?php
						// Add "spoiled" class if it's a spoiler card
						if ( in_array($card['setcode'], $spoiler_setcodes) ) {
							echo " spoiled";
						}
					?>"><!--
					<?php if ($card['thumb_path'] == "thumbs/missing2.jpg"): ?>
						--><div class="missing"><?=$card['cardname']?></div><!--
					<?php endif; ?>
					--><a><!--
						--><img src="<?=$card['thumb_path']?>" data-code="<?=$card['cardcode']?>" data-id="<?=$card['id']?>" data-set="<?=$card['setcode']?>" alt="<?=$card['cardname']?>" /><!--
					--></a><!--
				--></div><!--
			<?php endforeach; ?>
			--><a href="#top" class="toTop">Top</a>
			<?php else: ?>
				<?=UI_VIEWER_NORESULTS?>
		<?php endif; ?>
	</div>
</div>