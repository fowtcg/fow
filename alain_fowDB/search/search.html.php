<div id="searchBox">
	<form id="form_search" action="" method="GET">
		
		<!-- Do search -->
		<input type="hidden" name="do" value="search" />
	
		<div id="searchbar"><!--
			--><button type="button" id="help">?</button><!--
			--><input id="q" name="q" type="text" placeholder="<?=UI_SEARCHBOX_PLACEHOLDER?>" value="<?php if(isset($q_sanitize)) {echo $q_sanitize;}?>"/><!--
			--><button id="form_submit_top" type="submit"><?=UI_HEADER_NAVLINK_SEARCH?></button><!--
		--></div>
		
		<!-- =============================
		SYNTAX
		============================== -->
		<div id="syntax">
			<div class="syntaxRow"><?=UI_SEARCHBOX_QUERYEX?></div>
			<div class="syntaxCol">
				<ul>
					<li>{w} = <img src="_images/icons/w.png" /></li>
					<li>{r} = <img src="_images/icons/r.png" /></li>
					<li>{u} = <img src="_images/icons/u.png" /></li>
					<li>{g} = <img src="_images/icons/g.png" /></li>
					<li>{b} = <img src="_images/icons/b.png" /></li>
				</ul>
			</div>
			
			<div class="syntaxCol">
				<ul>
					<li>{1} = <img src="_images/icons/freewill.png" /></li>
					<li>{<?=UI_SEARCHBOX_REST?>} = <img src="_images/icons/rest.png" /></li>
					<li>[<?=UI_SEARCHBOX_ABILITY?>] = <span class="mark_abilities"><?=UI_SEARCHBOX_ABILITY?></span></li>
					<li>[_<?=UI_SEARCHBOX_SKILL?>_] = <span class="mark_skills"><?=UI_SEARCHBOX_SKILL?></span></li>
				</ul>
			</div>
			
			<div class="syntaxRow">
				<ul>
					<li>
						<?=UI_SEARCHBOX_OR?>
					</li>
					<li>
						<?=UI_SEARCHBOX_LITERAL?>
					</li>
					<li>
						<?=UI_SEARCHBOX_AND?>
					</li>
				</ul>
			</div>
		</div>
		
		
		
		<!-- ==============================
		FILTERS
		============================== -->
		<div id="filters" class="sections">
			<div class="header">
				<a class="hide">
					<img src="_images/icons/hide.png" />
					<h1 class="title"><?=UI_FILTERS_TITLE?></h1>
				</a>
			</div>
			<div class="content">
				<!--
				==============================
				////////// LEFT \\\\\\\\\\
				==============================
				-->
				<div id="filterL">
				
				
					<!-- FORMAT -->
					<div class="filter">
						<label for="format"><?=UI_CARD_LABEL_FORMAT?></label>
						<ul>
							<?php
								foreach($formats as $format => $format_sets) {
									
									// Sticky
									if (isset($filters['format'])) {
										$format == $filters['format']
											? $checked = ' checked=true'
											: $checked = '';
									}
									else {
										$format == $format_default
											? $checked = ' checked=true'
											: $checked = '';
									}
									
									// Print the checkboxes
									echo '<li><input type="radio" name="format" value="' .
											$format . '"' . $checked . ' />' . 
											'<span class="checklabel">' . $format . '</span></li>';
								}
							?>
						</ul>
					</div>
					
					
					<!-- SET -->
					<div class="filter">
						<label for="set"><?=UI_CARD_LABEL_SET?></label>
						<select name="setcode">
							
							<!-- Default -->
							<option name="set_default" value=0>
								<span class="checklabel"><?=UI_FILTERS_CHOOSESET?></span>
							</option>
							
							<?php
								foreach($blocks as $block_index => $block) {
									
									echo '<optgroup label="' . $block["label"] . '">';
									
									foreach($block['sets'] as $setcode => $setname) {
										
										// Sticky
										isset($filters['setcode']) AND $setcode == $filters['setcode']
											? $checked = ' selected=true'
											: $checked = '';
										
										// Output single set
										echo '<option value="' . $setcode . '"' . $checked . ' >' . $setname . '</option>';
									}
									echo '</optgroup>';
								}	
							?>	
						</select>
					</div>
					
					
					<!-- CARD NUMBER -->
					<div class="filter">
						<label for="cardnum"><?=UI_CARD_LABEL_NUMBER?></label>
						<?php
							// Sticky
							isset($filters['cardnum'])
								? $cardnum = $filters['cardnum']
								: $cardnum = '';
						?>
						<input type="number" name="cardnum" placeholder="<?=UI_CARD_LABEL_NUMBER?>.." value="<?=$cardnum?>" />
					</div>
					
					
					<!-- SUBTYPE_RACE -->
					<div class="filter">
						<label for="subtype_race"><?=UI_CARD_LABEL_SUBTYPE_RACE?></label>
						<?php
							// Sticky
							isset($filters['subtype_race'])
								? $sub_race = $filters['subtype_race']
								: $sub_race = '';
						?>
						<input type="text" name="subtype_race" placeholder="<?=UI_CARD_LABEL_SUBTYPE_RACE?>.." value="<?=$sub_race?>" />
					</div>
					
					
					<!-- ATTRIBUTE -->
					<div class="filter">
						<label for="attributes[]"><?=UI_CARD_LABEL_ATTRIBUTE?></label>
						<ul class="attributes">
							<?php
								foreach ($attributes as $attr_index => $attr) {
									
									
									// Sticky
									if(isset($filters['attributes'])) {
										in_array($attr_index, $filters['attributes'])
											? $checked = ' checked=true'
											: $checked = '';
									}
									else {
										$checked = '';
									}
									
									echo '<li><input type="checkbox" name="attributes[]"' . 
										' value="' . $attr_index . '"' . $checked . ' />' . 
										'<span class="checklabel"><img src="_images/icons/' . $attr_index . '.png" /></span></li>';
								}
							?>
						</ul>
					</div>
					
					
					<!-- TOTALCOST -->
					<div class="filter">
						<label for="totalcost[]"><?=UI_CARD_LABEL_TOTALCOST?></label>
						<ul>
							<?php
								foreach ($costs as $cost) {
									
									// Sticky
									isset($filters['totalcost']) AND in_array($cost, $filters['totalcost'])
										? $checked = ' checked=true'
										: $checked = '';
										
									// Break costs into two lines
									$cost == "4"
										? $a_capo = '<br />'
										: $a_capo = "";
									
									// Produce checkboxes
									echo '<li><input type="checkbox" name="totalcost[]"' . 
										' value=' . $cost . $checked . ' />' . 
										'<span class="checklabel">' . $cost . '</span></li>' . $a_capo;
								}
							?>
						</ul>
					</div>
					
					
					<!-- RARITY -->
					<div class="filter">
						<label for="rarity[]"><?=UI_CARD_LABEL_RARITY?></label>
						<ul>
							<?php
								foreach ($rarities as $rarity) {
									
									// Sticky
									isset($filters['rarity']) AND in_array($rarity, $filters['rarity'])
										? $checked = ' checked=true'
										: $checked = '';

									// Print checkboxes
									echo '<li><input type="checkbox" name="rarity[]"' . 
										' value="' . $rarity . '"' . $checked . ' />' . 
										'<span class="checklabel">' . strtoupper($rarity) . '</span></li>';
								}
							?>
						</ul>
					</div>
					
				</div>
				
				
				<!--
				==============================
				////////// RIGHT \\\\\\\\\\
				==============================
				-->
				<div id="filterR">
					
					<!-- TYPE -->
					<div class="filter">
						<label for="cardtype[]"><?=UI_CARD_LABEL_TYPE?></label>
						<ul class="cardtype">
							<?php
								foreach ($types as $type_index => $type) {
									
									// Check if value is already checked
									isset($filters['cardtype']) AND in_array($type_index, $filters['cardtype'])
										? $checked = ' checked=true'
										: $checked = '';

									// Produce checkboxes
									echo '<li><input type="checkbox" name="cardtype[]"' . 
										' value="' . $type_index . '"' . $checked . ' />' . 
										'<span class="checklabel">' . $type . '</span></li>';
								}
							?>
						</ul>
					</div>
					
					
					
					
					<!-- ATT -->
					<div class="filter" id="filter_atk">
						<label for="atk"><?=UI_CARD_LABEL_ATK?></label>
						<select name="atk-operator">
							<?php
								
								// Define operators
								$operators = array(
									'equals' => '=',
									'greaterthan' => '>',
									'lessthan' => '<'
								);
							
								foreach ($operators as $op_key => $op_value) {
							
									// Sticky - ATK operator
									isset($filters['atk-operator']) AND $op_key == $filters['atk-operator']
										? $checked = ' selected=true'
										: $checked = '';
									
									// Sticky - ATK
									isset($filters['atk'])
										? $atk = $filters['atk']
										: $atk = "";
										
									// Print the options
									echo '<option value="' . $op_key . '"' . $checked . '>' . $op_value . '</option>';
								}
							?>
						</select>
						<input type="number" name="atk" class="stats" placeholder="ATK.." value=<?=$atk?> />
					</div>
					
					
					
					
					<!-- DEF -->
					<div class="filter" id="filter_atk">
						<label for="atk"><?=UI_CARD_LABEL_DEF?></label>
						<select name="def-operator">
							<?php
								foreach ($operators as $op_key => $op_value) {
									
									// Sticky - DEF operator
									isset($filters['def-operator']) AND $op_key == $filters['def-operator']
										? $checked = ' selected=true'
										: $checked = '';
									
									// Sticky - DEF
									isset($filters['def'])
										? $def = $filters['def']
										: $def = "";
										
									// Print the options
									echo '<option value="' . $op_key . '"' . $checked . '>' . $op_value . '</option>';
								}
							?>
						</select>
						<input type="number" name="def" class="stats" placeholder="DEF.." value=<?=$def?> />
					</div>
					
					
					<!-- ORDER BY -->
					<div class="filter">
						<label for="orderby"><?=UI_FILTERS_ORDERBY?></label>
						<select name="orderby">
							<?php
								foreach($card_features as $feature => $feature_label) {
									
									// Sticky
									isset($filters['orderby']) AND $filters['orderby'] == $feature
										? $checked = ' selected=true'
										: $checked = '';
									
									// Print the options
									echo '<option value="' . $feature . '"' . $checked . '>' . $feature_label  . '</option>';
								}
							?>
						</select>
						
						<!-- Descendent (optional) -->
						<?php
							isset($filters['order_dir'])
								? $checked = ' checked=true'
								: $checked = '';
						?>
						<input type="checkbox" name="order_dir" value="desc"<?=$checked?>>
							<span class="checklabel"><?=UI_FILTERS_ORDERBY_DESC?></span>
						</input>
					</div>
					
					
					<!-- EXCLUDE SPOILERS -->
					<div class="filter">
						<label for="spoiler"><?=UI_HEADER_NAVLINK_SPOILER?></label>
						<!-- Descendent (optional) -->
						<?php
							isset($filters['spoiler'])
								? $checked = ' checked=true'
								: $checked = '';
						?>
						<input type="checkbox" name="spoiler" value="no"<?=$checked?>>
							<span class="checklabel"><?=UI_FILTERS_EXCLUDE_SPOILERS?></span>
						</input>
					</div>
					
					
				</div>

				<div>
					<button type="button" class="action_buttons" id="form_reset">Reset</button>
					<button type="submit" class="action_buttons" id="form_submit_bottom"><?=UI_HEADER_NAVLINK_SEARCH?></button>
				</div>
			</div>	
		</div>
	</form>
</div>