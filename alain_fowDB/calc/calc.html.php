<?php if ($_SESSION['lang'] == 'en'): ?>

	<!-- ==============================
		ENGLISH
	=============================== -->

	<!-- Calculator -->
	<div id="hypcalc" class="sections">
		<div class="header">
			<h1 class="title">Draw calculator</h1>
		</div>
		<div class="content">
			<p class="hypcalc_description">
				This tool helps you to calculate the probability of drawing a card from a deck using the <a href="http://en.wikipedia.org/wiki/Hypergeometric_distribution" class="inline_link" target=_blank>hypergeometric distribution</a>. The tool accepts four parameters (N, K, n, k) and calculates the probability of having k successes while drawing n cards from a deck of N cards, given that this deck contains K successes. To calculate the cumulative probability (ex.: probability of drawing 1 to 4 copies of a specific card in the opening hand), the <em>Drawn successes</em> field lets you enter a hyphen between two digits (See examples below).
			</p>
			
			<!-- Parameters -->
			<div id="hypcalc_inputs">
				
				<div class="hypcalc_parameters">
					<label for="pop_n">Cards in deck</label>
					<input type="number" name="pop_n" id="pop_n" placeholder="N.." autofocus />
				</div>
				
				<div class="hypcalc_parameters">
					<label for="pop_k">Successes in deck</label>
					<input type="number" name="pop_k" id="pop_k" placeholder="K.." />
				</div>
				
				<div class="hypcalc_parameters">
					<label for="smp_n">Drawn cards</label>
					<input type="number" name="smp_n" id="smp_n" placeholder="n.." />
				</div>
				
				<div class="hypcalc_parameters">
					<label for="smp_k">Drawn successes</label>
					<input type="text" name="smp_k" id="smp_k" placeholder="k.." />
				</div>
				
			</div>
			
			<!-- Result -->
			<div id="hypcalc_result"><!--
				--><div id="hypcalc_calculate">CALCULATE</div><div id="hypcalc_output">0.00%</div><!--
			--></div>
			
			<!-- Examples-->
			<div id="hypcalc_examples">
				<div class="hypcalc_example">
					<button class="hypcalc_example_button" id="hypcalc_ex1">Example 1</button>
					<span class="hypcalc_example_desc">
						Probability of drawing at least a copy (i.e. from 1 to 4 copies) of <a href="http://www.fowdb.altervista.org/index.php?p=card&code=CMF-067+C" class="inline_link" target="_blank">Elvish Priest</a> in the opening hand (no mulligan), having 4x Elvish Priest and 40 cards in the deck.
					</span>
				</div>
				<div class="hypcalc_example">
					<button class="hypcalc_example_button" id="hypcalc_ex2">Example 2</button>
					<span class="hypcalc_example_desc">
						Probability of drawing at least a copy (i.e. from 1 to 4 copies) of <a href="http://www.fowdb.altervista.org/index.php?p=card&code=CMF-026+C" class="inline_link" target="_blank">Hunter in Black Forest</a> in the opening hand with maximum mulligan of 5 cards (the same as drawing 10 cards from the deck), having 4x Hunter in Black Forest and 40 cards in the deck.
					</span>
				</div>
				<div class="hypcalc_example">
					<button class="hypcalc_example_button" id="hypcalc_ex3">Example 3</button>
					<span class="hypcalc_example_desc">
						Probability of having at least two dark magic stones to play <a href="http://www.fowdb.altervista.org/index.php?p=card&code=TAT-088+R" class="inline_link" target="_blank">Stoning to Death</a> in turn 4, having 6 dark magic stones out of 10 in the magic stone deck. Note: drawn successes here must be 2-4 being 2 the minimum required to play Stoning to Death and 4 drawn cards. Here we assume that you call one magic stone each turn.
					</span>
				</div>
				<div class="hypcalc_example">
					<button class="hypcalc_example_button" id="hypcalc_ex4">Example 4</button>
					<span class="hypcalc_example_desc">
					Probability of having <a href="http://www.fowdb.altervista.org/index.php?p=card&code=TAT-100+SR" class="inline_link" target="_blank">Moojdart, the Fantasy Stone</a> in turn 4 having just one copy of it in a magic stone deck of 10 cards.
					</span>
				</div>
			</div>
			
		</div>
	</div>

<?php elseif ($_SESSION['lang'] == 'it'): ?>
	
	<!-- ==============================
		ITALIANO
	=============================== -->

	<!-- Calculator -->
	<div id="hypcalc" class="sections">
		<div class="header">
			<h1 class="title">Calcolatore di pescata</h1>
		</div>
		<div class="content">
			<p class="hypcalc_description">
				Questo strumento aiuta a calcolare le probabilità di pescare una carta da un mazzo tramite l'ausilio della <a href="http://it.wikipedia.org/wiki/Distribuzione_ipergeometrica" class="inline_link"> distribuzione ipergeometrica</a>. Il tool prende quattro parametri (N, K, n, k) e calcola la probabilità di avere k successi in una pescata di n carte estratta da un mazzo di N carte contenente K successi. Per calcolare una probabilità cumulata (es.: probabilità di pescare da 1 a 4 copie di una carta alla prima mano), il campo <em>Successi pescati</em> permette di inserire un trattino tra due numeri (Clicca sugli esempi per approfondire).
			</p>
			
			<!-- Parameters -->
			<div id="hypcalc_inputs">
				
				<div class="hypcalc_parameters">
					<label for="pop_n">Carte mazzo</label>
					<input type="number" name="pop_n" id="pop_n" placeholder="N.." autofocus />
				</div>
				
				<div class="hypcalc_parameters">
					<label for="pop_k">Successi mazzo</label>
					<input type="number" name="pop_k" id="pop_k" placeholder="K.." />
				</div>
				
				<div class="hypcalc_parameters">
					<label for="smp_n">Carte pescate</label>
					<input type="number" name="smp_n" id="smp_n" placeholder="n.." />
				</div>
				
				<div class="hypcalc_parameters">
					<label for="smp_k">Successi pescati</label>
					<input type="text" name="smp_k" id="smp_k" placeholder="k.." />
				</div>
				
			</div>
			
			<!-- Result -->
			<div id="hypcalc_result"><!--
				--><div id="hypcalc_calculate">CALCOLA</div><div id="hypcalc_output">0.00%</div><!--
			--></div>
			
			<!-- Examples-->
			<div id="hypcalc_examples">
				<div class="hypcalc_example">
					<button class="hypcalc_example_button" id="hypcalc_ex1">Esempio 1</button>
					<span class="hypcalc_example_desc">
						Probabilità di pescare almeno una copia (cioè da 1 a 4 copie) di <a href="http://www.fowdb.altervista.org/index.php?p=card&code=CMF-067+C" class="inline_link" target="_blank">Elfo Sacerdote</a> alla prima pescata (no mulligan), con 4x Elfo Sacerdote nel mazzo, mazzo da 40 carte.
					</span>
				</div>
				<div class="hypcalc_example">
					<button class="hypcalc_example_button" id="hypcalc_ex2">Esempio 2</button>
					<span class="hypcalc_example_desc">
						Probabilità di pescare almeno una copia (cioè da 1 a 4 copie) di <a href="http://www.fowdb.altervista.org/index.php?p=card&code=CMF-067+C" class="inline_link" target="_blank">Il Cacciatore nella Foresta Nera</a> alla prima pescata con mulligan di 5 carte (cioè come pescare 10 carte), con 4x Il Cacciatore nella Foresta Nera nel mazzo, mazzo da 40 carte.
					</span>
				</div>
				<div class="hypcalc_example">
					<button class="hypcalc_example_button" id="hypcalc_ex3">Esempio 3</button>
					<span class="hypcalc_example_desc">
						Probabilità di avere almeno due pietre magiche oscurità per giocare <a href="http://www.fowdb.altervista.org/index.php?p=card&code=TAT-088+R" class="inline_link" target="_blank">Lapidare a Morte</a> nel turno 4, avendo 6 pietre magiche di oscurità su 10 nel mazzo delle pietre magiche. Nota: i successi pescati devono essere da 2 a 4 per avere sufficiente volontà e si assume che il giocatore convochi una pietra a turno.
					</span>
				</div>
				<div class="hypcalc_example">
					<button class="hypcalc_example_button" id="hypcalc_ex4">Esempio 4</button>
					<span class="hypcalc_example_desc">
						Probabilità di controllare <a href="http://www.fowdb.altervista.org/index.php?p=card&code=TAT-100+SR" class="inline_link" target="_blank">Moojdart, la Pietra Fantastica</a> entro il turno 4 avendone una sola copia in un mazzo delle pietre magiche da 10 carte.
					</span>
				</div>
			</div>
			
		</div>
	</div>
<?php endif; ?>