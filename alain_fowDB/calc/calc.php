<html>
	<head>
		<!-- Open Graph Project meta tags -->
		<?php $page = UI_HEADER_NAVLINK_CALC; include '_includes/ogp.php'; ?>
		
		<!-- Title -->
		<title><?=$ogp['title']?></title>

		<!-- Meta tags -->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Scripts -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="_scripts/common.js"></script>
		<script src="_scripts/calc.js"></script>

		<!-- Style -->
		<link href="_styles/reset.css" rel="stylesheet" type="text/css">
		<link href="_styles/style.css" rel="stylesheet" type="text/css">

		<!-- Favicon -->
		<link rel="icon" href="_images/icons/favicon.png" type="image/png"/>
	</head>
	
	<body>
		<?php
			echo '<div id="wrapper">';
			
				// Header
				include '_includes/header.php';
				
				// Content
				echo '<div id="content">';
					
					// Hypergeometric Calculator
					include 'calc.html.php';
					
				echo '</div>';
			
				// Footer
				include '_includes/footer.php';
			
			echo '</div>';
		?>
	</body>
</html>