<?php
	switch ($_SESSION['lang']) {
		case 'en':
			$title = 'FoWDB - Work in Progress...';
			$msg_title = 'Work in Progress...';
			$msg = 'We\'re updating the website, please visit us later or try to refresh the page. In the meantime, here\'s a list of websites you might like';
			$enSelect = " langSelect";
			$itSelect = "";
			break;
		case 'it':
			$title = 'FoWDB - Lavori in corso...';
			$msg_title = 'Lavori in corso...';
			$msg = 'Stiamo aggiornando il sito, vieni a trovarci più tardi o prova ad aggiornare la pagina. Nel frattempo ecco un elenco di siti che potrebbero interessarti';
			$enSelect = "";
			$itSelect = " langSelect";
			break;
	}
?>
<html>
	<head>
		<!-- Title -->
		<title><?=$title?></title>

		<!-- Meta tags -->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- Scripts -->
		<script src="_scripts/common.js"></script>

		<!-- Style -->
		<link href="_styles/reset.css" rel="stylesheet" type="text/css">
		<link href="_styles/style.css" rel="stylesheet" type="text/css">

		<!-- Favicon -->
		<link rel="icon" href="_images/icons/favicon.png" type="image/png"/>
	</head>
	
	<body>
		<div id="wrapper">
			
			<!-- BANNER ALTERVISTA -->
			<div class="ads">
				<script type="text/javascript">
					/* <![CDATA[ */
					google_color_border = "#bdbdbd";
					google_color_bg = "#ffffff";
					google_color_link = "#000000";
					google_color_url = "#08088a";
					google_color_text = "#000000";
					google_ui_features = "rc:6";
					document.write('<s'+'cript type="text/javascript" src="http://ad.altervista.org/js.ad/size=728X90/r='+new Date().getTime()+'"><\/s'+'cript>');
					/* ]]> */
				</script>
			</div>
		
			
			<div id="header">
				<!-- Select language -->
				<div id="lang">
					<a href="?lang=en" class="lang<?=$enSelect?>">English</a>
					<a href="?lang=it" class="lang<?=$itSelect?>">Italiano</a>
				</div>
				
				<!-- Logo -->
				<div id="logo">
					<a href="index.php">
						<!-- The url refers to index.php, the controller -->
						<img src="_images/logo.png"/>
					</a>
				</div>
			</div>
			
			
			<div id="content">
				<div id="links" class="sections">
					<div class="header">
						<div class="title"><?=$msg_title?></div>
					</div>
					<div class="content">
						<?php echo $msg; ?>
						<ul class="links">
							<li>
								<a href="http://fowtcg.com" target="_blank">Force of Will TCG Official Website</a>
								<div class="link_description">
									Official international website
								</div>
							</li>
							<li>
								<a href="http://www.fowtcg.it" target="_blank">Force of Will TCG Italia</a>
								<div class="link_description">
									Sito ufficiale italiano
								</div>
							</li>
							<li>
								<a href="http://www.fowdecks.com/forum" target="_blank">FoWrum by fowdecks.com</a>
								<div class="link_description">
									Comunità italiana di FoW TCG. Discussioni riguardo mazzi, metagame, regole ed eventi
								</div>
							</li>
							<li>
								<a href="http://www.fowtools.com" target="_blank">FoW Tools</a>
								<div class="link_description">
									Card database and deckbuilder
								</div>
							</li>
							<li>
								<a href="http://www.force-of-will.com" target="_blank">Force-Of-Will.com - Card Database</a>
								<div class="link_description">
									Card database and deckbuilder
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			
			<div id="footer">
				<ul class="links">
					Links
					<li> >> <a href="http://fowtcg.com" target="_blank">Force of Will TCG Official Website</a></li>
					<li>| <a href="http://www.fowtcg.it" target="_blank">Force of Will TCG Italia</a></li>
					<li>| <a href="http://www.fowdecks.com/forum" target="_blank">FoW Decks FoWrum</a></li>
					<li>| <a href="http://www.fowtools.com" target="_blank">FoW Tools</a></li>
					<li>| <a href="http://www.force-of-will.com" target="_blank">Force-Of-Will.com</a> |</li>
				</ul>
				
				<br />
				
				<ul>
					<li>| Copyright © 2015 </li>
					<li>| FoWDB - Force of Will Database </li>
					<li>| Alain D'Ettorre </li>
					<li>| <a href="mailto:alain.det@gmail.com">alain.det@gmail.com |</a> </li><br />
				</ul>
			</div>
			
		</div>
	</body>
</html>