<?php

// Default
$filter = "false";

// Get code
if (isset($_GET['code'])) {
	$code = htmlspecialchars($_GET['code'], ENT_QUOTES, 'UTF-8');
	
	// Filter
	$filter = "cardcode = \"" . $code . "\"";
}

// Get name
if (isset($_GET['name'])) {
	$name = htmlspecialchars($_GET['name'], ENT_QUOTES, 'UTF-8');
	
	// Separate name's words
	$name_words = explode(" ", $name);
	
	// Filter
	$filter = "cardname LIKE \"%";
		foreach ($name_words as $word) { $filter .= $word . "%"; }
	$filter .= "\"";
}


// Build sql
$sql = "SELECT block, setcode, cardcode, attribute, attributecost, freecost, totalcost, rarity, cardtype, atk, def, cardname, subtype_race, cardtext, flavortext, image_path, thumb_path " .
	"FROM cards_basic INNER JOIN cards_" . $_SESSION['lang'] .
			" ON cards_basic.id = cards_" . $_SESSION['lang'] .".id " .
	"WHERE " . $filter . " LIMIT 10"; // TESTING: only 10 results at a time are permitted

try {
	$query = $pdo->query($sql);
	$result = $query->fetchAll();
}
catch (PDOException $e) {
	$error = UI_ERROR_DB . " - " . $e->getMessage();
	include '_template/error.php';
	exit();
}

// If result is not empty
if ($query->rowCount() > 0) {
	
	$results = true;
	$cards = array();
	
	foreach ($result as $row) {
		
		// Fill NULL values with empty string
		foreach($row as $key => $value) {
			if ($value == "") {
				$row[$key] = "";
			}
		}
		
		// COST
		//-------------------------------------------------
		!empty($row['freecost'])
			? $freecost = "<img class=\"costicons\" src=\"_images/icons/free" . $row['freecost'] . ".png\" />"
			: $freecost = "";
			
		if (!empty($row['attributecost'])) {
			$attributecost = "";
			// Return array from string (1 character = 1 element)
			$attrcost = str_split($row['attributecost']);
			foreach($attrcost as $attr) {
				$attributecost .= "<img class=\"mark\" src=\"_images/icons/" . $attr . ".png\"" .
					" alt=\"" . constant("UI_CARD_ATTRIBUTES_" . strtoupper($attr)) . "\" />";
			}
		}
		else {
			$attributecost = "no";
		}
		
		if(empty($row['totalcost'])) {
			$cost = "0";
			$totalcost = "0";
		}
		else {
			$cost = $attributecost . $freecost;
		}
		//-------------------------------------------------
		
		
		
		// ATTRIBUTE
		//-------------------------------------------------
		// If there's the slash /, it's a multi-attribute card
		if (!empty($row['attribute'])) {
			if (strpos($row['attribute'], "/") !== FALSE) {
				
				$attributes = explode("/", $row['attribute']);
				for ($i = 0, $last = count($attributes); $i < $last; $i++) {
					// Translate attributes
					$attributes[$i] = constant("UI_CARD_ATTRIBUTES_" . strtoupper($attributes[$i]));
				}
				$attribute = implode("/", $attributes);
				
			}
			else {
				$attribute = constant("UI_CARD_ATTRIBUTES_" . strtoupper($row['attribute']));
			}
		}
		else {
			$attribute = "no";
		}
		//-------------------------------------------------
		
		
		
		// SUBTYPE/RACE
		//-------------------------------------------------
		if (
			$row['cardtype'] == 'Ruler' ||
			$row['cardtype'] == 'J-Ruler' ||
			$row['cardtype'] == 'Resonator'
		) {
			$subtype_race = 'race';
		}
		else {
			$subtype_race = 'subtype';
		}
		
		// Format
		$spoiler_card = false;
		
		// Check if the card is a spoiler card
		for ($i = 0; $i < count($spoiler_sets); $i++) {
			if ($row['setcode'] == $spoiler_sets[$i]['setcode']) {
				$spoiler_card = true;
				break;
			}
		}
		if ($spoiler_card) {
			$format = '<span class="mark_spoiler">Spoiler</span>';
		}
		else {
			$format = "";
			foreach ($formats as $frmt => $frmt_sets) {
				if (in_array($row['setcode'], $frmt_sets)) {
					$format .= $frmt . ", ";
				}
			}
			$format = substr($format, 0, strlen($format) - 2);
		}
		//-------------------------------------------------
		
		
		
		// TEXT - Replace symbols
		//-------------------------------------------------
		$text = marked_text($row['cardtext']);
		
		
		// SET
		//-------------------------------------------------
		$set = strtoupper($row['setcode']) ." - " .
					 constant("UI_BLOCK" . $row['block'] . "_" . strtoupper($row['setcode']));
		
		
		// Everything else
		$card = array (
			'name' => $row['cardname'],
			'cost' => $cost,
			'totalcost' => $row['totalcost'],
			'attribute' => $attribute,
			'type' => $types[$row['cardtype']],
			$subtype_race => $row['subtype_race'],
			'text' => $text,
			'atk_def' => $row['atk'] . ' / ' . $row['def'],
			'set' => $set,
			'code' => $row['cardcode'],
			'rarity' => strtoupper($row['rarity']),
			'format' => $format,
			'flavor' => "<span class='flavortext'>" . $row['flavortext'] . "</span>",
			'image_path' => $row['image_path'],
			'thumb_path' => $row['thumb_path']
			// Add flavor text
		);
	
		// Unset unwanted properties for some card types
		if ($row['flavortext'] == "") {
			unset($card['flavor']);
		}
		if (
			$row['cardtype'] == 'Ruler' ||
			$row['cardtype'] == 'J-Ruler' ||
			$row['cardtype'] == 'Magic Stone' ||
			$row['cardtype'] == 'Special Magic Stone' ||
			$row['cardtype'] == 'Special Magic Stone/True Magic Stone'
		) {
			unset($card['cost']);
			unset($card['totalcost']);
		}
		if (
			$row['cardtype'] == 'Ruler' ||
			$row['cardtype'] == 'Addition:Resonator' ||
			$row['cardtype'] == 'Addition:Resonator/J-Ruler' ||
			$row['cardtype'] == 'Addition:Ruler/J-Ruler' ||
			$row['cardtype'] == 'Addition:Field' ||
			$row['cardtype'] == 'Spell:Chant' ||
			$row['cardtype'] == 'Spell:Chant-Instant' ||
			$row['cardtype'] == 'Spell:Chant-Standby' ||
			$row['cardtype'] == 'Regalia' ||
			$row['cardtype'] == 'Magic Stone' ||
			$row['cardtype'] == 'Special Magic Stone' ||
			$row['cardtype'] == 'Special Magic Stone/True Magic Stone'
		) {
			unset($card['atk_def']);
		}
		if (
			$row['cardtype'] == 'Spell:Chant' ||
			$row['cardtype'] == 'Spell:Chant-Instant' ||
			$row['cardtype'] == 'Spell:Chant-Standby' ||
			$row['cardtype'] == 'Magic Stone' ||
			$row['cardtype'] == 'Special Magic Stone' ||
			$row['cardtype'] == 'Special Magic Stone/True Magic Stone'
		) {
			unset($card['subtype']);
		}
		if (
			$row['cardtype'] == 'Magic Stone' ||
			$row['cardtype'] == 'Special Magic Stone' ||
			$row['cardtype'] == 'Special Magic Stone/True Magic Stone'
		) {
			unset($card['attribute']);
		}
		
		// Add card to cards
		$cards[] = $card;
	}
}
else {
	$cards = array();
	$notif = UI_VIEWER_NORESULTS;
	$results = false;
}


// Marked text function
function marked_text ($txt) {
	
	// Rest
	$txt = str_replace('{Rest}', '<img class="mark" src="_images/icons/rest.png" />', $txt);
	$txt = str_replace('{Spossa}', '<img class="mark" src="_images/icons/rest.png" />', $txt);
	
	// Attributes
	$txt = preg_replace("/{([wrugbm])}/", "<img class='mark' src='_images/icons/$1.png' />", $txt);
	
	// Free will
	$txt = preg_replace("/{([\dx])}/", "<img class='mark' src='_images/icons/free$1.png' />", $txt);
	
	// Symbol skills
	$txt = preg_replace("/\[_(.+?)_\]/", "<span class='mark_skills'>$1</span>", $txt);
	
	// Break
	$txt = preg_replace("/^\[Break\](.+?)(<br \/>|$)/", "<span class='mark_break'>Break</span> <span class='mark_breaktext'>$1</span><br />", $txt);
	
	// Abilities
	$txt = preg_replace("/\[([^\d^:^+]+?)\]/", "<span class='mark_abilities'>$1</span>", $txt);
	
	// Errata
	$txt = preg_replace("/-ERRATA-(.+?)-ERRATA-/", "<span class='mark_errata'>$1</span>", $txt);
	
	return $txt;
} 