<?php include '_includes/db_connect.php'; ?>
<?php include 'cardpage.sql.php'; // Retrieve info from db ?>

<html prefix="og: http://ogp.me/ns#">
	<head>
		<!-- Open Graph Project meta tags -->
		<?php $page = "Card not found"; include '_includes/ogp.php'; ?>
		
		<!-- Title -->
		<title><?=$ogp['title']?></title>
		
		<!-- Other Meta tags -->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Scripts -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="_scripts/common.js"></script>
		
		<!-- LIGHTBOX -->
		<script src="_scripts/lightbox/js/lightbox.min.js" charset="utf-8"></script>
		<link href="_scripts/lightbox/css/lightbox.css" type="text/css" rel="stylesheet" title="Featherlight Styles" />

		<!-- Style -->
		<link href="_styles/reset.css" rel="stylesheet" type="text/css">
		<link href="_styles/style.css" rel="stylesheet" type="text/css">

		<!-- Favicon -->
		<link rel="icon" href="_images/icons/favicon.png" type="image/png"/>
	</head>
	
	<body>
		<?php
			echo '<div id="wrapper">';
			
				// Header
				include '_includes/header.php';
				
				// Content
				echo '<div id="content">';
					
					// Card image and info
					include 'cardpage/cardpage.html.php';
					
				echo '</div>';
			
				// Footer
				include '_includes/footer.php';
			
			echo '</div>';
		?>
	</body>
</html>