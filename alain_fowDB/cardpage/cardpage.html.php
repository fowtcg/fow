<div class="cardpage"><!--
	<?php if ($results): ?>
		<?php foreach($cards as $card): ?>
			--><div class="cardimage"><!--
				--><a href="<?=$card['image_path']?>" data-lightbox="<?=$card['code']?>" data-title="<?=$card['name']?>"><!--
					--><img src="<?=$card['image_path']?>" alt="<?=$card['name']?>" /><!--
				--></a><!--
				<?php
					// Remove image and thumb path so I can iterate on $card array later
					unset($card['image_path']);
					unset($card['thumb_path']);
				?>
			--></div><!--
			
			--><div class="cardinfo"><!--
					<?php foreach ($card as $prop => $val): ?>
						--><div class="cardprop"><!--
							--><div class="propLabel"><?php if ($prop == "atk_def"){echo "ATK/DEF";} else{echo constant("UI_CARD_LABEL_" . strtoupper($prop));}?></div><!--
							--><div class="propValue <?php if ($prop == "text") { echo "cardtext"; }?>"><?=$val?></div><!--
						--></div><!--
					<?php endforeach; ?>
			--></div><!--
		<?php endforeach; ?>
	<?php else: ?>
		--><div style="padding: 1em; text-align: center;"><?=UI_VIEWER_NORESULTS?></div><!--
	<?php endif; ?>
--></div>