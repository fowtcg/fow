<div id="links" class="sections">
	<div class="header">
		<div class="title"><?=UI_LINK_TITLE?></div>
	</div>
	<div class="content">
		<ul class="links">
			<li>
				<a href="http://fowtcg.com" target="_blank">Force of Will TCG Official Website</a>
				<div class="link_description">
					Official international website
				</div>
			</li>
			<li>
				<a href="http://www.fowtcg.it" target="_blank">Force of Will TCG Italia</a>
				<div class="link_description">
					Sito ufficiale italiano
				</div>
			</li>
			<li>
				<a href="http://www.fowdecks.com/forum" target="_blank">FoWrum by fowdecks.com</a>
				<div class="link_description">
					Comunità italiana di FoW TCG. Discussioni riguardo mazzi, metagame, regole ed eventi
				</div>
			</li>
			<li>
				<a href="http://www.fowtools.com" target="_blank">FoW Tools</a>
				<div class="link_description">
					Card database and deckbuilder
				</div>
			</li>
			<li>
				<a href="http://www.force-of-will.com" target="_blank">Force-Of-Will.com - Card Database</a>
				<div class="link_description">
					Card database and deckbuilder
				</div>
			</li>
		</ul>
	</div>
</div>