<html>
	<head>
		<!-- Open Graph Project meta tags -->
		<?php $page = UI_HEADER_NAVLINK_SPOILER; include '_includes/ogp.php'; ?>
		
		<!-- Title -->
		<title><?=$ogp['title']?></title>

		<!-- Meta tags -->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Scripts -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="_scripts/common.js"></script>
		<script src="_scripts/search.js"></script>

		<!-- Style -->
		<link href="_styles/reset.css" rel="stylesheet" type="text/css">
		<link href="_styles/style.css" rel="stylesheet" type="text/css">
		
		<!-- LIGHTBOX -->
		<script src="_scripts/lightbox/js/lightbox.min.js" charset="utf-8"></script>
		<link href="_scripts/lightbox/css/lightbox.css" type="text/css" rel="stylesheet" title="Featherlight Styles" />

		<!-- Favicon -->
		<link rel="icon" href="_images/icons/favicon.png" type="image/png"/>
	</head>
	
	<body>
		<?php
			echo '<div id="wrapper">';
			
				// Header
				include '_includes/header.php';
				
				// Content
				echo '<div id="content">';
				
					// Options
					echo '<div id="floatleft">';
						$is_spoiler = true;
						include 'search/options.html.php';
					echo '</div>';
					
					// Viewer
					echo '<div id="floatright">';
						include 'spoiler.html.php';
					echo '</div>';
					
				echo '</div>';
			
			// Footer
			include '_includes/footer.php';
		
			// Close wrapper
			echo '</div>';
		?>
	</body>
</html>