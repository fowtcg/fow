<?php

// Connect to DB
include '_includes/db_connect.php';

// Set cards container and number of spoiler sets

$cards = array();
$set_cards = array();

for($i = 0; $i < $spoiler_sets_count; $i++) {
	// Build final sql
	$sql = "SELECT cards_basic.id as id, cardcode, cardnum, cardname, cardtype, image_path, thumb_path " .
				 "FROM cards_basic INNER JOIN cards_" . $_SESSION['lang'] .
				 " ON cards_basic.id = cards_" . $_SESSION['lang'] .".id " .
				 "WHERE setcode=\"" . $spoiler_sets[$i]["setcode"] . "\"";
				
	// Try executing the query on db
	try {
		$query = $pdo->query($sql);
		$result = $query->fetchAll();
	}
	catch (PDOException $e) {
		$error = UI_ERROR_DB . " - " . $e->getMessage();
		include '_template/error.html.php';
		exit();
	}
	
	// Reset spoiler count and set_cards array
	$spoiled = 0;
	if (!empty($result)) {
		$set_cards = array();
		
		foreach($result as $row) {
			$set_cards[] = array (
				// $cards holds all columns fetched from db	
				'id' => $row['id'],
				'cardcode' => $row['cardcode'],
				'cardnum' => $row['cardnum'],
				'cardname' => $row['cardname'],
				'cardtype' => $row['cardtype'],
				'thumb_path' => $row['thumb_path'],
				'image_path' => $row['image_path']
			);
			
			if ($row['cardname'] != '-1' AND $row['cardtype'] != 'J-Ruler') {
				$spoiled++;
			}
		}
	}
	else {
		$set_cards = array();
		$notif = UI_VIEWER_NORESULTS;
		$results = false;
	}
	
	// Define spoiler set
	$set = array (
		"name" => $spoiler_sets[$i]['setname'],
		"code" => $spoiler_sets[$i]['setcode'],
		"spoiled" => $spoiled,
		"count" => $spoiler_sets[$i]['setcount'],
		"cards" => $set_cards
	);
	
	// Add set_cards to cards array
	$cards[] = $set;
	unset($set);
}