<?php

// COOKIES
define("UI_COOKIES_POLICY", "I cookie ci aiutano a fornirti i nostri servizi web. Continuando la navigazione sul sito, accetti l'utilizzo dei cookie da parte di quest'ultimo. <div class='notification_close'>OK</div>");

// ERRORS
define("UI_ERROR_TITLE", "Si è verificato un errore - FoWDB");
define("UI_ERROR_DB", "Impossibile collegarsi al database server");

// HEADER
define("UI_HEADER_NAVLINK_SPOILER","Spoiler");
define("UI_HEADER_NAVLINK_SEARCH","Cerca");
define("UI_HEADER_NAVLINK_DECK","Mazzo");
define("UI_HEADER_NAVLINK_CALC","Calc");
define("UI_HEADER_NAVLINK_DEMO","Demo");
define("UI_HEADER_NAVLINK_LINKS","Link");
define("UI_HEADER_NAVLINK_CONTACT","Contatti");

// FOOTER
define("UI_FOOTER_COPYRIGHT","Tutti i nomi, gli artwork e i concetti di gioco in questo sito sono proprietà di Force of Will Co, Ltd.");

// SECTION - LINKS
define("UI_LINK_TITLE","Siti consigliati");

// SECTION - CONTACT
define("UI_CONTACT_NAME","Il tuo nome");
define("UI_CONTACT_EMAIL","La tua email");
define("UI_CONTACT_MESSAGE","Il tuo messaggio");
define("UI_CONTACT_SEND","Invia");
define("UI_CONTACT_ERROR","Tutti i campi sono necessari. Il nome non può contenere punteggiatura, l''email dev''essere valida e il messaggio non può essere vuoto.");
define("UI_CONTACT_SENT","Email inviata.");

// SECTION - SEARCH - SEARCHBOX
define("UI_SEARCHBOX_PLACEHOLDER","Nome, codice, testo, sottotipo, razza..");
define("UI_SEARCHBOX_ABILITY","Entrata");
define("UI_SEARCHBOX_SKILL","Volare");
define("UI_SEARCHBOX_REST","Spossa");

// SECTION - SEARCH - SYNTAX
define("UI_SEARCHBOX_QUERYEX","La barra qui in alto effettua una ricerca in ogni nome, codice, testo, sottotipo e razza del database. Utilizza i filtri sottostanti per raffinare la ricerca. Esempi:");
define("UI_SEARCHBOX_OR","<span class=\"queryex\">risonatore bersaglio</span> = tutte le carte con almeno \"risonatore\" o 'bersaglio\"");
define("UI_SEARCHBOX_LITERAL","<span class=\"queryex\">risonatore_bersaglio</span> = solo carte con esattamente  \"risonatore bersaglio\"");
define("UI_SEARCHBOX_AND","<span class=\"queryex\">risonatore&bersaglio</span> = tutte le carte con \"risonatore\" e \"bersaglio\" contemporaneamente");

// SECTION - SEARCH - FILTERS
define("UI_FILTERS_TITLE","Filtri");
define("UI_FILTERS_CHOOSESET","Scegli un set..");
define("UI_FILTERS_ORDERBY","Ordina per");
define("UI_FILTERS_ORDERBY_DESC","Discendente");
define("UI_FILTERS_EXCLUDE_SPOILERS","Escludi carte spoiler");
define("UI_FILTERS_SUBMIT","Cerca");

// SET NAMES
define("UI_BLOCK","Blocco");
define("UI_BLOCK1_S","Starter");
define("UI_BLOCK1_1","L'Alba del Valhalla");
define("UI_BLOCK1_2","La Guerra del Valhalla");
define("UI_BLOCK1_3","");

define("UI_BLOCK2_CMF","La Fiaba della Luna Scarlatta");
define("UI_BLOCK2_TAT","Il Castello Volante e le Due Torri");
define("UI_BLOCK2_MPR","Il Ritorno della Sacerdotessa della Luna");
define("UI_BLOCK2_MOA","Le Ere Millenarie");
define("UI_BLOCK2_VIN001","Vingolf series - Engage Knights");
define("UI_BLOCK2_VS01","Faria, la Sacra Regina/Melgis, il Re della Fiamma");

// SECTION - SEARCH - OPTIONS
define("UI_OPTIONS_TITLE","Opzioni");
define("UI_OPTIONS_NUMXROW_TITLE","Carte per riga");
define("UI_OPTIONS_INFO_OPEN","Apri pagina info");
define("UI_OPTIONS_ZOOM_ZOOMONCLICK","Zoom al click");
define("UI_OPTIONS_MISSING_TITLE","Carte mancanti");
define("UI_OPTIONS_MISSING","Mostra carte mancanti come coperte");
define("UI_OPTIONS_SELECT_TITLE","Seleziona");
define("UI_OPTIONS_SELECT_SELCARDS","Attiva selezione");
define("UI_OPTIONS_SELECT_SELALL","Seleziona tutto");
define("UI_OPTIONS_SELECT_DESELALL","Deseleziona tutto");
define("UI_OPTIONS_SELECT_SHOWSEL","Mostra solo selezionate");
define("UI_OPTIONS_SELECT_SHOWALL","Mostra tutte");
define("UI_OPTIONS_SELECT_SAVESEL","Salva selezione");

// SECTION - SEARCH - VIEWER
define("UI_VIEWER_TITLE","Risultati");
define("UI_VIEWER_NORESULTS","Nessun risultato trovato, prova a cambiare i criteri di ricerca.");

// CARD - LABELS
define("UI_CARD_LABEL_SET","Set");
define("UI_CARD_LABEL_FORMAT","Formato");
define("UI_CARD_LABEL_CODE","Codice");
define("UI_CARD_LABEL_NUMBER","Numero");
define("UI_CARD_LABEL_ATTRIBUTE","Attributo");
define("UI_CARD_LABEL_COST","Costo");
define("UI_CARD_LABEL_ATTRIBUTECOST","Costo di Attributo");
define("UI_CARD_LABEL_FREECOST","Costo Libero");
define("UI_CARD_LABEL_TOTALCOST","Costo Totale");
define("UI_CARD_LABEL_RARITY","Rarità");
define("UI_CARD_LABEL_ATK","Attacco");
define("UI_CARD_LABEL_DEF","Difesa");
define("UI_CARD_LABEL_NAME","Nome");
define("UI_CARD_LABEL_SUBTYPE_RACE","Sottotipo o Razza");
define("UI_CARD_LABEL_SUBTYPE","Sottotipo");
define("UI_CARD_LABEL_RACE","Razza");
define("UI_CARD_LABEL_TEXT","Testo");
define("UI_CARD_LABEL_TYPE","Tipo");
define("UI_CARD_LABEL_FLAVOR","Testo di colore");

// CARD - ATTRIBUTES
define("UI_CARD_ATTRIBUTES_W","Luce");
define("UI_CARD_ATTRIBUTES_R","Fuoco");
define("UI_CARD_ATTRIBUTES_U","Acqua");
define("UI_CARD_ATTRIBUTES_G","Vento");
define("UI_CARD_ATTRIBUTES_B","Oscurità");
define("UI_CARD_ATTRIBUTES_V","Vuoto");
define("UI_CARD_ATTRIBUTES_M","Luna");

// CARDS - TYPES
define("UI_CARD_TYPES_RULER","Sovrano");
define("UI_CARD_TYPES_JRULER","Sovrano-G");
define("UI_CARD_TYPES_RESONATOR","Risonatore");
define("UI_CARD_TYPES_ADD_RESONATOR","Addizione:Risonatore");
define("UI_CARD_TYPES_ADD_J_RESONATOR","Addizione:Risonatore/Sovrano-G");
define("UI_CARD_TYPES_ADD_J_RULER","Addizione:Sovrano/Sovrano-G");
define("UI_CARD_TYPES_ADD_FIELD","Addizione:Terreno");
define("UI_CARD_TYPES_SPELL_CHANT","Magia:Canto");
define("UI_CARD_TYPES_SPELL_INSTANT","Magia:Canto-Istantanea");
define("UI_CARD_TYPES_SPELL_STANDBY","Magia:Canto-Attesa");
define("UI_CARD_TYPES_REGALIA","Emblema");
define("UI_CARD_TYPES_MAGICSTONE","Pietra Magica");
define("UI_CARD_TYPES_SPECIAL_MAGICSTONE","Pietra Magica Speciale");
define("UI_CARD_TYPES_TRUE_MAGICSTONE","Pietra Magica Speciale/Vera Pietra Magica");