<?php

// COOKIES
define("UI_COOKIES_POLICY", "Cookies help us provide our web services. By navigating this website, you accept cookies from it. <div class='notification_close'>OK</div>");

// ERRORS
define("UI_ERROR_TITLE", "An error occurred - FoWDB");
define("UI_ERROR_DB", "Unable to connect to the database server");

// HEADER
define("UI_HEADER_NAVLINK_SPOILER","Spoiler");
define("UI_HEADER_NAVLINK_SEARCH","Search");
define("UI_HEADER_NAVLINK_DECK","Deck");
define("UI_HEADER_NAVLINK_CALC","Calc");
define("UI_HEADER_NAVLINK_DEMO","Demo");
define("UI_HEADER_NAVLINK_LINKS","Links");
define("UI_HEADER_NAVLINK_CONTACT","Contact");

// FOOTER
define("UI_FOOTER_COPYRIGHT","All card names, artwork, and intrinsic Force of Will game concepts are copyright to Force of Will Co., Ltd.");

// SECTION - LINKS
define("UI_LINK_TITLE","Recommended websites");

// SECTION - CONTACT
define("UI_CONTACT_NAME","Your name");
define("UI_CONTACT_EMAIL","Your email");
define("UI_CONTACT_MESSAGE","Your message");
define("UI_CONTACT_SEND","Send");
define("UI_CONTACT_ERROR","All fields are required. Name cannot contain punctuation, email must be valid, message must not be empty.");
define("UI_CONTACT_SENT","Email sent.");

// SECTION - SEARCH - SEARCHBOX
define("UI_SEARCHBOX_PLACEHOLDER","Name, code, text, subtype, race..");
define("UI_SEARCHBOX_ABILITY","Enter");
define("UI_SEARCHBOX_SKILL","Flying");
define("UI_SEARCHBOX_REST","Rest");

// SECTION - SEARCH - SYNTAX			
define("UI_SEARCHBOX_QUERYEX","The above searchbar searches for your query in every card name, code, text, subtype and race from the database. Select some filters below to fine tune your search results. Examples:");
define("UI_SEARCHBOX_OR","<span class=\"queryex\">target resonator</span> = every card with \"target\" or \"resonator\" in it");
define("UI_SEARCHBOX_LITERAL","<span class=\"queryex\">target_resonator</span> = every card with exactly \"target resonator\" in it");
define("UI_SEARCHBOX_AND","<span class=\"queryex\">target&resonator</span> = every card with both \"target\" and \"resonator\" in it, no particular order");

// SECTION - SEARCH - FILTERS
define("UI_FILTERS_TITLE","Filters");
define("UI_FILTERS_CHOOSESET","Choose a set..");
define("UI_FILTERS_ORDERBY","Order by");
define("UI_FILTERS_ORDERBY_DESC","Descendent");
define("UI_FILTERS_EXCLUDE_SPOILERS","Exclude spoiled cards");
define("UI_FILTERS_SUBMIT","Search");

// SET NAMES
define("UI_BLOCK","Block");
define("UI_BLOCK1_S","Starter");
define("UI_BLOCK1_1","The Dawn of Valhalla");
define("UI_BLOCK1_2","The War of Valhalla");
define("UI_BLOCK1_3","The Shaft of Light of Valhalla");

define("UI_BLOCK2_CMF","The Crimson Moon Fairy Tale");
define("UI_BLOCK2_TAT","The Castle of Heaven and the Two Towers");
define("UI_BLOCK2_MPR","The Moon Priestess Returns");
define("UI_BLOCK2_MOA","The Millennia of Ages");
define("UI_BLOCK2_VIN001","Vingolf series - Engage Knights");
define("UI_BLOCK2_VS01","Faria, the Sacred Queen/Melgis, the Flame King");

// SECTION - SEARCH - OPTIONS
define("UI_OPTIONS_TITLE","Options");
define("UI_OPTIONS_NUMXROW_TITLE","Cards per row");
define("UI_OPTIONS_INFO_OPEN","Open info page");
define("UI_OPTIONS_ZOOM_ZOOMONCLICK","Zoom on click");
define("UI_OPTIONS_MISSING_TITLE","Missing cards");
define("UI_OPTIONS_MISSING","Show missing cards as covered");
define("UI_OPTIONS_SELECT_TITLE","Select");
define("UI_OPTIONS_SELECT_SELCARDS","Activate selection");
define("UI_OPTIONS_SELECT_SELALL","Select all");
define("UI_OPTIONS_SELECT_DESELALL","Deselet all");
define("UI_OPTIONS_SELECT_SHOWSEL","Show only selected");
define("UI_OPTIONS_SELECT_SHOWALL","Show all");
define("UI_OPTIONS_SELECT_SAVESEL","Save selection");

// SECTION - SEARCH - VIEWER
define("UI_VIEWER_TITLE","Results");
define("UI_VIEWER_NORESULTS","No results were found, try changing your search criteria.");

// CARD - LABELS
define("UI_CARD_LABEL_SET","Set");
define("UI_CARD_LABEL_FORMAT","Format");
define("UI_CARD_LABEL_CODE","Code");
define("UI_CARD_LABEL_NUMBER","Number");
define("UI_CARD_LABEL_ATTRIBUTE","Attribute");
define("UI_CARD_LABEL_COST","Cost");
define("UI_CARD_LABEL_ATTRIBUTECOST","Attribute Cost");
define("UI_CARD_LABEL_FREECOST","Free Cost");
define("UI_CARD_LABEL_TOTALCOST","Total Cost");
define("UI_CARD_LABEL_RARITY","Rarity");
define("UI_CARD_LABEL_ATK","Attack");
define("UI_CARD_LABEL_DEF","Defense");
define("UI_CARD_LABEL_NAME","Name");
define("UI_CARD_LABEL_SUBTYPE_RACE","Subtype or Race");
define("UI_CARD_LABEL_SUBTYPE","Subtype");
define("UI_CARD_LABEL_RACE","Race");
define("UI_CARD_LABEL_TEXT","Text");
define("UI_CARD_LABEL_TYPE","Type");
define("UI_CARD_LABEL_FLAVOR","Flavor text");

// CARD - ATTRIBUTES
define("UI_CARD_ATTRIBUTES_W","Light");
define("UI_CARD_ATTRIBUTES_R","Fire");
define("UI_CARD_ATTRIBUTES_U","Water");
define("UI_CARD_ATTRIBUTES_G","Wind");
define("UI_CARD_ATTRIBUTES_B","Dark");
define("UI_CARD_ATTRIBUTES_V","Void");
define("UI_CARD_ATTRIBUTES_M","Moon");

// CARD - TYPES
define("UI_CARD_TYPES_RULER","Ruler");
define("UI_CARD_TYPES_JRULER","J-Ruler");
define("UI_CARD_TYPES_RESONATOR","Resonator");
define("UI_CARD_TYPES_ADD_RESONATOR","Addition:Resonator");
define("UI_CARD_TYPES_ADD_J_RESONATOR","Addition:J/Resonator");
define("UI_CARD_TYPES_ADD_J_RULER","Addition:Ruler/J-Ruler");
define("UI_CARD_TYPES_ADD_FIELD","Addition:Field");
define("UI_CARD_TYPES_SPELL_CHANT","Spell:Chant");
define("UI_CARD_TYPES_SPELL_INSTANT","Spell:Chant-Instant");
define("UI_CARD_TYPES_SPELL_STANDBY","Spell:Chant-Standby");
define("UI_CARD_TYPES_REGALIA","Regalia");
define("UI_CARD_TYPES_MAGICSTONE","Magic Stone");
define("UI_CARD_TYPES_SPECIAL_MAGICSTONE","Special Magic Stone");
define("UI_CARD_TYPES_TRUE_MAGICSTONE","Special Magic Stone/True Magic Stone");