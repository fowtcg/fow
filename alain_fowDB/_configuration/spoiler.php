<?php

$spoiler_sets = array(
	array(
		"setcode" => "moa",
		"setname" => UI_BLOCK2_MOA,
		"setcount" => "55"
	)
	,array (
		"setcode" => "vin001",
		"setname" => UI_BLOCK2_VIN001,
		"setcount" => "89"
	)
	,array (
		"setcode" => "vs01",
		"setname" => UI_BLOCK2_VS01,
		"setcount" => "32"
	)
);

$spoiler_sets_setcodes = array();
$spoiler_sets_setnames = array();
$spoiler_sets_count = count($spoiler_sets);

foreach ($spoiler_sets as $set) {
	$spoiler_sets_setcodes[] = $set['setcode'];
	$spoiler_sets_setnames[] = $set['names'];
}