<?php

// LOCALHOST or ONLINE (ALTERVISTA)
// --------------------------------------------------
$local_dev = true;
$domain = "http://www.fowdb.altervista.org/";


// MAINTENANCE
// --------------------------------------------------
$maintenance = false;


// LANGUAGE
// --------------------------------------------------

// Set default language (English)
if (empty($_SESSION['lang'])) { $_SESSION['lang'] = 'en'; }

// Get selected language if any
if(isset($_GET['lang'])) { $_SESSION['lang'] = $_GET['lang']; }

// Select translation

switch($_SESSION['lang']) {
	case 'en':
		include '_configuration/translations/en.php';
		
	case 'it':
		include '_configuration/translations/it.php';
}


// COOKIES
// --------------------------------------------------
if (!isset($_SESSION['accept cookies']) || $_SESSION['accept cookies'] != 'yes') {
	$notif = UI_COOKIES_POLICY;
}


// SET ROOT
// --------------------------------------------------

if ($local_dev) {
	$root = $_SERVER['DOCUMENT_ROOT'] . "/_fowdb/";
}
else {
	$root = "/membri/fowdb/";
}


// DATABASE
// --------------------------------------------------

if ($local_dev) {
	define("DB_NAME", "cards");
	define("DB_USER", "root");
	define("DB_PASSWORD", "c632t");
}
else {
	define("DB_NAME", "my_fowdb");
	define("DB_USER", "fowdb");
	define("DB_PASSWORD", "");
}


// SEARCH
// --------------------------------------------------
$results = false; // No results as default


// LEGALITY OF SETS AND SPOILER
// --------------------------------------------------
include '_configuration/legality.php';
include '_configuration/spoiler.php';


// FILTERS
// --------------------------------------------------

// Card features
$card_features = array(
	'setnum' => UI_CARD_LABEL_SET, // Set number as default
	'cardnum' => UI_CARD_LABEL_NUMBER,
	'subtype_race' => UI_CARD_LABEL_RACE,
	'attribute' => UI_CARD_LABEL_ATTRIBUTE,
	'totalcost' => UI_CARD_LABEL_TOTALCOST,
	'rarity' => UI_CARD_LABEL_RARITY,
	'cardtype' => UI_CARD_LABEL_TYPE,
    'atk' => UI_CARD_LABEL_ATK,
    'def' => UI_CARD_LABEL_DEF
);

$orderby_dirs = array(
	'asc' => UI_FILTERS_ORDERBY_ASC,
	'desc' => UI_FILTERS_ORDERBY_DESC,
);