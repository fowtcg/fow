<?php

// Start session
session_start();

// Check if there's an action and the action is "accept cookies"
if (isset($_POST['action']) && $_POST['action'] == 'accept cookies') {
	
	// Set cookie variable so that cookies policy don't show on next refresh
	$_SESSION['accept cookies'] = 'yes';
	$cookie = $_SESSION['accept cookies'];
	
	// Generate response
	$response = array (
		'status' => 'success',
		'message' => $cookie,
	);
	
	// Send Response
	$encoded = json_encode($response);
	header('Content-type: application/json');
	exit($encoded);
}