<?php

// Formats
$format_default = "New Frontiers";

// Add New Frontiers when it's time!
$formats = array(
	"New Frontiers" => array(
		'cmf',
		'tat',
		'mpr',
		'moa',
		'vin001',
		'vs01'
	),
	'Origin' => array(
		's',
		'1',
		'2',
		'3',
		'cmf',
		'tat',
		'mpr',
		'moa',
		'vin001',
		'vs01'
	),
	'Valhalla' => array (
		's',
		'1',
		'2',
		'3'
	),
);


// BLOCKS
$blocks = array(
	"1" => array(
		"label" => UI_BLOCK . " 1",
		"sets" => array(
			"s" => UI_BLOCK1_S,
			"1" => UI_BLOCK1_1,
			"2" => UI_BLOCK1_2,
			"3" => UI_BLOCK1_3
		)
	),
	"2" => array(
		"label" => UI_BLOCK . " 2",
		"sets" => array(
			"cmf" => UI_BLOCK2_CMF,
			"tat" => UI_BLOCK2_TAT,
			"mpr" => UI_BLOCK2_MPR,
			"moa" => UI_BLOCK2_MOA,
			"vin001" => UI_BLOCK2_VIN001,
			"vs01" => UI_BLOCK2_VS01
		)
	)
);

// ATTRIBUTES
$attributes = array (
	"w" => UI_CARD_ATTRIBUTES_W,
	"r" => UI_CARD_ATTRIBUTES_R,
	"u" => UI_CARD_ATTRIBUTES_U,
	"g" => UI_CARD_ATTRIBUTES_G,
	"b" => UI_CARD_ATTRIBUTES_B,
	"v" => UI_CARD_ATTRIBUTES_V
);

// COSTS
$costs = array ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10');

// TYPES
$types = array (
	"Ruler" => UI_CARD_TYPES_RULER,
	"J-Ruler" => UI_CARD_TYPES_JRULER,
	"Resonator" => UI_CARD_TYPES_RESONATOR,
	"Spell:Chant" => UI_CARD_TYPES_SPELL_CHANT,
	"Spell:Chant-Instant" => UI_CARD_TYPES_SPELL_INSTANT,
	"Spell:Chant-Standby" => UI_CARD_TYPES_SPELL_STANDBY,
	"Addition:Resonator" => UI_CARD_TYPES_ADD_RESONATOR,
	"Addition:J/Resonator" => UI_CARD_TYPES_ADD_J_RESONATOR,
	"Addition:Ruler/J-Ruler" => UI_CARD_TYPES_ADD_J_RULER,
	"Addition:Field" => UI_CARD_TYPES_ADD_FIELD,
	"Regalia" => UI_CARD_TYPES_REGALIA,
	"Magic Stone" => UI_CARD_TYPES_MAGICSTONE,
	"Special Magic Stone" => UI_CARD_TYPES_SPECIAL_MAGICSTONE,
	"Special Magic Stone/True Magic Stone" => UI_CARD_TYPES_TRUE_MAGICSTONE
);

// RARITIES
// Decomment when you add promo cards to db
//$rarities = array ("sr", "r", "u", "c", "s", "pr");
$rarities = array ("sr", "r", "u", "c", "s");