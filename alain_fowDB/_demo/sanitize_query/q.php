<html>
	<head>
	</head>
	<body>
		<div>
			<?php
				if (isset($_GET['q'])) {
					$q = htmlspecialchars($_GET['q'], ENT_QUOTES, 'UTF-8');
					
					print_r(get_html_translation_table(HTML_SPECIALCHARS, ENT_QUOTES));
					
					echo 'query (raw): ' . $_GET['q'] . '<br />';
					echo 'query (escaped): ' . $q . '<br />';
				}
			?>
		</div>
		<form method="GET" action="">
			<input type="text" name="q" id="q" />
			<input type="submit" id="submitInput" />
		</div>
		<div id="output"></div>
	</body>
</html>