<html>
	<head>
		<!-- Open Graph Project meta tags -->
			<?php $page = "Autocomplete (Demo)"; include '_includes/ogp.php'; ?>
		
		<!-- Title -->
			<title><?=$ogp['title']?></title>

		<!-- Meta tags -->
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Scripts -->
			<!-- jQuery -->
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
			<!-- jQuery UI (Core) -->
			<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
			<!-- FoWDB common -->
			<script src="_scripts/common.js"></script>
			<!-- Autocomplete -->
			<script src="_demo/autocomplete/autocomplete.js"></script>

		<!-- Style -->
			<link href="_styles/reset.css" rel="stylesheet" type="text/css">
			<link href="_styles/style.css" rel="stylesheet" type="text/css">
			<!-- jQuery UI (CSS) -->
			<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

		<!-- Favicon -->
			<link rel="icon" href="_images/icons/favicon.png" type="image/png"/>
	</head>
	
	<body>
		<div id="wrapper">
			<!-- Header -->
			<?php include '_includes/header.php'; ?>
			
			<!-- Content -->
			<div id="content">
				<div id="autocomplete" class="sections demo">
					<div class="header">
						<h1 class="title">Autocomplete (DEMO)</h1>
					</div>
					<div class="content">
						<p class="demo_desc">
							Type a card name into the input box, select it and go directly to the card's info page. Configuration:
								<ul>
									<li>- Automatic focus on input after page loads</li>
									<li>- Autocomplete starts in 300 milliseconds if there are two or more characters</li>
									<li>- Returns only the first 10 results</li>
									<li>- Returns only cards from Grimm block</li>
								</ul>
							To leave a feedback, <a class="feedback" href="?p=contact&subject=Autocomplete">click here</a>.
						</p>
						<form name="form_cardnames" style="text-align: center;">
							<input id="cardnames" name="cardnames" type="text" placeholder="Type card name here.." autofocus />
						</form>
						<p class="demo_version">Last update: 2015/07/12</p>
					</div>
			</div>
			
			<!-- Footer -->
			<?php include '_includes/footer.php'; ?>
		</div>
	</body>
</html>