$(document).ready(function () {
	$("#cardnames").autocomplete({
		// URL dependent from index.php in root folder
		// because index.php produced the autocomplete demo page
		source: "index.php?do=autocomplete"
		,select: function(event, ui) {
			var cardcode = ui.item.value.replace(" ", "+"),
					cardname = ui.item.label;
			url = window.location.href.substring(0, window.location.href.indexOf("?"));
			url += "?p=card" + "&code=" + cardcode;
			//url += "?p=card" + "&name=" + cardname;
			window.open(url, "_blank");
		}
		,delay: 300
		,minLength: 2
	});
});