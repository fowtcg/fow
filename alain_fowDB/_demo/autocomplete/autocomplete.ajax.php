<?php

// Connect to the database
include '_includes/db_connect.php';

// Get term of input
isset($_GET['term'])
	? $q = $_GET['term']
	:	$q = "";

// Build SQL statement
$sql = "SELECT cards_basic.id as id, cardcode, cardname " .
			 "FROM cards_basic INNER JOIN cards_" . $_SESSION['lang'] .
			 " ON cards_basic.id = cards_" . $_SESSION['lang'] . ".id " .
			 "WHERE cardname LIKE \"%" . $q . "%\" " . "AND block = 2 " .
			 "LIMIT 10";

// Try executing the query on db
try {
	$query = $pdo->query($sql);
	$result = $query->fetchAll();
}
catch (PDOException $e) {
	$error = UI_ERROR_DB . $e->getMessage();
	exit();
}

// If there are results
if (!empty($result)) {
	// Store results in $cardnames[] array
	foreach($result as $row) {
		//$cardnames[] = $row['cardname'];
		$cardnames[] = array(
			'id' => $row['id']
			,'label' => $row['cardname']
			,'value' => $row['cardcode']
		);
	}
}
else {
	$cardnames[] = array(
		'id' => '1'
		,'label' => 'No results'
		,'value' => 'No results'
	);
}

// Return the cardnames in JSON format
echo json_encode($cardnames);

// Save results on results.json external file
/*
$fp = fopen('results.json', 'w');
fwrite($fp, json_encode($cardnames));
fclose($fp);
*/