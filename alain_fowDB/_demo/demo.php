<html>
	<head>
		<!-- Open Graph Project meta tags -->
		<?php $page = UI_HEADER_NAVLINK_DEMO; include '_includes/ogp.php'; ?>
		
		<!-- Title -->
		<title><?=$ogp['title']?></title>

		<!-- Meta tags -->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Scripts -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="_scripts/common.js"></script>

		<!-- Style -->
		<link href="_styles/reset.css" rel="stylesheet" type="text/css">
		<link href="_styles/style.css" rel="stylesheet" type="text/css">

		<!-- Favicon -->
		<link rel="icon" href="_images/icons/favicon.png" type="image/png"/>
	</head>
	
	<body>
		<div id="wrapper">
		
			<!-- Header -->
			<?php include '_includes/header.php'; ?>
			
			<!-- Content -->
			<div id="content">
				<div id="autocomplete" class="sections">
					<div class="header">
						<h1 class="title">List of demo</h1>
					</div>
					<div class="content" style="text-align: center;">
						<ul class="section_list">
							<li><a href="?p=demo_autocomplete">Autocomplete</a></li>
						</ul>
					</div>
			</div>
			
			<!-- Footer -->
			<?php include '_includes/footer.php'; ?>
			
		</div>
	</body>
</html>