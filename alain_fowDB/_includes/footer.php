<div id="footer">
	<ul class="links">
		Links
		<li> >> <a href="http://fowtcg.com" target="_blank">Force of Will TCG Official Website</a></li>
		<li>| <a href="http://www.fowtcg.it" target="_blank">Force of Will TCG Italia</a></li>
		<li>| <a href="http://www.fowdecks.com/forum" target="_blank">FoW Decks FoWrum</a></li>
		<li>| <a href="http://www.fowtools.com" target="_blank">FoW Tools</a></li>
		<li>| <a href="http://www.force-of-will.com" target="_blank">Force-Of-Will.com</a> |</li>
	</ul>
	
	<br />
	
	<ul>
		<li>| Copyright © 2015 </li>
		<li>| FoWDB - Force of Will Database </li>
		<li>| Alain D'Ettorre </li>
		<li>| <a href="mailto:alain.det@gmail.com">alain.det@gmail.com |</a> </li><br />
		<li>| <?=UI_FOOTER_COPYRIGHT?> |</li>
	</ul>
</div>

<!-- BANNER ALTERVISTA -->
<div class="ads">
	<script type="text/javascript">
		/* <![CDATA[ */
		document.write('<s'+'cript type="text/javascript" src="http://ad.altervista.org/js.ad/size=300X250/r='+new Date().getTime()+'"><\/s'+'cript>');
		/* ]]> */
	</script>
</div>