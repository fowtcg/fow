<?php
	switch ($_SESSION['lang']) {
		case 'en':
			$enSelect = " langSelect";
			$itSelect = "";
			break;
		case 'it':
			$enSelect = "";
			$itSelect = " langSelect";
			break;
	}
?>

<!-- Top anchor -->
<a name="top"></a>

<!-- Notification -->
<?php if (isset($notif)) { include '_includes/notification.php'; } ?>

<!-- BANNER GOOGLE -->
<div class="ads">
	<script type="text/javascript">
		/* <![CDATA[ */
		document.write('<s'+'cript type="text/javascript" src="http://ad.altervista.org/js2.ad/size=728X90/r='+new Date().getTime()+'"><\/s'+'cript>');
		/* ]]> */
	</script>
</div>

<div id="header">
	
	<!-- Select language -->
	<div id="lang">
		<a data-lang="en" class="lang<?=$enSelect?>">English</a>
		<a data-lang="it" class="lang<?=$itSelect?>">Italiano</a>
	</div>

	<!-- Navigation -->
	<div id="nav">	
			<ul class="navlinks"><!--
				
				Spoiler
				--><li><a href="?p=spoiler" class="navlink spoiler"><?=UI_HEADER_NAVLINK_SPOILER?></a></li><!--
				
				Search
				--><li><a href="?p=search" class="navlink"><?=UI_HEADER_NAVLINK_SEARCH?></a></li><!--

				Calc
				--><li><a href="?p=calc" class="navlink"><?=UI_HEADER_NAVLINK_CALC?></a></li><!--
				
				Demo
				--><li><a href="?p=demo" class="navlink">Demo</a></li><!--
				
				Links
				--><li><a href="?p=links" class="navlink"><?=UI_HEADER_NAVLINK_LINKS?></a></li><!--
				
			--></ul>
	</div>
	
	<!-- Logo -->
	<div id="logo">
		<a href="index.php"">
			<img src="_images/logo.png"/>
		</a>
	</div>
</div>