<?php // Open Graph Project

global $results;
global $cards;
global $domain;
$ogp = array();

// Title
if(isset($results) and $results) {
	if($page == 'Card not found') {// It's the cardpage
		$ogp['title'] = $cards[0]['name'] . " || FoWDB - Force of Will Database";
	}
	else {
		$ogp['title'] = "FoWDB - Force of Will Database";
	}
}
else {
	$ogp['title'] = $page . " || FoWDB - Force of Will Database";
}

// Type
$ogp['type'] = 'article';

// Image
if (isset($results) and $results and count($cards) <= 2) {
	$ogp['image'] = array();
	foreach ($cards as $card) {
		$ogp['image'][] = $domain . $card['thumb_path'];
	}
	$ogp['image'][] = $domain . "_images/logosquare.jpg";
}
else {
	$ogp['image'] = $domain . "_images/logosquare.jpg";
	$ogp['image:width'] = "300";
	$ogp['image:height'] = "300";
}

// URL
$ogp['url'] = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; // URL

// Site name (optional)
$ogp['site_name'] = "FoWDB - Force of Will Database";

// Description
$ogp['description'] = "Browse all FoW TCG cards with FoWDB! Responsive design, search filters, syntax-supported searchbar, online tools, updated SPOILER section and more.";

$metas = "";

foreach($ogp as $property => $content) {
	if (is_array($content)) {
		foreach($content as $subcontent) {
			$metas .= '<meta property="og:' . $property . '" content="' . $subcontent . '" />';
		}
	}
	else {
		$metas .= '<meta property="og:' . $property . '" content="' . $content . '" />';
	}
}

echo $metas;