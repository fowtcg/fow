<?php

try {
	$pdo = new PDO('mysql:host=localhost;dbname=' . DB_NAME, DB_USER, DB_PASSWORD);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo->exec('SET NAMES "utf8"');
}
catch (PDOException $e) {
	$error = UI_ERROR_DB . " - " . $e->getMessage();
	include $root . '_template/error.php';
	exit();
}