<?php

// SESSION START
// --------------------------------------------------
session_start();

// CONFIGURATION
// --------------------------------------------------
include '_configuration/configuration.php';

// MAINTENANCE
// --------------------------------------------------
if ($maintenance) {
	include '_template/workinprogress.php';
	exit();
}

// PAGE SERVICE
// --------------------------------------------------
if(isset($_GET['p'])) {
		
	switch($_GET['p']) {
		
		case 'calc':
			include 'calc/calc.php';
			exit();
			
		case 'card':
			include 'cardpage/cardpage.php';
			exit();
			
		case 'customsearch':
			include 'search/search_custom.sql.php';
			exit();
			
		case 'deck':
			include 'deck/deck.php';
			exit();
		
		case 'links':
			include 'links/links.php';
			exit();
			
		case 'search':
			include 'search/search.php';
			exit();
			
		case 'spoiler':
			include 'spoiler/spoiler.php';
			exit();
			
		// DEMO
		case 'demo':
			include '_demo/demo.php';
			exit();
		
		case 'demo_autocomplete':
			include '_demo/autocomplete/autocomplete.php';
			exit();
	}
}

// ACTION - Display search results
// --------------------------------------------------
if (isset($_GET['do'])) {
	switch($_GET['do']) {
		case 'search':
			include 'search/search_retrievedb.php';
			exit();
		case 'autocomplete':
			include '_demo/autocomplete/autocomplete.ajax.php';
			exit();
	}
}

// DEFAULT
// --------------------------------------------------
include 'search/search.php';
exit();