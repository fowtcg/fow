<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Force of Will - FoW - Team - Add Cards</title>
		<meta charset="ISO-8859-1">	
		<meta name="author" content="Christian Schuler" />
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale = 0.1, maximum-scale=4.0, user-scalable=yes">
		<meta name="description" content="TCG - Force of Will - FoW - News - Errata - Card Database" />
		<link rel="shortcut icon" href="/pics/favicon.ico" />
		<link rel="stylesheet" href="/styles.css" type="text/css" />
		<link rel="stylesheet" href="/js/jquery-ui-1.11.0.custom/jquery-ui.css"/>		
		<style type="text/css"> 
			div.content td {
				text-align:center;
			}
		</style>
		<script type="text/javascript" src="/js/scripts.js"></script>	
		<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
		<script src="/js/jquery-ui-1.11.0.custom/external/jquery/jquery.js"></script>
		<script src="/js/jquery-ui-1.11.0.custom/jquery-ui.js"></script>		
		<script type="text/javascript">
			$(function() {
				$( "#name" ).autocomplete({
					minLength: 2,
					source:'/site/helper/searchname.php',
				})
				$( "#race" ).autocomplete({
					minLength: 2,
					source:'/site/helper/searchrace.php',
				})
				$( "#subtype" ).autocomplete({
					minLength: 2,
					source:'/site/helper/searchsubtype.php',
				})
			});
		</script>		
		<!--[if lt IE 9]>
			<script src="/js/html5shiv.js"></script>
			<link rel="stylesheet" href="/css/ie.css" type="text/css" />
		<![endif]-->
	</head>
	<body>
		<script type="text/javascript" src="/js/tooltip/wz_tooltip.js"></script>
		<div class="wrapper">
			<a href="/index.php"><div class="banner"></div></a>
  
			<!-- Beginning main navigation -->
				<p class="me-trigger-main">
					<a id="mainmenu">Main Navigation</a>
				</p>
				<nav id="main">
					<ul>
						<li><a href="/index.php">News</a></li>
						<li><a href="/site/rules/rules.htm">Rules</a></li>
						<li><a href="/site/card-db/card-db.php">Card Database</a></li>
						<li><a href="/site/deck/deckbuilder.php">Deckbuilder</a></li>
						<?php 
						// Connection Info
						$server = "localhost";
						$techuser = "83735_0.usr1";
						$password = "y74pEuLwQTEasNXZ";	
						$database = "83735_0";
						$loggedin = false;
						$ismember = false;
						$pic = "/non_existent.jpg";

						// Server Connection
						mysql_connect($server,$techuser,$password) or die ("No Connection");

						// phpBB User Info
						define('IN_PHPBB', true);
						$phpbb_root_path = '../../forum/';
						$phpEx = substr(strrchr(__FILE__, '.'), 1);
						include($phpbb_root_path . 'common.' . $phpEx);

						// Start session management
						$user->session_begin();
						$auth->acl($user->data);
						$user->setup();
						
						mysql_select_db($database) or die ("DB doesn't exist");
						
						if (($user->data['is_registered'])){
							$loggedin = true;
							$query = "SELECT 
									*
								FROM 
									phpbb_user_group, phpbb_users 
								WHERE 
									phpbb_users.username like '".$user->data['username']."' and phpbb_users.user_id = phpbb_user_group.user_id and phpbb_user_group.group_id = 9
								LIMIT 0 , 1";
							$result = mysql_query ($query) or die (mysql_error());
							while ($row = mysql_fetch_assoc($result)) {
								echo '<li><a href="/site/team/insertcard.php" class="active">Team</a></li>';
								$ismember = true;
							}
						}
						echo '
						<li><a href="/site/faq/faq.htm">FAQ</a></li>
						<li><a href="/site/about-me/about-me.htm">About Me</a></li>
					</ul>
				</nav>
			<!-- End main navigation -->

			<!-- Beginning sub navigation -->
				<p class="me-trigger-sub">
					<a id="submenu">Sub Navigation</a>
				</p>
				<nav id="sub">';
				if (!$loggedin){
					echo '<h2>Login</h2>';
					//user is not logged in  
					echo '
					<form method="POST" action="/forum/ucp.php?mode=login">
						<p>
						Username: <input type="text" name="username" size="40"><br />
						Password: <input type="password" name="password" size="40"><br />
						Remember Me?: <input type="checkbox" name="autologin"><input type="submit" value="Submit" name="login">
						</p>
						<input type="hidden" name="redirect" value="/site/team/updatecard.php?language='.$language.'&n1='.mysql_real_escape_string(urldecode($_GET['n1'])).'&n2='.mysql_real_escape_string(urldecode($_GET['n2'])).'"">
					</form>';
				} else {
					echo '
					<h2>Sub Navigation</h2>
					<ul>
						<li><a href="/site/team/insertcard.php">Insert New Card</a></li>
						<li><a href="/site/team/updatecard.php" class="active">Update Card</a></li>
						<li><a href="/site/team/addfaq.php">Add FAQ</a></li>
						<!--<li><a href="/site/team/updatefaq.php">Update FAQ</a></li>-->
					</ul>';
				}
				echo '
				</nav>
			<!-- End sub navigation-->

			<!-- Beginning content -->
				<div class="content">';
					if ($ismember){
						$language = "";
						$n1 = "";
						$n2 = 0;
						if (empty ($_GET['language']) && empty ($_GET['n1']) && empty ($_GET['n2']) ) { 
						echo '
						<form id="selectCard" name="selectCard" action="/site/team/updatecard.php">
							<p>
								<label for="language">Language:</label>
								<select name="language">
									<option value="en">English</option>  
									<option value="it">Italiano</option>  
									<option value="de">Deutsch</option> 
								</select>
							</p>
							<p>
								<label for="n1">Edition:</label>
								<select name="n1">
									<optgroup label="1st Cycle">
										<option value="1">Dawn of Valhalla</option>  
										<option value="2">War of Valhalla</option>  
										<option value="3">The Shaft of Light of Valhalla</option>  
									</optgroup>
									<optgroup label="2nd Cycle">
										<option value="4">The Crimson Moon\'s Fairy Tale</option>  	
										<option value="5">The Castle of Heaven and the Two Towers</option>										
									</optgroup>								
								</select>
							</p>
							<p>	
								<label for="n2">Cardnumber:</label>
								<input name="n2" type="number" size="10" required placeholder="Example: 13"/>
							</p>
							<p>
								<input type="submit" name="set" value="Search">
							</p>
						</form>';
						} else {
						
						$query = "SELECT 
							*
						FROM 
							`cards` 
						WHERE 
							language LIKE '".$_GET['language']."' and n1 LIKE '".$_GET['n1']."' and n2 = ".$_GET['n2']." ORDER BY type DESC";
						$result = mysql_query ($query);
						while ($row = mysql_fetch_assoc($result)) {
						echo '
						<form id="myform" name="myform" action="/site/team/saveupdatecard.php">
							<fieldset class="data">
        						<legend>Data</legend>
								<div class="left">
									<p>
										<label for="language">Language:</label>
										<select name="language">
											<option value="en" '.(($row['language']=='en')?'selected="selected"':"").'>English</option>  
											<option value="it" '.(($row['language']=='it')?'selected="selected"':"").'>Italiano</option>  
											<option value="de" '.(($row['language']=='de')?'selected="selected"':"").'>Deutsch</option> 
										</select>
									</p>							
									<p>
										<label for="edition">Edition:</label>
										<select name="edition">
											<optgroup label="1st Cycle">
												<option value="1" '.(($row['n1']=='1')?'selected="selected"':"").'>Dawn of Valhalla</option>  
												<option value="2" '.(($row['n1']=='2')?'selected="selected"':"").'>War of Valhalla</option>  
												<option value="3" '.(($row['n1']=='3')?'selected="selected"':"").'>The Shaft of Light of Valhalla</option>  
											</optgroup>
											<optgroup label="2nd Cycle">
												<option value="4" '.(($row['n1']=='4')?'selected="selected"':"").'>The Crimson Moon\'s Fairy Tale</option>  	
												<option value="5" '.(($row['n1']=='5')?'selected="selected"':"").'>The Castle of Heaven and the Two Towers</option>										
											</optgroup>								
									</select>
									</p>
									<p>	
										<label for="n2">Cardnumber:</label>
										<input id="n2" name="n2" type="number" size="10" required value="'.$row['n2'].'" placeholder="Example: 13"/>
									</p>
									<p>
										<label for="code">Code:</label>
										<input id="code" name="code" type="text" size="25" required value="'.$row['code'].'" placeholder="Example: \'CMF-053 C\'"/>						
									</p>
									<p>
										<label for="name">Card Name:</label>
										<input id="name" name="name" type="text" size="25" required value="'.$row['name'].'" placeholder="Card Name..."/>						
									</p>									
									<p>	
										<label for="type">Type:</label>
										<select id="type" name="type"> 
											<option value="Ruler" '.(($row['type']=='Ruler')?'selected="selected"':"").'>Ruler</option>  
											<option value="J-Ruler" '.(($row['type']=='J-Ruler')?'selected="selected"':"").'>J-Ruler</option> 								
											<option value="Resonator" '.(($row['type']=='Resonator')?'selected="selected"':"").'>Resonator</option>  
											<option value="Addition" '.(($row['type']=='Addition')?'selected="selected"':"").'>Addition</option>  
											<option value="Spell" '.(($row['type']=='Spell')?'selected="selected"':"").'>Spell</option>  
											option value="Magic Stone" '.(($row['type']=='Magic Stone')?'selected="selected"':"").'>Magic Stone</option> 
										</select>
									</p>	
									<div class="ui-widget">	
										<p>
											<label for="subtype">Subtype:</label>
											<input id="subtype" name="subtype" value="'.$row['subtype'].'" type="text" size="20" />
										</p>
										<p>
											<label for="race">Race:</label>
											<input id="race" name="race" type="text" value="'.$row['race'].'" size="25" placeholder="Race..."/>						
										</p>
									</div>
									<p>	
										<label for="attributecost">Attribute Cost:</label>
										<input id="attributecost" name="attributecost" required value="'.$row['attributecost'].'" type="number" size="5" />
									</p>	
									<p>	
										<label for="freecost">Free Cost:</label>
										<input id="freecost" name="freecost" required value="'.$row['freecost'].'" type="number" size="5" />
									</p>	
									<p>	
										<label for="att">ATT:</label>
										<input id="att" name="att" type="number" value="'.$row['att'].'" size="5" />
									</p>	
									<p>	
										<label for="def">DEF:</label>
										<input id="def" name="def" type="number" value="'.$row['def'].'" size="5" />
									</p>
									<p>
										<label for="pic">Picture-Path:</label>
										<input id="pic" name="pic" type="text" value="'.$row['pic'].'" size="25" />						
									</p>
								</div>
    							<div class="left">	
									<p>
										<label for="spoiler">Spoiler:</label><br />
										<input type="radio" name="spoiler" value="0" '.(($row['spoiler']=='0')?'checked="checked"':"").' /> Old Card, show only in Card-DB <br />
										<input type="radio" name="spoiler" value="1" '.(($row['spoiler']=='1')?'checked="checked"':"").' /> Spoiler Card, show in Card-DB and as Spoiler<br />
										<input type="radio" name="spoiler" value="2" '.(($row['spoiler']=='2')?'checked="checked"':"").' /> Brand new card - neither show as spoiler, nor in the DB!
									</p>					
									<p>
										<label for="rarity">Rarity:</label><br />
										<input type="radio" name="rarity" value="SR" '.(($row['rarity']=='SR')?'checked="checked"':"").' /> Super Rare<br />
										<input type="radio" name="rarity" value="R" '.(($row['rarity']=='R')?'checked="checked"':"").' /> Rare<br />
										<input type="radio" name="rarity" value="U" '.(($row['rarity']=='U')?'checked="checked"':"").' /> Uncommon<br />
										<input type="radio" name="rarity" value="C" '.(($row['rarity']=='C')?'checked="checked"':"").' /> Common<br />
										<input type="radio" name="rarity" value="PR" '.(($row['rarity']=='PR')?'checked="checked"':"").' /> Promo
									</p>
									<p>
										<label for="attribute">Attribute:</label><br />
										<input type="radio" name="attribute" value="Light" '.(($row['attribute']=='Light')?'checked="checked"':"").' /> Light<br />
										<input type="radio" name="attribute" value="Fire" '.(($row['attribute']=='Fire')?'checked="checked"':"").' /> Fire<br />
										<input type="radio" name="attribute" value="Water" '.(($row['attribute']=='Water')?'checked="checked"':"").' /> Water<br />
										<input type="radio" name="attribute" value="Wind" '.(($row['attribute']=='Wind')?'checked="checked"':"").' /> Wind<br />
										<input type="radio" name="attribute" value="Dark" '.(($row['attribute']=='Dark')?'checked="checked"':"").' /> Dark<br />
										<input type="radio" name="attribute" value="Void" '.(($row['attribute']=='Void')?'checked="checked"':"").' /> Void								
									</p>	
								</div>
    							<div class="clear">								
									<p>
										<label for="text">Rule Text:</label><br />
										<textarea name="text" cols="60" rows="10">'.$row['text'].'</textarea>
									</p>	
								</div>
							</fieldset>						
							<p>
								<input type="hidden" name="old_language" value="'.$_GET['language'].'">
								<input type="hidden" name="old_n1" value="'.$_GET['n1'].'">
								<input type="hidden" name="old_n2" value="'.$_GET['n2'].'">
								<input type="hidden" name="old_type" value="'.$row['type'].'">
								<input type="submit" name="save" value="Update Card">
							</p>
						</form>';
						$pic = $row['pic'];
						}
						}
					} else {
					echo "
					<h1>You are not logged in</h1>
					<p>Please <a href='http://force-of-will.com/forum/ucp.php?mode=register'><strong>register</strong></a> yourself and join us.<br />
					<img class='astema' src='/pics/joinus.png' />";
					}
					// Database Connection
					// SQL Statement
					//$req = "INSERT INTO `decklist`(`decknumber`, `deckname`, `username`, `description`) VALUES ('','".mysql_real_escape_string($_REQUEST['term'])."','".$user->data['username']."','bla')";
				echo '
					<!-- Beginning footer -->
						<footer>
							<ul>
								<li><a href="#" class="top">&uarr; Back to Top</a></li>
							</ul>
							<p class="copy">       
								<strong>| Copyright &#169; 2014 | Force-of-Will.com | Version 8.0 | by Christian Schuler</strong> <br /> 
								All card names, artwork, and intrinsic Force of Will game concepts are copyright to Force of Will Co., Ltd. and used with permission.
							</p>
						</footer>
					<!-- End footer -->
				</div>
			<!-- End content -->

			<!-- Beginning info area -->
				<aside>
					<div class="infobox">';
						$thumb_name = $_SERVER['DOCUMENT_ROOT'].$pic;
							if( file_exists($thumb_name)) {
								echo '<img src="'.$pic.'" width=346 />';
							} else {
								if($loggedin){
								echo "<h5>Login</h5>";
								echo '<p>';
								echo "Logged in as:<strong> " . $user->data['username']. "</strong><br />"; 
								echo '<br />';
								echo '
									<a href="' . append_sid("{$phpbb_root_path}ucp.$phpEx", 'mode=logout', true, $user->session_id). '">
										<input type="hidden" name="redirect" value="/index.php">
										<input type="submit" name="logout" value="Logout">
									</a><br />';
								echo '</p>';
								}
							}
					echo '		
					</div>
				</aside>
			<!-- End info area -->';
			?>
			<!-- Beginning Google Analytics -->
				<script>
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  					})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

					ga('create', 'UA-52004644-1', 'force-of-will.com');
					ga('send', 'pageview');
				</script>
			<!-- End Google Analytics -->	

			</div> 
	</body>
</html>