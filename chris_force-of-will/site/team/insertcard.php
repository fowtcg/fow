<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Force of Will - FoW - Team - Add Cards</title>
		<meta charset="ISO-8859-1">	
		<meta name="author" content="Christian Schuler" />
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale = 0.1, maximum-scale=4.0, user-scalable=yes">
		<meta name="description" content="TCG - Force of Will - FoW - News - Errata - Card Database" />
		<link rel="shortcut icon" href="/pics/favicon.ico" />
		<link rel="stylesheet" href="/styles.css" type="text/css" />
		<link rel="stylesheet" href="/js/jquery-ui-1.11.0.custom/jquery-ui.css"/>		
		<style type="text/css"> 
			div.content td {
				text-align:center;
			}
		</style>
		<script type="text/javascript" src="/js/scripts.js"></script>	
		<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
		<script src="/js/jquery-ui-1.11.0.custom/external/jquery/jquery.js"></script>
		<script src="/js/jquery-ui-1.11.0.custom/jquery-ui.js"></script>		
		<script type="text/javascript">
		$(function() {
			$( "#name" ).autocomplete({
				minLength: 2,
				source:'/site/helper/searchname.php',
			})
			$( "#race" ).autocomplete({
				minLength: 2,
				source:'/site/helper/searchrace.php',
			})
			$( "#subtype" ).autocomplete({
				minLength: 2,
				source:'/site/helper/searchsubtype.php',
			})
		});
		</script>		
		<!--[if lt IE 9]>
			<script src="/js/html5shiv.js"></script>
			<link rel="stylesheet" href="/css/ie.css" type="text/css" />
		<![endif]-->
	</head>
	<body>
		<script type="text/javascript" src="/js/tooltip/wz_tooltip.js"></script>
		<div class="wrapper">
			<a href="/index.php"><div class="banner"></div></a>
  
			<!-- Beginning main navigation -->
				<p class="me-trigger-main">
					<a id="mainmenu">Main Navigation</a>
				</p>
				<nav id="main">
					<ul>
						<li><a href="/index.php">News</a></li>
						<li><a href="/site/rules/rules.htm">Rules</a></li>
						<li><a href="/site/card-db/card-db.php">Card Database</a></li>
						<li><a href="/site/deck/deckbuilder.php">Deckbuilder</a></li>
						<?php 
						// Connection Info
						$server = "localhost";
						$techuser = "83735_0.usr1";
						$password = "y74pEuLwQTEasNXZ";	
						$database = "83735_0";
						$loggedin = false;
						$ismember = false;

						// Server Connection
						mysql_connect($server,$techuser,$password) or die ("No Connection");

						// phpBB User Info
						define('IN_PHPBB', true);
						$phpbb_root_path = '../../forum/';
						$phpEx = substr(strrchr(__FILE__, '.'), 1);
						include($phpbb_root_path . 'common.' . $phpEx);

						// Start session management
						$user->session_begin();
						$auth->acl($user->data);
						$user->setup();
						
						mysql_select_db($database) or die ("DB doesn't exist");
						
						if (($user->data['is_registered'])){
							$loggedin = true;
							$query = "SELECT 
									*
								FROM 
									phpbb_user_group, phpbb_users 
								WHERE 
									phpbb_users.username like '".$user->data['username']."' and phpbb_users.user_id = phpbb_user_group.user_id and phpbb_user_group.group_id = 9
								LIMIT 0 , 1";
							$result = mysql_query ($query) or die (mysql_error());
							while ($row = mysql_fetch_assoc($result)) {
								echo '<li><a href="/site/team/insertcard.php" class="active">Team</a></li>';
								$ismember = true;
							}
						}
						echo '
						<li><a href="/site/faq/faq.htm">FAQ</a></li>
						<li><a href="/site/about-me/about-me.htm">About Me</a></li>
					</ul>
				</nav>
			<!-- End main navigation -->

			<!-- Beginning sub navigation -->
				<p class="me-trigger-sub">
					<a id="submenu">Sub Navigation</a>
				</p>
				<nav id="sub">';
				if (!$loggedin){
					echo '<h2>Login</h2>';
					//user is not logged in  
					echo '
					<form method="POST" action="/forum/ucp.php?mode=login">
						<p>
						Username: <input type="text" name="username" size="40"><br />
						Password: <input type="password" name="password" size="40"><br />
						Remember Me?: <input type="checkbox" name="autologin"><input type="submit" value="Submit" name="login">
						</p>
						<input type="hidden" name="redirect" value="/site/team/insertcard.php">
					</form>';
				} else {
					echo '
					<h2>Sub Navigation</h2>
					<ul>
						<li><a href="/site/team/insertcard.php" class="active">Insert New Card</a></li>
						<li><a href="/site/team/updatecard.php">Update Card</a></li>
						<li><a href="/site/team/addfaq.php">Add FAQ</a></li>
						<!--<li><a href="/site/team/updatefaq.php">Update FAQ</a></li>-->
					</ul>';
				}
				echo '
				</nav>
			<!-- End sub navigation-->

			<!-- Beginning content -->
				<div class="content">';
					if ($ismember){
					echo '
					<form id="myform" name="myform" action="/site/team/saveinsertcard.php">
						<fieldset class="data">
        					<legend>Data</legend>
							<div class="left">
							<p>
								<label for="language">Language:</label>
								<select name="language">
									<option value="en">English</option>  
									<option value="it">Italiano</option>  
									<option value="de">Deutsch</option> 
								</select>
							</p>							
							<p>
								<label for="edition">Edition:</label>
								<select name="edition">
									<optgroup label="1st Cycle">
										<option value="1">Dawn of Valhalla</option>  
										<option value="2">War of Valhalla</option>  
										<option value="3">The Shaft of Light of Valhalla</option>  
									</optgroup>
									<optgroup label="2nd Cycle">
										<option value="4">The Crimson Moon\'s Fairy Tale</option>  	
										<option value="5">The Castle of Heaven and the Two Towers</option>										
									</optgroup>								
								</select>
							</p>
							<p>
								<label for="name">Card Name:</label>
								<input id="name" name="name" type="text" size="25" required placeholder="Card Name..."/>						
							</p>
							<p>	
								<label for="n2">Cardnumber:</label>
								<input id="n2" name="n2" type="number" size="10" required placeholder="Example: 13"/>
							</p>
							<p>
								<label for="code">Code:</label>
								<input id="code" name="code" type="text" size="25" required placeholder="Example: \'CMF-053 C\'"/>						
							</p>									
							<p>
								<label for="type">Type:</label>
								<select id="type" name="type"> 
									<option value="Ruler">Ruler</option>  
									<option value="J-Ruler">J-Ruler</option> 								
									<option value="Resonator">Resonator</option>  
									<option value="Addition">Addition</option>  
									<option value="Spell">Spell</option>  
									<option value="Magic Stone">Magic Stone</option>  
								</select>
							</p>	
							<div class="ui-widget">	
								<p>
									<label for="subtype">Subtype:</label>
									<input id="subtype" name="subtype" type="text" size="20" />
								</p>
								<p>
									<label for="race">Race:</label>
									<input id="race" name="race" type="text" size="25" placeholder="Race..."/>						
								</p>
							</div>
							<p>	
								<label for="attributecost">Attribute Cost:</label>
								<input id="attributecost" name="attributecost" required type="number" size="5" />
							</p>	
							<p>	
								<label for="freecost">Free Cost:</label>
								<input id="freecost" name="freecost" required type="number" size="5" />
							</p>	
							<p>	
								<label for="att">ATT:</label>
								<input id="att" name="att" type="number" size="5" />
							</p>	
							<p>	
								<label for="def">DEF:</label>
								<input id="def" name="def" type="number" size="5" />
							</p>
							</div>
    						<div class="left">	
							<p>
								<label for="spoiler">Spoiler:</label><br />
								<input type="radio" name="spoiler" value="0" /> Old Card, show only in Card-DB <br />
								<input type="radio" name="spoiler" value="1" /> Spoiler Card, show in Card-DB and as Spoiler<br />
								<input type="radio" name="spoiler" value="2" /> Brand new card - neither show as spoiler, nor in the DB!
							</p>					
							<p>
								<label for="rarity">Rarity:</label><br />
								<input type="radio" name="rarity" value="SR" /> Super Rare<br />
								<input type="radio" name="rarity" value="R" /> Rare<br />
								<input type="radio" name="rarity" value="U" /> Uncommon<br />
								<input type="radio" name="rarity" value="C" /> Common<br />
								<input type="radio" name="rarity" value="PR" /> Promo
							</p>
							<p>
								<label for="attribute">Attribute:</label><br />
								<input type="radio" name="attribute" value="Light" /> Light<br />
								<input type="radio" name="attribute" value="Fire" /> Fire<br />
								<input type="radio" name="attribute" value="Water" /> Water<br />
								<input type="radio" name="attribute" value="Wind" /> Wind<br />
								<input type="radio" name="attribute" value="Dark" /> Dark<br />
								<input type="radio" name="attribute" value="Void" /> Void								
							</p>	
							</div>
    						<div class="clear">								
							<p>
								<label for="text">Rule Text:</label><br />
								<textarea name="text" cols="60" rows="10"></textarea>
							</p>	
							</div>
						</fieldset>
						<p>
							<input type="submit" name="save" value="Save Card">
						</p>
					</form>';
					
					} else {
					echo "<h1>You are not logged in</h1><p>Please <a href='http://force-of-will.com/forum/ucp.php?mode=register'><strong>register</strong></a> yourself and join us.<br /><img class='astema' src='/pics/joinus.png' />";
				}
					// Database Connection
					// SQL Statement
					//$req = "INSERT INTO `decklist`(`decknumber`, `deckname`, `username`, `description`) VALUES ('','".mysql_real_escape_string($_REQUEST['term'])."','".$user->data['username']."','bla')";
			?>
				
					<!-- Beginning footer -->
						<footer>
							<ul>
								<li><a href="#" class="top">&uarr; Back to Top</a></li>
							</ul>
							<p class="copy">       
								<strong>| Copyright &#169; 2014 | Force-of-Will.com | Version 8.0 | by Christian Schuler</strong> <br /> 
								All card names, artwork, and intrinsic Force of Will game concepts are copyright to Force of Will Co., Ltd. and used with permission.
							</p>
						</footer>
					<!-- End footer -->
				</div>
			<!-- End content -->

			<!-- Beginning info area -->
				<aside>
					<div class="infobox">
						<h4>Info</h4>
						<h5>Last Update</h5>
						<p class="change">19.12.2014</p>
						<br />
						<h5>Search my website:</h5>
						<script>
							(function() {
							var cx = '004487255471104360343:9achku3grbs';
							var gcse = document.createElement('script');
							gcse.type = 'text/javascript';
							gcse.async = true;
							gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							'//www.google.com/cse/cse.js?cx=' + cx;
							var s = document.getElementsByTagName('script')[0];
							s.parentNode.insertBefore(gcse, s);
							})();
						</script>
						<gcse:search></gcse:search>
					</div>
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- force-of-will -->
					<ins class="adsbygoogle" style="display:inline-block;width:180px;height:150px" data-ad-client="ca-pub-9292681495337777" data-ad-slot="7421495048"></ins>
					<script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
				</aside>
			<!-- End info area -->

			<!-- Beginning Google Analytics -->
				<script>
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  					})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

					ga('create', 'UA-52004644-1', 'force-of-will.com');
					ga('send', 'pageview');
				</script>
			<!-- End Google Analytics -->	

			</div> 
	</body>
</html>