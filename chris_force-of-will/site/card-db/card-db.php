<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Force of Will - FoW - Card Database - Search</title>
		<meta charset="ISO-8859-1">	
		<meta name="author" content="Christian Schuler" />
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale = 0.1, maximum-scale=4.0, user-scalable=yes">
		<meta name="description" content="Card Database for the Trading Card Game - TCG - Force of Will - FoW - Rules - News - Errata - Card Database" />
		<link rel="shortcut icon" href="/pics/favicon.ico" />
		<link rel="stylesheet" href="/styles.css" type="text/css" />
		<link rel="stylesheet" href="/js/jquery-ui-1.11.0.custom/jquery-ui.css"/>		
		<style type="text/css"> 
			div.content td {
				text-align:center;
			}
		</style>
		<script type="text/javascript" src="/js/scripts.js"></script>	
		<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
		<script src="/js/jquery-ui-1.11.0.custom/external/jquery/jquery.js"></script>
		<script src="/js/jquery-ui-1.11.0.custom/jquery-ui.js"></script>		
		<script type="text/javascript">
		$(function() {
			$( "#name" ).autocomplete({
				minLength: 2,
				source:'/site/helper/searchname.php',
			})
			$( "#race" ).autocomplete({
				source:'/site/helper/searchrace.php',
			})
		});
		</script>			
		<!--[if lt IE 9]>
			<script src="/js/html5shiv.js"></script>
			<link rel="stylesheet" href="/css/ie.css" type="text/css" />
		<![endif]-->
	</head>
	<body>
		<script type="text/javascript" src="/js/tooltip/wz_tooltip.js"></script>
		<div class="wrapper">
			<a href="/index.php"><div class="banner"></div></a>
  
			<!-- Beginning main navigation -->
				<p class="me-trigger-main">
					<a id="mainmenu">Main Navigation</a>
				</p>
				<nav id="main">
					<ul>
						<li><a href="/index.php">News</a></li>
						<li><a href="/site/rules/rules.htm">Rules</a></li>
						<li><a href="/site/card-db/card-db.php" class="active">Card Database</a></li>
						<li><a href="/site/deck/deckbuilder.php">Deckbuilder</a></li>
						<?php 
						// Connection Info
						$server = "localhost";
						$techuser = "83735_0.usr1";
						$password = "y74pEuLwQTEasNXZ";	
						$database = "83735_0";
						$loggedin = false;
						$ismember = false;

						// Server Connection
						mysql_connect($server,$techuser,$password) or die ("No Connection");

						// phpBB User Info
						define('IN_PHPBB', true);
						$phpbb_root_path = '../../forum/';
						$phpEx = substr(strrchr(__FILE__, '.'), 1);
						include($phpbb_root_path . 'common.' . $phpEx);
						include($phpbb_root_path . 'includes/functions_display.' . $phpEx);						

						// Start session management
						$user->session_begin();
						$auth->acl($user->data);
						$user->setup();
						
						mysql_select_db($database) or die ("DB doesn't exist");
						
						// If user is registered User
						if (($user->data['is_registered'])){
							$loggedin = true;
							$query = "SELECT 
									*
								FROM 
									phpbb_user_group, phpbb_users 
								WHERE 
									phpbb_users.username like '".$user->data['username']."' and phpbb_users.user_id = phpbb_user_group.user_id and phpbb_user_group.group_id = 9
								LIMIT 0 , 1";
							$result = mysql_query ($query) or die (mysql_error());
							
							// If user is member of Group 9 (Team)
							while ($row = mysql_fetch_assoc($result)) {
								echo '<li><a href="/site/team/insertcard.php" class="team">Team</a></li>';
								$ismember = true;
							}	
						}
						echo '
						<li><a href="/site/faq/faq.htm">FAQ</a></li>
						<li><a href="/site/about-me/about-me.htm">About Me</a></li>
					</ul>
				</nav>
			<!-- End main navigation -->

			<!-- Beginning sub navigation -->
				<p class="me-trigger-sub">
					<a id="submenu">Sub Navigation</a>
				</p>
				<nav id="sub">
					<h2>Search Filter</h2>
					<form id="myform" name="myform" action="card-db.php">
						<p>
							<label for="language">Language:</label>
							<select id="language" name="language" style="width:100px" >
								<option value="en" '; if(urldecode($_GET['language']) == 'en'){echo("selected");} echo '>English</option>  
								<option value="it" '; if(urldecode($_GET['language']) == 'it'){echo("selected");} if (!$ismember) {echo "disabled";} echo '>Italian</option> 								
								<option value="fr" '; if(urldecode($_GET['language']) == 'fr'){echo("selected");} if (!$ismember) {echo "disabled";} echo '>French</option>   
							</select>
						</p>					
						<div class="ui-widget">
							<p>
								<label for="name">Cardname:</label>
								<input id="name" value="';echo urldecode($_GET['name']); echo '" name="name" type="text" size="24" autofocus="autofocus" placeholder="Name..."/>
							</p>
						</div>
						<p>
							<label for="edition">Edition:</label>
							<select name="edition">
								<option value="0">--- All ---</option>  
								<optgroup label="1st Cycle">
									<option '; if(urldecode($_GET['edition']) == 'Dawn of Valhalla'){echo("selected");} echo '>Dawn of Valhalla</option>  
									<option '; if(urldecode($_GET['edition']) == 'War of Valhalla'){echo("selected");} echo '>War of Valhalla</option>  
									<option '; if(urldecode($_GET['edition']) == 'The Shaft of Light of Valhalla'){echo("selected");} echo '>The Shaft of Light of Valhalla</option>  
								</optgroup>
								<optgroup label="2nd Cycle">
								<option '; if(urldecode($_GET['edition']) == 'The Crimson Moon\'s Fairy Tale'){echo("selected");} echo '>The Crimson Moon\'s Fairy Tale</option>  
								<option '; if(urldecode($_GET['edition']) == 'The Castle of Heaven and the Two Towers'){echo("selected");} echo '>The Castle of Heaven and the Two Towers</option> 
								<option '; if(urldecode($_GET['edition']) == 'The Moon Priestess Returns'){echo("selected");} echo '>The Moon Priestess Returns</option> 
								</optgroup>								
							</select>
						</p>
						<p>
							<label for="n2">Cardnumber:</label>
							<input id="n2" name="n2" value="'; echo urldecode($_GET['n2']); echo '" type="text" size="15" placeholder="Number..."/>						
						</p>
						<p>
							<label for="attribute">Attribute:</label>
							<select id="attribute" name="attribute">
								<option value="0">--- All ---</option>  
								<option '; if(urldecode($_GET['attribute']) == 'Light'){echo("selected");} echo '>Light</option>  
								<option '; if(urldecode($_GET['attribute']) == 'Fire'){echo("selected");} echo '>Fire</option>  
								<option '; if(urldecode($_GET['attribute']) == 'Water'){echo("selected");} echo '>Water</option>  
								<option '; if(urldecode($_GET['attribute']) == 'Wind'){echo("selected");} echo '>Wind</option>  
								<option '; if(urldecode($_GET['attribute']) == 'Dark'){echo("selected");} echo '>Dark</option> 	
								<option '; if(urldecode($_GET['attribute']) == 'Void'){echo("selected");} echo '>Void</option> 									
							</select>
						</p>				
						<p>
							<label for="type">Type:</label>
							<select id="type" name="type">
								<option value="0">--- All ---</option>  
								<option '; if(urldecode($_GET['type']) == 'Ruler'){echo("selected");} echo '>Ruler</option>  
								<option '; if(urldecode($_GET['type']) == 'J-Ruler'){echo("selected");} echo '>J-Ruler</option> 								
								<option '; if(urldecode($_GET['type']) == 'Resonator'){echo("selected");} echo '>Resonator</option>  
								<option '; if(urldecode($_GET['type']) == 'Addition'){echo("selected");} echo '>Addition</option>  
								<option '; if(urldecode($_GET['type']) == 'Spell'){echo("selected");} echo '>Spell</option>  
								<option '; if(urldecode($_GET['type']) == 'Magic Stone'){echo("selected");} echo '>Magic Stone</option>  
							</select>
						</p>	
						<p>
							<label for="subtype">Subtype:</label>
							<select id="subtype" name="subtype">
								<option value="0">--- All ---</option>  
								<optgroup label="Addition">
									<option '; if(urldecode($_GET['subtype']) == 'Field'){echo("selected");} echo '>Field</option>  
									<option '; if(urldecode($_GET['subtype']) == 'Resonator'){echo("selected");} echo '>Resonator</option>  
								</optgroup>
								<optgroup label="Spell">								
									<option '; if(urldecode($_GET['subtype']) == 'Chant'){echo("selected");} echo '>Chant</option>  
									<option '; if(urldecode($_GET['subtype']) == 'Instant'){echo("selected");} echo '>Instant</option>  
									<option '; if(urldecode($_GET['subtype']) == 'Standby'){echo("selected");} echo '>Standby</option>  
								</optgroup>
							</select>
						</p>
						<p>
							<label for="text">Rule Text:</label>
							<textarea id="text" name="text" width="100%" rows="2" placeholder="Cardtext...">'; echo urldecode($_GET['text']); echo '</textarea>
						</p>
						<p>
							<label for="race">Race:</label>
							<input id="race" name="race" type="text" value="'; echo urldecode($_GET['race']); echo '" size="25" placeholder="Race..."/>						
						</p>						
						<p>
							<label for="totalcost">Total Cost:</label>
							<select id="costsel" name="costsel">
								<option '; if(urldecode($_GET['costsel']) == '='){echo("selected");} echo '>=</option>  
								<option '; if(urldecode($_GET['costsel']) == '<'){echo("selected");} echo '>&lt;</option>  
								<option '; if(urldecode($_GET['costsel']) == '>'){echo("selected");} echo '>&gt;</option>  
							</select>
							<input id="totalcost" name="totalcost" type="text" value="'; echo urldecode($_GET['cost']); echo '" size="15" placeholder="Total Cost..."/>	
						</p>
						<p>
							<label for="att">ATT:</label>
							<select id="attsel" name="attsel">
								<option '; if(urldecode($_GET['attsel']) == '='){echo("selected");} echo '>=</option>  
								<option '; if(urldecode($_GET['attsel']) == '<'){echo("selected");} echo '>&lt;</option>  
								<option '; if(urldecode($_GET['attsel']) == '>'){echo("selected");} echo '>&gt;</option>  
							</select>							
							<input id="att" name="att" type="text" value="'; echo urldecode($_GET['att']); echo '" size="15" placeholder="ATT..."/>	
						</p>
						<p>
							<label for="def">DEF:</label>
							<select id="defsel" name="defsel">
								<option '; if(urldecode($_GET['defsel']) == '='){echo("selected");} echo '>=</option>  
								<option '; if(urldecode($_GET['defsel']) == '<'){echo("selected");} echo '>&lt;</option>  
								<option '; if(urldecode($_GET['defsel']) == '>'){echo("selected");} echo '>&gt;</option>   
							</select>							
							<input id="def" name="def" type="text" value="'; echo urldecode($_GET['def']); echo '" size="15" placeholder="DEF..."/>
						</p>
						<p>
							<label for="rarity">Rarity:</label>
							<select id="rarity" name="rarity">
								<option value="0">--- All ---</option>  
								<option '; if(urldecode($_GET['rarity']) == 'SR'){echo("selected");} echo '>SR</option>  
								<option '; if(urldecode($_GET['rarity']) == 'R'){echo("selected");} echo '>R</option>  
								<option '; if(urldecode($_GET['rarity']) == 'U'){echo("selected");} echo '>U</option>  
								<option '; if(urldecode($_GET['rarity']) == 'C'){echo("selected");} echo '>C</option> 
								<option value="S" '; if(urldecode($_GET['rarity']) == 'S'){echo("selected");} echo '>Starter</option>  
								<option value="PR" '; if(urldecode($_GET['rarity']) == 'PR'){echo("selected");} echo '>Promo</option> 								
							</select>
						</p>	
						<p>Symbol Skills:</p>
						<p>
							<input '; if(urldecode($_GET['pierce']) == '1'){echo("checked");} echo ' type="checkbox" name="pierce" value="1" id="pierce">
							<label for="pierce">[Pierce]</label><br />
							<input '; if(urldecode($_GET['target']) == '1'){echo("checked");} echo ' type="checkbox" name="target" value="1" id="target">
							<label for="target">[Target Attack]</label><br />
							<input '; if(urldecode($_GET['first']) == '1'){echo("checked");} echo ' type="checkbox" name="first" value="1" id="first">
							<label for="first">[First Strike]</label><br />
							<input '; if(urldecode($_GET['explode']) == '1'){echo("checked");} echo ' type="checkbox" name="explode" value="1" id="explode">
							<label for="explode">[Explode]</label><br />
							<input '; if(urldecode($_GET['flying']) == '1'){echo("checked");} echo ' type="checkbox" name="flying" value="1" id="flying">
							<label for="flying">[Flying]</label>
						</p>
						<p>
							<a href="card-db.php"><input type="button" name="reset" value="Reset"></a>
							<input type="submit" name="search" value="Search">
						</p>
					</form>
				</nav>
			<!-- End sub navigation-->

			<!-- Beginning content -->
				<div class="content">';
				
						$condition = '1=1';
						if (!empty ($_GET['language'])) { 
							switch ($_GET['language']) {
								case "en":
									$condition .= ' AND language LIKE "en"';
									break;
								case "it":
									$condition .= ' AND language LIKE "it"';
									break;
								case "fr":
									$condition .= ' AND language LIKE "fr"';
									break;
								case "de":
									$condition .= ' AND language LIKE "de"';
									break;									
								default:
							}
						}
						if (!empty ($_GET['name'])) { 
							$temp = str_replace ( ' ' , '%' , $_GET['name']);							
							$condition .= ' AND name LIKE "%'.$temp.'%"';
						}
						if (!empty ($_GET['edition'])) { 
							$condition .= ' AND edition LIKE "%'.$_GET['edition'].'%"';
						}
						if (!empty ($_GET['n2'])) { 
							$condition .= ' AND n2 = '.$_GET['n2'];
						}
						if (!empty ($_GET['attribute'])) { 
							$condition .= ' AND attribute LIKE "%'.$_GET['attribute'].'%"';
						}						
						if (!empty ($_GET['type'])) { 
							$condition .= ' AND type LIKE "'.$_GET['type'].'"';
						}						
						if (!empty ($_GET['subtype'])) { 
							$condition .= ' AND subtype LIKE "%'.$_GET['subtype'].'%"';
						}						
						if (!empty ($_GET['text'])) { 
							$temp = str_replace ( ' ' , '%' , $_GET['text']);
							$condition .= ' AND text LIKE "%'.$temp.'%"';
						}						
						if (!empty ($_GET['race'])) { 
							$condition .= ' AND race LIKE "%'.$_GET['race'].'%"';
						}						
						if (!empty ($_GET['totalcost'])) { 
							$condition .= ' AND totalcost '.$_GET['costsel'].' '.$_GET['totalcost'];
						}						
						if (!empty ($_GET['att'])) { 
							$condition .= ' AND att '.$_GET['attsel'].' '.$_GET['att'];
						}						
						if (!empty ($_GET['def'])) { 
							$condition .= ' AND def '.$_GET['defsel'].' '.$_GET['def'];
						}				 		
						if (!empty ($_GET['rarity'])) { 
							$condition .= ' AND rarity LIKE "'.$_GET['rarity'].'"';
						}
						if (!empty ($_GET['pierce'])) { 
							$condition .= ' AND text LIKE "%[Pierce]%"';
						}
						if (!empty ($_GET['target'])) { 
							$condition .= ' AND text LIKE "%[Target Attack]%"';
						}
						if (!empty ($_GET['first'])) { 
							$condition .= ' AND text LIKE "%[First Strike]%"';
						}
						if (!empty ($_GET['explode'])) { 
							$condition .= ' AND text LIKE "%[Explode]%"';
						}
						if (!empty ($_GET['flying'])) { 
							$condition .= ' AND text LIKE "%[Flying]%"';
						}
						$condition .= " AND spoiler not LIKE '2'";
						
						if (empty ($_GET['name']) && empty ($_GET['language']) && empty ($_GET['edition']) && empty ($_GET['n2']) && empty ($_GET['attribute']) && empty ($_GET['type']) && empty ($_GET['subtype']) && empty ($_GET['text']) && empty ($_GET['race']) && empty ($_GET['cost']) && empty ($_GET['att']) && empty ($_GET['def']) && empty ($_GET['rarity']) && empty ($_GET['pierce']) && empty ($_GET['target']) && empty ($_GET['first']) && empty ($_GET['explode']) && empty ($_GET['flying']) ) {
							$condition = '1=0';
							echo '
								<h1>Card Database</h1>
								<h2>Syntax</h2>
								<p>
									Rule Text Cost:<br />
									<img src="/pics/cost/light.png" alt="{W}" /> = {W}<br />
									<img src="/pics/cost/fire.png" alt="{R}" /> = {R}<br />
									<img src="/pics/cost/water.png" alt="{U}" /> = {U}<br />
									<img src="/pics/cost/wind.png" alt="{G}" /> = {G}<br />
									<img src="/pics/cost/darkness.png" alt="{B}" /> = {B}<br />
									<img src="/pics/cost/1-free-will.png" alt="{1}" /> = {1}<br />
									<br />
									<img src="/pics/cost/rest.png" alt="{Rest}" /> = {Rest}<br />
									<br >
									Abilities:
									<ul>
										<li>[Break]</li>
										<li>[Trigger]</li>
										<li>[Enter]</li>
										<li>[Continuous]</li>
									</ul>
									The same logic works to search for [Addition:Resonator] or [Race:Elf] in the Rule Text area.<br />
									<br />
									If you have a rule question to a specific card write me an <a href="mailto:xxxGrizzlxxx@gmail.com"><i>e-mail</i></a> and I will add it to the "Frequently Asked Questions"
								</p>';
						} else {


						//echo $condition;	

						$query = 'SELECT 
								language, n1, n2, name, pic 
							FROM 
								cards 
							WHERE 
								'.$condition.'
							ORDER BY 
								n1 ASC, n2 ASC, type DESC'; 
//							LIMIT 0, 10';
						$resultcount=mysql_query ("SELECT COUNT(DISTINCT name) FROM cards WHERE ".$condition);
						while($rowcount=mysql_fetch_array($resultcount)){ 
							echo '<h1>Search Result: '.$rowcount[0].' cards found</h1><br />'; // Count
						}
						$result = mysql_query ($query) ;
							echo '<table>';
							$i = 0;
							echo '<tr>';
							while ($row = mysql_fetch_assoc($result)) {
								if ($i == 4) {
									echo '<tr>';
								}
									echo '<td>';
										echo '<a href="card.php?l='.urlencode($row['language']).'&amp;n1='.urlencode($row['n1']).'&amp;n2='.urlencode($row['n2']).'">';									
										echo $row['n1'].'-'.str_pad($row['n2'], 3 ,'0', STR_PAD_LEFT); 
										echo '</a><br />';
										echo '<a href="card.php?l='.urlencode($row['language']).'&amp;n1='.urlencode($row['n1']).'&amp;n2='.urlencode($row['n2']).'">';			
										$thumb_name = $_SERVER['DOCUMENT_ROOT'].$row['pic'];
										if( file_exists($thumb_name)) {
											echo '<img src="'.$row['pic'].'" alt="'.$row['name'].'" width="120" onmouseover="Tip(\' <img src='.$row['pic'].' width=265 >\', BGCOLOR, \'#D6D6D6\', SHADOW, true, WIDTH, 265, SHADOWCOLOR, \'#4d4b4a\')" onmouseout="UnTip()" />';
										} else {
											echo '<img src="/pics/back.jpg" alt="'.$row['name'].'" width="120" onmouseover="Tip(\' <img src=/pics/back.jpg width=265 >\', BGCOLOR, \'#D6D6D6\', SHADOW, true, WIDTH, 265, SHADOWCOLOR, \'#4d4b4a\')" onmouseout="UnTip()" />';
										}
										echo '</a><br />';										
										echo '<a href="card.php?l='.urlencode($row['language']).'&amp;n1='.urlencode($row['n1']).'&amp;n2='.urlencode($row['n2']).'">';		
										echo $row['name'];
										echo '</a>';
									echo '</td>';
								if ($i == 3) {
									echo '</tr>';
									$i = 0;
								} else {
									$i++;
								}
							}
							echo '</tr>';
							echo '</table>';
							echo '<br />';
						}
					echo '

					<!-- Beginning footer -->
						<footer>
							<ul>
								<li><a href="#" class="top">&uarr; Back to Top</a></li>
							</ul>
							<p class="copy">       
								<strong>| Copyright &#169; 2014 | Force-of-Will.com | Version 8.0 | by Christian Schuler</strong> <br /> 
								All card names, artwork, and intrinsic Force of Will game concepts are copyright to Force of Will Co., Ltd. and used with permission.
							</p>
						</footer>
					<!-- End footer -->
				</div>
			<!-- End content -->

			<!-- Beginning info area -->
				<aside>
					<div class="infobox">
						<h4>Info</h4>';
						if($loggedin){
							//user is logged in  
							echo '<h5>Login</h5>';
							echo "Logged in as:<strong> " . $user->data['username']. "</strong><br />";  
							echo '
								<a href="' . append_sid("{$phpbb_root_path}ucp.$phpEx", 'mode=logout', true, $user->session_id). '">
								<input type="submit" name="logout" value="Logout"></a><br />';
							echo '<br />';
						} else {
							echo '<h5>Login</h5>';
							//user is not logged in  
							echo '
							<form method="POST" action="/forum/ucp.php?mode=login">
								<p>
									Username: <input type="text" name="username" size="40"><br />
									Password: <input type="password" name="password" size="40"><br />
									Remember Me?: <input type="checkbox" name="autologin"><input type="submit" value="Submit" name="login">
								</p>
								<input type="hidden" name="redirect" value="/site/card-db/card-db.php">
							</form><br />';
						}
						?>
						<h5>Last Update</h5>
						<p class="change">19.12.2014</p>
						<br />
						<h5>Search my website:</h5>
						<script>
							(function() {
							var cx = '004487255471104360343:9achku3grbs';
							var gcse = document.createElement('script');
							gcse.type = 'text/javascript';
							gcse.async = true;
							gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							'//www.google.com/cse/cse.js?cx=' + cx;
							var s = document.getElementsByTagName('script')[0];
							s.parentNode.insertBefore(gcse, s);
							})();
						</script>
						<gcse:search></gcse:search>
					</div>
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- force-of-will -->
					<ins class="adsbygoogle" style="display:inline-block;width:180px;height:150px" data-ad-client="ca-pub-9292681495337777" data-ad-slot="7421495048"></ins>
					<script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
				</aside>
			<!-- End info area -->
			
			<!-- Beginning Google Analytics -->
				<script>
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  					})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

					ga('create', 'UA-52004644-1', 'force-of-will.com');
					ga('send', 'pageview');
				</script>
			<!-- End Google Analytics -->			
		</div> 
	</body>
</html>