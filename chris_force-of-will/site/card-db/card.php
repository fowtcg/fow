<!DOCTYPE html>
<html lang="en">
	<head>
		<title>
		<?php 
			$user = '83735_0.usr1';
			$host = 'localhost';
			$password = 'y74pEuLwQTEasNXZ';
			$database = '83735_0';

			$db = mysql_connect($host, $user, $password) or die (mysql_error());
			mysql_select_db($database, $db) or die ("Couldn't connect to database.");
			$query = "SELECT 
					name 
				FROM 
					cards 
				WHERE 
					language LIKE 'en' and n1 LIKE '".mysql_real_escape_string(urldecode($_GET['n1']))."' and n2 = ".mysql_real_escape_string(urldecode($_GET['n2']))."
				LIMIT 0 , 1";
			$result = mysql_query ($query) or die (mysql_error());
			while ($row = mysql_fetch_assoc($result)) {
				echo $row['name'];
			}
		?> - Card Database - Force of Will - FoW - 
		</title>
		<meta charset="ISO-8859-1">	
		<meta name="author" content="Christian Schuler" />
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale = 0.1, maximum-scale=4.0, user-scalable=yes">
		<meta name="description" content="Rules for the Trading Card Game - TCG - Force of Will - FoW - News - Errata - Card Database" />
		<link rel="shortcut icon" href="/pics/favicon.ico" />
		<link rel="stylesheet" href="/styles.css" type="text/css" />
		<script type="text/javascript" src="/js/scripts.js"></script>
		<!--[if lt IE 9]>
			<script src="/js/html5shiv.js"></script>
			<link rel="stylesheet" href="/css/ie.css" type="text/css" />
		<![endif]-->
	</head>
	<body>
		<div class="wrapper">
			<a href="/index.php"><div class="banner"></div></a>
  
			<!-- Beginning main navigation -->
				<p class="me-trigger-main">
					<a id="mainmenu">Main Navigation</a>
				</p>
				<nav id="main">
					<ul>
						<li><a href="/index.php">News</a></li>
						<li><a href="/site/rules/rules.htm">Rules</a></li>
						<li><a href="/site/card-db/card-db.php" class="active">Card Database</a></li>
						<li><a href="/site/deck/deckbuilder.php">Deckbuilder</a></li>
						<?php 
						// Connection Info
						$server = "localhost";
						$techuser = "83735_0.usr1";
						$password = "y74pEuLwQTEasNXZ";	
						$database = "83735_0";
						$loggedin = false;
						$ismember = false;

						// Server Connection
						mysql_connect($server,$techuser,$password) or die ("No Connection");

						// phpBB User Info
						define('IN_PHPBB', true);
						$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : '../../forum/';
						$phpEx = substr(strrchr(__FILE__, '.'), 1);
						include($phpbb_root_path . 'common.' . $phpEx);
						include($phpbb_root_path . 'includes/functions_display.' . $phpEx);						

						// Start session management
						$user->session_begin();
						$auth->acl($user->data);
						$user->setup();
						
						mysql_select_db($database) or die ("DB doesn't exist");
						
						// If user is registered User
						if (($user->data['is_registered'])){
							$loggedin = true;
							$query = "SELECT 
									*
								FROM 
									phpbb_user_group, phpbb_users 
								WHERE 
									phpbb_users.username like '".$user->data['username']."' and phpbb_users.user_id = phpbb_user_group.user_id and phpbb_user_group.group_id = 9
								LIMIT 0 , 1";
							$result = mysql_query ($query) or die (mysql_error());
							
							// If user is member of Group 9 (Team)
							while ($row = mysql_fetch_assoc($result)) {
								echo '<li><a href="/site/team/insertcard.php" class="team">Team</a></li>';
								$ismember = true;
							}	
						}
						echo '
						<li><a href="/site/faq/faq.htm">FAQ</a></li>
						<li><a href="/site/about-me/about-me.htm">About Me</a></li>
					</ul>
				</nav>
			<!-- End main navigation -->

			<!-- Beginning sub navigation -->
				<p class="me-trigger-sub">
					<a id="submenu">Sub Navigation</a>
				</p>
				<nav id="sub">
					<h2>Card Database</h2>
					<p><input type=button value="Back" onClick="history.back()"></p>
				</nav>
			<!-- End sub navigation-->

			<!-- Beginning content -->
				<div class="content">
					';
						$name = '';
						
						$language = "en";
						if (!empty($_GET['l'])) { 
							$language = (mysql_real_escape_string(urldecode($_GET['l'])));
						}
						
						$query = 'SELECT 
									* 
								FROM 
									cards 
								WHERE 
									language LIKE "'.$language.'" AND n1 LIKE "'.mysql_real_escape_string(urldecode($_GET['n1'])).'" and n2 = '.mysql_real_escape_string(urldecode($_GET['n2'])).'
								ORDER BY type DESC';

						$result = mysql_query ($query) or die (mysql_error());
						while ($row = mysql_fetch_assoc($result)) {
							echo '<h1>'.$row['name']." ";
							if ($loggedin){
								echo ' <a href="/site/team/updatecard.php?language='.$language.'&n1='.mysql_real_escape_string(urldecode($_GET['n1'])).'&n2='.mysql_real_escape_string(urldecode($_GET['n2'])).'"><input type="submit" value="Edit Card" name="Edit Card"></a>';
							}
							echo '</h1>';
							$name = $row['name'];
							echo '<p class="left">';
							$thumb_name = $_SERVER['DOCUMENT_ROOT'].$row['pic'];
							if( file_exists($thumb_name)) {
								echo '<img src="'.$row['pic'].'" alt="'.$row['name'].'" width=346 />';
							} else {
								echo '<img src="/pics/back.jpg" alt="'.$row['name'].'" width=346 />';
							}
							echo '</p>';
							echo '<p class="right">	<table>';
								echo '<tr>
										<td style="width:30%"><strong>Name:</strong></td>
										<td style="width:70%">'.$row['name'].'</td>
									</tr>';
								echo '<tr>
										<td><strong>Attribute:</strong></td>								
										<td>'.$row['attribute'].'</td>
									</tr>';
								if ($row['type'] == 'Addition' || $row['type'] == 'Resonator' || $row['type'] == 'Spell') {
									echo '<tr>
											<td><strong>Total Cost:</strong></td>								
											<td>'.$row['totalcost'].'</td>
										</tr>';
								}
								echo '<tr>
										<td><strong>Type:</strong></td>								
										<td>'.$row['type'].'</td>
									</tr>';	
								if (!($row['subtype'] == '')) {
									echo '<tr>
											<td><strong>Subtype:</strong></td>								
											<td>'.$row['subtype'].'</td>
										</tr>';
								}
								if (!($row['race'] == '')) {
									echo '<tr>
											<td><strong>Race:</strong></td>								
											<td>'.$row['race'].'</td>
										</tr>';							
								}
								if (!($row['text'] == '')) {								
									echo '<tr>
											<td><strong>Cardtext:</strong></td>								
											<td>'.$row['text'].'</td>
										</tr>';
								} else {
									echo '<tr>
											<td><strong>Cardtext:</strong></td>								
											<td>-</td>
										</tr>';
								}
								if ($row['type'] == 'Resonator' || $row['type'] == 'J-Ruler') {
									echo '<tr>
											<td><strong>ATT:</strong></td>								
											<td>'.$row['att'].'</td>
										</tr>';
								}
								if ($row['type'] == 'Resonator' || $row['type'] == 'J-Ruler') {								
									echo '<tr>
											<td><strong>DEF:</strong></td>								
											<td>'.$row['def'].'</td>
										</tr>';		
								}
								echo '<tr>
										<td><strong>Rarity:</strong></td>								
										<td>'.$row['rarity'].'</td>
									</tr>';							
								echo '<tr>
										<td><strong>Set:</strong></td>								
										<td>'.$row['n1'].'-'.$row['edition'].'</td>
									</tr>';
								echo '<tr>
										<td><strong>Number:</strong></td>								
										<td>'.str_pad($row['n2'], 3 ,'0', STR_PAD_LEFT).'</td>
									</tr>';	
								echo '<tr>
										<td><strong>Released:</strong></td>								
										<td>'.$row['release'].'</td>
									</tr>';									
							echo '</table>';
							echo '</p>';
						}
						echo '<h1>Frequently Asked Questions';
						if ($loggedin){
							echo ' <a href="/site/team/addfaq.php?n1='.mysql_real_escape_string(urldecode($_GET['n1'])).'&n2='.mysql_real_escape_string(urldecode($_GET['n2'])).'"><input type="submit" value="Add FAQ" name="Add FAQ"></a>';
						}
						echo '</h1>';
						$query2 = 'SELECT 
									* 
								FROM 
									faq 
								WHERE
									n1 LIKE "'.mysql_real_escape_string(urldecode($_GET['n1'])).'" and n2 = '.mysql_real_escape_string(urldecode($_GET['n2']));
						$result2 = mysql_query ($query2) or die (mysql_error());
						while ($row2 = mysql_fetch_assoc($result2)) {
							echo '
								<h2>'.$row2['question'].'</h2>
								<p>'.$row2['answer'].'</p>
							';
						}
						echo '<p><a href="mailto:xxxGrizzlxxx@gmail.com"><i>E-mail</i></a></p>';
					
					echo '

					<!-- Beginning footer -->
						<footer>
							<ul>
								<li><a href="#" class="top">&uarr; Back to Top</a></li>
							</ul>
							<p class="copy">       
								<strong>| Copyright &#169; 2014 | Force-of-Will.com | Version 8.0 | by Christian Schuler</strong> <br />  
								All card names, artwork, and intrinsic Force of Will game concepts are copyright to Force of Will Co., Ltd. and used with permission.
							</p>
						</footer>
					<!-- End footer -->
				</div>
			<!-- End content -->

			<!-- Beginning info area -->
				<aside>
					<div class="infobox">
						<h4>Info</h4>';
						if($loggedin){
							//user is logged in  
							echo '<h5>Login</h5>';
							echo "Logged in as:<strong> " . $user->data['username']. "</strong><br />";  
							echo '
								<a href="' . append_sid("{$phpbb_root_path}ucp.$phpEx", 'mode=logout', true, $user->session_id). '">
								<input type="submit" name="logout" value="Logout"></a><br />';
							echo '<br />';
						} else {
							echo '<h5>Login</h5>';
							//user is not logged in  
							echo '
							<form method="POST" action="/forum/ucp.php?mode=login">
								<p>
									Username: <input type="text" name="username" size="40"><br />
									Password: <input type="password" name="password" size="40"><br />
									Remember Me?: <input type="checkbox" name="autologin"><input type="submit" value="Submit" name="login">
								</p>
								<input type="hidden" name="redirect" value="/site/card-db/card.php?l='.mysql_real_escape_string(urldecode($_GET['l'])).'&amp;n1='.mysql_real_escape_string(urldecode($_GET['n1'])).'&amp;n2='.mysql_real_escape_string(urldecode($_GET['n2'])).'">
							</form>';
						}
						?>
						<h5>Last Update</h5>
						<p class="change">19.12.2014</p>
						<br />
						<h5>Search my website:</h5>
						<script>
							(function() {
							var cx = '004487255471104360343:9achku3grbs';
							var gcse = document.createElement('script');
							gcse.type = 'text/javascript';
							gcse.async = true;
							gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							'//www.google.com/cse/cse.js?cx=' + cx;
							var s = document.getElementsByTagName('script')[0];
							s.parentNode.insertBefore(gcse, s);
							})();
						</script>
						<gcse:search></gcse:search>
					</div>
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- force-of-will -->
					<ins class="adsbygoogle" style="display:inline-block;width:180px;height:150px" data-ad-client="ca-pub-9292681495337777" data-ad-slot="7421495048"></ins>
					<script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
				</aside>
			<!-- End info area -->

			<!-- Beginning Google Analytics -->
				<script>
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  					})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

					ga('create', 'UA-52004644-1', 'force-of-will.com');
					ga('send', 'pageview');
				</script>
			<!-- End Google Analytics -->			
		</div> 
	</body>
</html>