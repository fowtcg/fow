<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Force of Will - FoW - Deckbuilder</title>
		<meta charset="ISO-8859-1">	
		<meta name="author" content="Christian Schuler" />
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale = 0.1, maximum-scale=4.0, user-scalable=yes">
		<meta name="description" content="Deck Builder for the Trading Card Game - TCG - Force of Will - FoW - News - Errata - Card Database" />
		<link rel="shortcut icon" href="/pics/favicon.ico" />
		<link rel="stylesheet" href="/styles.css" type="text/css" />
		<link rel="stylesheet" href="/js/jquery-ui-1.11.0.custom/jquery-ui.css"/>		
		<style type="text/css"> 
			div.content td {
				text-align:center;
			}
		</style>
		<script type="text/javascript" src="/js/scripts.js"></script>	
		<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
		<script src="/js/jquery-ui-1.11.0.custom/external/jquery/jquery.js"></script>
		<script src="/js/jquery-ui-1.11.0.custom/jquery-ui.js"></script>		
		<script type="text/javascript">
		$(function() {
			$( "#ruler" ).autocomplete({
				source:'/site/helper/searchruler.php',
			})
		});
		</script>			
		<!--[if lt IE 9]>
			<script src="/js/html5shiv.js"></script>
			<link rel="stylesheet" href="/css/ie.css" type="text/css" />
		<![endif]-->
	</head>
	<body>
		<div class="wrapper">
			<a href="/index.php"><div class="banner"></div></a>
  
			<!-- Beginning main navigation -->
				<p class="me-trigger-main">
					<a id="mainmenu">Main Navigation</a>
				</p>
				<nav id="main">
					<ul>
						<li><a href="/index.php">News</a></li>
						<li><a href="/site/rules/rules.htm">Rules</a></li>
						<li><a href="/site/card-db/card-db.php">Card Database</a></li>
						<li><a href="/site/deck/deckbuilder.php" class="active">Deckbuilder</a></li>
						<?php 
						// Connection Info
						$server = "localhost";
						$techuser = "83735_0.usr1";
						$password = "y74pEuLwQTEasNXZ";	
						$database = "83735_0";
						$loggedin = false;
						$ismember = false;

						// Server Connection
						mysql_connect($server,$techuser,$password) or die ("No Connection");

						// phpBB User Info
						define('IN_PHPBB', true);
						$phpbb_root_path = '../../forum/';
						$phpEx = substr(strrchr(__FILE__, '.'), 1);
						include($phpbb_root_path . 'common.' . $phpEx);
						include($phpbb_root_path . 'includes/functions_display.' . $phpEx);	

						// Start session management
						$user->session_begin();
						$auth->acl($user->data);
						$user->setup();
						
						mysql_select_db($database) or die ("DB doesn't exist");
						
						if (($user->data['is_registered'])){
							$loggedin = true;
							$query = "SELECT 
									*
								FROM 
									phpbb_user_group, phpbb_users 
								WHERE 
									phpbb_users.username like '".$user->data['username']."' and phpbb_users.user_id = phpbb_user_group.user_id and phpbb_user_group.group_id = 9
								LIMIT 0 , 1";
							$result = mysql_query ($query) or die (mysql_error());
							
							// If user is member of Group 9 (Team)
							while ($row = mysql_fetch_assoc($result)) {
								$ismember = true;
								echo '<li><a href="/site/team/insertcard.php" class="team">Team</a></li>';
							}	
						}
						echo '
						<li><a href="/site/faq/faq.htm">FAQ</a></li>
						<li><a href="/site/about-me/about-me.htm">About Me</a></li>
					</ul>
				</nav>
			<!-- End main navigation -->';
						
				//Beginning sub navigation
				echo '<p class="me-trigger-sub">
						<a id="submenu">Sub Navigation</a>
					</p>
				<nav id="sub">';
				if (!$loggedin){
					echo '<h2>Login</h2>';
					//user is not logged in  
					echo '
					<form method="POST" action="/forum/ucp.php?mode=login">
						<p>
							Username: <input type="text" name="username" size="40"><br />
							Password: <input type="password" name="password" size="40"><br />
							Remember Me?: <input type="checkbox" name="autologin"><input type="submit" value="Submit" name="login">
						</p>
						<input type="hidden" name="redirect" value="/site/deck/deckbuilder.php">
					</form>';
				}

				if($loggedin){
					echo '				
						<h2>Create New Deck</h2>
						<form id="myform" name="myform" method="POST" action="createdeck.php">
							<p>
								<label for="deckname">Deckname:</label>
								<input id="deckname" name="deckname" type="text" size="24" placeholder=" Deckname..."/>
							</p>
							<div class="ui-widget">
								<p>
									<label for="ruler">Ruler:</label>
									<input id="ruler" name="ruler" type="text" size="24" placeholder=" Ruler..."/>
								</p>
							</div>
							<p>						
								<input type="submit" name="create" value="Create Deck">
							</p>
						</form>';
				}
				
				echo '</nav><div class="content">';
				
				if($loggedin){
					// End sub navigation

					// Beginning content
					echo '
						<h1>My Decks</h1>';

						// SQL Statement
						$query = 'SELECT 
							* 
						FROM 
							decklist 
						WHERE
							username LIKE "'.$user->data['username'].'"';
					
						echo '<table>';

						$result = mysql_query ($query);
						while ($row = mysql_fetch_assoc($result)) {
							echo '<tr>';
								echo '<td style="width:30%" rowspan="2">';
									echo '<a href="view.php?decknumber='.$row['decknumber'].'">';
							
									// inner query start
									$query2 = 'SELECT 
										pic 
									FROM 
										cards 
									WHERE
										name LIKE "'.$row['ruler'].'"';
									$result2 = mysql_query ($query2);
									while ($row2 = mysql_fetch_assoc($result2)) {
										echo '<img src="'.$row2['pic'].'" alt="'.$row['ruler'].'" width="120" />';
									}
									// inner query END
						
								echo '</a></td>';
								echo '<td style="width:70%">';
									echo '<h2><a href="view.php?decknumber='.$row['decknumber'].'">'.$row['deckname'].'</a> - <a href="edit.php?decknumber='.$row['decknumber'].'">[Edit]</a> - <a href="deletedeck.php?decknumber='.$row['decknumber'].'"  onClick="return confirm(\'You want to delete your deck, are you sure ?\');" >[Delete]</a></h2>';
								echo '</td>';
							echo '</tr><tr><td>';
								echo '<p>'.$row['description'].'</p>';		
							echo '</td></tr>';
						}
						
						echo '</table>';
					
						//$req = "INSERT INTO `decklist`(`decknumber`, `deckname`, `username`, `description`) VALUES ('','".mysql_real_escape_string($_REQUEST['term'])."','".$user->data['username']."','bla')";
						//$query = mysql_query($req);
				} else {
					echo "
					<h1>You are not logged in</h1>
					<p>Please <a href='http://force-of-will.com/forum/ucp.php?mode=register'><strong>register</strong></a> yourself and join us.<br />
					<img class='astema' src='/pics/joinus.png' />";
				}
					// Database Connection
					// SQL Statement
					//$req = "INSERT INTO `decklist`(`decknumber`, `deckname`, `username`, `description`) VALUES ('','".mysql_real_escape_string($_REQUEST['term'])."','".$user->data['username']."','bla')";
			?>

					<!-- Beginning footer -->
						<footer>
							<ul>
								<li><a href="#" class="top">&uarr; Back to Top</a></li>
							</ul>
							<p class="copy">       
								<strong>| Copyright &#169; 2014 | Force-of-Will.com | Version 8.0 | by Christian Schuler</strong> <br /> 
								All card names, artwork, and intrinsic Force of Will game concepts are copyright to Force of Will Co., Ltd. and used with permission.
							</p>
						</footer>
					<!-- End footer -->
				</div>
			<!-- End content -->

			<!-- Beginning info area -->
				<aside>
					<div class="infobox">
						<h4>Info</h4>
						<?php
							if($user->data['is_registered']){
								//user is logged in
								echo '<h5>Login</h5>';
								echo "Logged in as:<strong> " . $user->data['username']. "</strong><br />";  
								echo '
									<a href="' . append_sid("{$phpbb_root_path}ucp.$phpEx", 'mode=logout', true, $user->session_id). '">
									<input type="submit" name="logout" value="Logout"></a><br />';
								echo '<br />';
							} 
						?>
						<h5>Last Update</h5>
						<p class="change">19.12.2014</p>
						<br />
						<h5>Search my website:</h5>
						<script>
							(function() {
							var cx = '004487255471104360343:9achku3grbs';
							var gcse = document.createElement('script');
							gcse.type = 'text/javascript';
							gcse.async = true;
							gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							'//www.google.com/cse/cse.js?cx=' + cx;
							var s = document.getElementsByTagName('script')[0];
							s.parentNode.insertBefore(gcse, s);
							})();
						</script>
						<gcse:search></gcse:search>
					</div>
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- force-of-will -->
					<ins class="adsbygoogle" style="display:inline-block;width:180px;height:150px" data-ad-client="ca-pub-9292681495337777" data-ad-slot="7421495048"></ins>
					<script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
				</aside>
			<!-- End info area -->
			
			<!-- Beginning Google Analytics -->
				<script>
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  					})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

					ga('create', 'UA-52004644-1', 'force-of-will.com');
					ga('send', 'pageview');
				</script>
			<!-- End Google Analytics -->			
		</div> 
	</body>
</html>