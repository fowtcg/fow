<!DOCTYPE html>
<html lang="en">
	<head>
		<title>FoW - 
		<?php 
			$techuser = '83735_0.usr1';
			$host = 'localhost';
			$password = 'y74pEuLwQTEasNXZ';
			$database = '83735_0';

			$db = mysql_connect($host, $techuser, $password) or die (mysql_error());
			mysql_select_db($database, $db) or die ("Couldn't connect to database.");
			$query = 'SELECT 
						* 
					FROM 
						decklist 
					WHERE
						decknumber = '.mysql_real_escape_string($_REQUEST['decknumber']);
			$result = mysql_query ($query);
			while ($row = mysql_fetch_assoc($result)) {
				echo $row['deckname'];
			}
		?> - Force of Will - Deckbuilder</title>
		<meta charset="ISO-8859-1">	
		<meta name="author" content="Christian Schuler" />
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale = 0.1, maximum-scale=4.0, user-scalable=yes">
		<meta name="description" content="Deck Builder for the Trading Card Game - TCG - Force of Will - FoW - News - Errata - Deckbuilder - Card Database" />
		<link rel="shortcut icon" href="/pics/favicon.ico" />
		<link rel="stylesheet" href="/styles.css" type="text/css" />
		<link rel="stylesheet" href="/js/jquery-ui-1.11.0.custom/jquery-ui.css"/>		
		<style type="text/css"> 
			div.content td {
				text-align:center;
			}
		</style>
		<script type="text/javascript" src="/js/scripts.js"></script>	
		<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
		<script src="/js/jquery-ui-1.11.0.custom/external/jquery/jquery.js"></script>
		<script src="/js/jquery-ui-1.11.0.custom/jquery-ui.js"></script>
		<script type="text/javascript">
		$(function() {
			$( "#name" ).autocomplete({
				minLength: 2,
				source:'/site/helper/searchname.php',
			})
			$( "#ruler" ).autocomplete({
				source:'/site/helper/searchruler.php',
			})			
		});
		</script>			
		<!--[if lt IE 9]>
			<script src="/js/html5shiv.js"></script>
			<link rel="stylesheet" href="/css/ie.css" type="text/css" />
		<![endif]-->
	</head>
	<body>
		<script type="text/javascript" src="/js/tooltip/wz_tooltip.js"></script>
		<div class="wrapper">
			<a href="/index.php"><div class="banner"></div></a>
  
			<!-- Beginning main navigation -->
				<p class="me-trigger-main">
					<a id="mainmenu">Main Navigation</a>
				</p>
				<nav id="main">
					<ul>
						<li><a href="/index.php">News</a></li>
						<li><a href="/site/rules/rules.htm">Rules</a></li>
						<li><a href="/site/card-db/card-db.php">Card Database</a></li>
						<li><a href="/site/deck/deckbuilder.php" class="active">Deckbuilder</a></li>
						<?php 
						// Connection Info
						$server = "localhost";
						$techuser = "83735_0.usr1";
						$password = "y74pEuLwQTEasNXZ";	
						$database = "83735_0";
						$loggedin = false;
						$ismember = false;

						// Server Connection
						mysql_connect($server,$techuser,$password) or die ("No Connection");

						// phpBB User Info
						define('IN_PHPBB', true);
						$phpbb_root_path = '../../forum/';
						$phpEx = substr(strrchr(__FILE__, '.'), 1);
						include($phpbb_root_path . 'common.' . $phpEx);

						// Start session management
						$user->session_begin();
						$auth->acl($user->data);
						$user->setup();
						
						mysql_select_db($database) or die ("DB doesn't exist");
						
						if (($user->data['is_registered'])){
							$loggedin = true;
							$query = "SELECT 
									*
								FROM 
									phpbb_user_group, phpbb_users 
								WHERE 
									phpbb_users.username like '".$user->data['username']."' and phpbb_users.user_id = phpbb_user_group.user_id and phpbb_user_group.group_id = 9
								LIMIT 0 , 1";
							$result = mysql_query ($query) or die (mysql_error());
							
							// If user is member of Group 9 (Team)
							while ($row = mysql_fetch_assoc($result)) {
								$ismember = true;
								echo '<li><a href="/site/team/insertcard.php" class="team">Team</a></li>';
							}	
						}
						echo '
						<li><a href="/site/faq/faq.htm">FAQ</a></li>
						<li><a href="/site/about-me/about-me.htm">About Me</a></li>
					</ul>
				</nav>
			<!-- End main navigation -->';
			
				
				//Beginning sub navigation
				echo '
				<p class="me-trigger-sub">
					<a id="submenu">Sub Navigation</a>
				</p>
				<nav id="sub">
					<h2>Ruler</h2>';
					// List of Magic Stones
					$qruler = 'SELECT 
							*
						FROM 
							decklist d, cards c
						WHERE
							d.ruler = c.name
							AND d.decknumber = '.mysql_real_escape_string($_REQUEST['decknumber']);
					$ruler = mysql_query ($qruler);
					while ($row = mysql_fetch_assoc($ruler)) {
						if ($row['type'] == "Ruler") {
							echo '<p><img src="'.$row['pic'].'" alt="'.$row['name'].'" width="120"  onmouseover="Tip(\' <img src='.$row['pic'].' >\', BGCOLOR, \'#D6D6D6\', SHADOW, true, WIDTH, 370, SHADOWCOLOR, \'#4d4b4a\')" onmouseout="UnTip()" /> <br />';
							echo '<a href="/site/card-db/card.php?n1='.$row['n1'].'&amp;n2='.$row['n2'].'">';
							echo $row['name']." ";
							echo '</a></p><br />';
						}
					}
					$qms_count = 'SELECT 
							SUM(d.amount) as summe
						FROM 
							deckcards d, cards c
						WHERE
							d.n1 = c.n1 AND d.n2 = c.n2 AND d.name = c.name
							AND type LIKE "Magic Stone" AND d.decknumber = '.mysql_real_escape_string($_REQUEST['decknumber']);
					$ms_count = mysql_query ($qms_count);
					while ($row = mysql_fetch_assoc($ms_count)) {
						echo '<h2>Magic Stone ('.$row['summe'].')</h2>';
					}
					echo '<p>';
					// List of Magic Stones
					$qms = 'SELECT 
							* 
						FROM 
							deckcards d, cards c
						WHERE
							d.n1 = c.n1 AND d.n2 = c.n2 AND d.name = c.name
							AND type IN ("Magic Stone", "Ruler") AND d.decknumber = '.mysql_real_escape_string($_REQUEST['decknumber']).'
						ORDER BY d.name';					
					$ms = mysql_query ($qms);
					while ($row = mysql_fetch_assoc($ms)) {
						if ($row['type'] == "Magic Stone") {					
							echo '<img src="'.$row['pic'].'" alt="'.$row['name'].'" width="50"  onmouseover="Tip(\' <img src='.$row['pic'].' >\', BGCOLOR, \'#D6D6D6\', SHADOW, true, WIDTH, 370, SHADOWCOLOR, \'#4d4b4a\')" onmouseout="UnTip()" /> <br />';
							echo $row['amount'].'x <a href="/site/card-db/card.php?n1='.$row['n1'].'&amp;n2='.$row['n2'].'">';
							echo $row['name']." ";
							echo '</a><br />';
						}
					}					
					echo '</p>';					
				echo '</nav>';
				// End sub navigation
					// SQL Statement
				$query = 'SELECT 
						* 
					FROM 
						decklist 
					WHERE
						decknumber = '.mysql_real_escape_string($_REQUEST['decknumber']);
				
				
				// Beginning content
				echo '<div class="content">';
				// Deckname
				$result = mysql_query ($query);
				$creator ="";
				while ($row = mysql_fetch_assoc($result)) {
					echo '<h1>'.$row['deckname'];
					$creator = $row['username'];
				}
				// Total number of cards
				$qres_count = 'SELECT 
						SUM(d.amount) as summe
					FROM 
						deckcards d, cards c
					WHERE
						d.n1 = c.n1 AND d.n2 = c.n2 AND d.name = c.name
						AND d.decknumber = '.mysql_real_escape_string($_REQUEST['decknumber']).' AND c.type NOT LIKE "Magic Stone"';
				$res_count = mysql_query ($qres_count);
				while ($row = mysql_fetch_assoc($res_count)) {
					echo ' ('.$row['summe'].' Cards)';
				}
				echo ' by '.$creator.'</h1>';
				// Description
				$qdescription = 'SELECT 
						description
					FROM 
						decklist
					WHERE
						decknumber = '.mysql_real_escape_string($_REQUEST['decknumber']);
				$description = mysql_query ($qdescription);
				while ($row = mysql_fetch_assoc($description)) {
					echo '
					<h2>Description</h2>';
					$text = "No description";
					$text = $row['description'];
					echo '<p>'.$text.'</p>
							<p>';
				}					
				
				// Headline + Sum of resonators
				$qres_count = 'SELECT 
						SUM(d.amount) as summe
					FROM 
						deckcards d, cards c
					WHERE
						d.n1 = c.n1 AND d.n2 = c.n2 AND d.name = c.name
						AND type LIKE "Resonator" AND d.decknumber = '.mysql_real_escape_string($_REQUEST['decknumber']);
				$res_count = mysql_query ($qres_count);
				while ($row = mysql_fetch_assoc($res_count)) {
					echo '<h2>Resonators ('.$row['summe'].')</h2>';
				}
	
				// List of Resonators
				$qresonator = 'SELECT 
						* 
					FROM 
						deckcards d, cards c
					WHERE
						d.n1 = c.n1 AND d.n2 = c.n2 AND d.name = c.name
						AND type LIKE "Resonator" AND d.decknumber = '.mysql_real_escape_string($_REQUEST['decknumber']).'
					ORDER BY d.name';
					echo '<table>';
					$resonator = mysql_query ($qresonator);
					while ($row = mysql_fetch_assoc($resonator)) {
						echo '<tr>';
							echo '<td style="width:15%">';
								echo '<img src="'.$row['pic'].'" alt="'.$row['name'].'" width="50"  onmouseover="Tip(\' <img src='.$row['pic'].' >\', BGCOLOR, \'#D6D6D6\', SHADOW, true, WIDTH, 370, SHADOWCOLOR, \'#4d4b4a\')" onmouseout="UnTip()" />';
							echo '</td>';						
							echo '<td style="width:15%">';
								echo " ".$row['amount']." x";
							echo '</td>';
							echo '<td style="width:60%">';
									echo '<a href="/site/card-db/card.php?&amp;l=en&amp;n1='.$row['n1'].'&amp;n2='.$row['n2'].'">';
									echo $row['name']." ";
									echo '</a>';
							echo '</td>';
						echo '</tr>';
					}
				echo '</table>';
				
				// Headline + Sum of Additions
				$qres_count = 'SELECT 
						SUM(d.amount) as summe
					FROM 
						deckcards d, cards c
					WHERE
						d.n1 = c.n1 AND d.n2 = c.n2 AND d.name = c.name
						AND type LIKE "Addition" AND d.decknumber = '.mysql_real_escape_string($_REQUEST['decknumber']);
				$res_count = mysql_query ($qres_count);
				while ($row = mysql_fetch_assoc($res_count)) {
					echo '<h2>Additions ('.$row['summe'].')</h2>';
				}					
				
				// List of Additions
				$qaddition = 'SELECT 
						* 
					FROM 
						deckcards d, cards c
					WHERE
						d.n1 = c.n1 AND d.n2 = c.n2 AND d.name = c.name
						AND type LIKE "Addition" AND d.decknumber = '.mysql_real_escape_string($_REQUEST['decknumber']).'
					ORDER BY d.name';
				
				echo '<table>';
				$addition = mysql_query ($qaddition);
					while ($row = mysql_fetch_assoc($addition)) {
						echo '<tr>';
							echo '<td style="width:15%">';
								echo '<img src="'.$row['pic'].'" alt="'.$row['name'].'" width="50"  onmouseover="Tip(\' <img src='.$row['pic'].' >\', BGCOLOR, \'#D6D6D6\', SHADOW, true, WIDTH, 370, SHADOWCOLOR, \'#4d4b4a\')" onmouseout="UnTip()" />';
							echo '</td>';						
							echo '<td style="width:15%">';
								echo " ".$row['amount']." x";
							echo '</td>';
							echo '<td style="width:60%">';
									echo '<a href="/site/card-db/card.php?&amp;l=en&amp;n1='.$row['n1'].'&amp;n2='.$row['n2'].'">';
									echo $row['name']." ";
									echo '</a>';
							echo '</td>';
						echo '</tr>';
					}
				echo '</table>';
				
				// Headline + Sum of Spells
				$qres_count = 'SELECT 
						SUM(d.amount) as summe
					FROM 
						deckcards d, cards c
					WHERE
						d.n1 = c.n1 AND d.n2 = c.n2 AND d.name = c.name
						AND type LIKE "Spell" AND d.decknumber = '.mysql_real_escape_string($_REQUEST['decknumber']);
				$res_count = mysql_query ($qres_count);
				while ($row = mysql_fetch_assoc($res_count)) {
					echo '<h2>Spells ('.$row['summe'].')</h2>';
				}	
				
				// List of Spells
				$qspell = 'SELECT 
						* 
					FROM 
						deckcards d, cards c
					WHERE
						d.n1 = c.n1 AND d.n2 = c.n2 AND d.name = c.name
						AND type LIKE "Spell" AND d.decknumber = '.mysql_real_escape_string($_REQUEST['decknumber']).'
					ORDER BY d.name';
				
				echo '<table>';
				$spell = mysql_query ($qspell);
					while ($row = mysql_fetch_assoc($spell)) {
						echo '<tr>';
							echo '<td style="width:15%">';
								echo '<img src="'.$row['pic'].'" alt="'.$row['name'].'" width="50"  onmouseover="Tip(\' <img src='.$row['pic'].' >\', BGCOLOR, \'#D6D6D6\', SHADOW, true, WIDTH, 370, SHADOWCOLOR, \'#4d4b4a\')" onmouseout="UnTip()" />';
							echo '</td>';						
							echo '<td style="width:15%">';
								echo " ".$row['amount']." x";
							echo '</td>';
							echo '<td style="width:60%">';
									echo '<a href="/site/card-db/card.php?&amp;l=en&amp;n1='.$row['n1'].'&amp;n2='.$row['n2'].'">';
									echo $row['name']." ";
									echo '</a>';
							echo '</td>';
						echo '</tr>';
					}
				echo '</table>';
			?>

					<!-- Beginning footer -->
						<footer>
							<ul>
								<li><a href="#" class="top">&uarr; Back to Top</a></li>
							</ul>
							<p class="copy">       
								<strong>| Copyright &#169; 2014 | Force-of-Will.com | Version 8.0 | by Christian Schuler</strong> <br /> 
								All card names, artwork, and intrinsic Force of Will game concepts are copyright to Force of Will Co., Ltd. and used with permission.
							</p>
						</footer>
					<!-- End footer -->
				</div>
			<!-- End content -->

			<!-- Beginning info area -->
				<aside>
					<div class="infobox">
						<h4>Info</h4>
						<h5>Login</h5>
						<?php
							if($user->data['is_registered']){
								//user is logged in
								echo "Logged in as:<strong> " . $user->data['username']. "</strong><br />";  
								echo '
									<a href="' . append_sid("{$phpbb_root_path}ucp.$phpEx", 'mode=logout', true, $user->session_id). '">
									<input type="submit" name="logout" value="Logout"></a><br /><br />';
							} else {
								//user is not logged in  
								echo 'Please log in:<br />
								<form method="POST" action="/forum/ucp.php?mode=login">
									<p>
										Username: <input type="text" name="username" size="40"><br />
										Password: <input type="password" name="password" size="40"><br />
										Remember Me?: <input type="checkbox" name="autologin"><input type="submit" value="Submit" name="login">
									</p>
									<input type="hidden" name="redirect" value="/site/deck/view.php?decknumber='.mysql_real_escape_string($_REQUEST['decknumber']).'">
								</form>';
							}
						?>
						<br />
						<h5>Last Update</h5>
						<p class="change">19.12.2014</p>
						<br />
						<h5>Search my website:</h5>
						<script>
							(function() {
							var cx = '004487255471104360343:9achku3grbs';
							var gcse = document.createElement('script');
							gcse.type = 'text/javascript';
							gcse.async = true;
							gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							'//www.google.com/cse/cse.js?cx=' + cx;
							var s = document.getElementsByTagName('script')[0];
							s.parentNode.insertBefore(gcse, s);
							})();
						</script>
						<gcse:search></gcse:search>
					</div>
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- force-of-will -->
					<ins class="adsbygoogle" style="display:inline-block;width:180px;height:150px" data-ad-client="ca-pub-9292681495337777" data-ad-slot="7421495048"></ins>
					<script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
				</aside>
			<!-- End info area -->
			
			<!-- Beginning Google Analytics -->
				<script>
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  					})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

					ga('create', 'UA-52004644-1', 'force-of-will.com');
					ga('send', 'pageview');
				</script>
			<!-- End Google Analytics -->			
		</div> 
	</body>
</html>