<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Force of Will - FoW - News - Welcome</title>
		<meta charset="ISO-8859-1">	
		<meta name="author" content="Christian Schuler" />
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale = 0.1, maximum-scale=4.0, user-scalable=yes">
		<meta name="description" content="News for the Trading Card Game - TCG - Force of Will - FoW - News - Rules - Card Database - FAQ" />
		<meta name="google-site-verification" content="7FvDeHOwFe0pBCv0_ldL7umubH-DT6VbTxIteA4iRMA" />
		<link rel="shortcut icon" href="pics/favicon.ico" />
		<link rel="stylesheet" href="styles.css" type="text/css" />
		<link rel="stylesheet" href="/css/lightbox.css">		
		<script src="/js/jquery-ui-1.11.0.custom/external/jquery/jquery.js"></script>
		<script src="/js/jquery-ui-1.11.0.custom/jquery-ui.js"></script>
		<script src="/js/lightbox/lightbox.js"></script>
		<script type="text/javascript" src="js/scripts.js"></script>
		<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
			<link rel="stylesheet" href="css/ie.css" type="text/css" />
		<![endif]-->
	</head>
	<body>
		<div class="wrapper">
			<a href="index.php"><div class="banner"></div></a>
  
			<!-- Beginning main navigation -->
				<p class="me-trigger-main">
					<a id="mainmenu">Main Navigation</a>
				</p>
				<nav id="main">
					<ul>
						<li><a href="/index.php" class="active">News</a></li>
						<li><a href="/site/rules/rules.htm">Rules</a></li>
						<li><a href="/site/card-db/card-db.php">Card Database</a></li>
						<li><a href="/site/deck/deckbuilder.php">Deckbuilder</a></li>
						<?php 
						// Connection Info
						$server = "localhost";
						$techuser = "83735_0.usr1";
						$password = "y74pEuLwQTEasNXZ";	
						$database = "83735_0";
						$loggedin = false;
						$ismember = false;

						// Server Connection
						mysql_connect($server,$techuser,$password) or die ("No Connection");

						// phpBB User Info
						define('IN_PHPBB', true);
						$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : 'forum/';
						$phpEx = substr(strrchr(__FILE__, '.'), 1);
						include($phpbb_root_path . 'common.' . $phpEx);
						include($phpbb_root_path . 'includes/functions_display.' . $phpEx);						

						// Start session management
						$user->session_begin();
						$auth->acl($user->data);
						$user->setup();
						
						mysql_select_db($database) or die ("DB doesn't exist");
						
						// If user is registered User
						if (($user->data['is_registered'])){
							$loggedin = true;
							$query = "SELECT 
									*
								FROM 
									phpbb_user_group, phpbb_users 
								WHERE 
									phpbb_users.username like '".$user->data['username']."' and phpbb_users.user_id = phpbb_user_group.user_id and phpbb_user_group.group_id = 9
								LIMIT 0 , 1";
							$result = mysql_query ($query) or die (mysql_error());
							
							// If user is member of Group 9 (Team)
							while ($row = mysql_fetch_assoc($result)) {
								echo '<li><a href="/site/team/insertcard.php" class="team">Team</a></li>';
								$ismember = true;
							}	
						}
						echo '
						<li><a href="/site/faq/faq.htm">FAQ</a></li>
						<li><a href="/site/about-me/about-me.htm">About Me</a></li>
					</ul>
				</nav>
			<!-- End main navigation -->

			<!-- Beginning sub navigation -->
				<p class="me-trigger-sub">
					<a id="submenu">Sub Navigation</a>
				</p>
				<nav id="sub">';
					echo '<h2>Login</h2>';
					// If user is not logged in show Login
					if (!$loggedin){
						echo '
						<form method="POST" action="/forum/ucp.php?mode=login">
							<p>
								Username: <input type="text" name="username" size="20"><br />
								Password: <input type="password" name="password" size="20"><br />
								Remember Me?: <input type="checkbox" name="autologin"><input type="submit" value="Submit" name="login">
							</p>
							<input type="hidden" name="redirect" value="/index.php">
						</form>';
					}
				
					if($loggedin){
						echo '<p>';
							echo "Logged in as:<strong> " . $user->data['username']. "</strong><br />"; 
							echo '<br />';
							echo '
								<a href="' . append_sid("{$phpbb_root_path}ucp.$phpEx", 'mode=logout', true, $user->session_id). '">
									<input type="hidden" name="redirect" value="/index.php">
									<input type="submit" name="logout" value="Logout">
								</a><br />';
						echo '</p>';
					}
					?>
				</nav>
			<!-- End sub navigation-->

			<!-- Beginning content -->
				<div class="content">
					<h1>Welcome</h1>
					<p>
						Hello guys<br />
						Finally the new set is online, check it out: <br />
						The Moon Priestess Returns <br />
						~~~ <br />
						Pictures will follow soon.
					</p>
					
					<!-- Beginning footer -->
						<footer>
							<ul>
								<li><a href="#" class="top">&uarr; Back to Top</a></li>
							</ul>
							<p class="copy">       
								<strong>| Copyright &#169; 2014 | Force-of-Will.com | Version 8.0 | by Christian Schuler</strong> <br /> 
								All card names, artwork, and intrinsic Force of Will game concepts are copyright to Force of Will Co., Ltd. and used with permission.
							</p>
						</footer>
					<!-- End footer -->
				</div>
			<!-- End content -->

			<!-- Beginning info area -->
				<aside>
					<div class="infobox">
						<h4>Info</h4>
						<h5>Last Update</h5>
						<p class="change">19.12.2014</p>
						<br />
							<h5>Search my website:</h5>
						<script>
							(function() {
							var cx = '004487255471104360343:9achku3grbs';
							var gcse = document.createElement('script');
							gcse.type = 'text/javascript';
							gcse.async = true;
							gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							'//www.google.com/cse/cse.js?cx=' + cx;
							var s = document.getElementsByTagName('script')[0];
							s.parentNode.insertBefore(gcse, s);
							})();
						</script>
						<gcse:search></gcse:search>
					</div>
				</aside>
			<!-- End info area -->
			
			<!-- Beginning Google Analytics -->
				<script>
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  					})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

					ga('create', 'UA-52004644-1', 'force-of-will.com');
					ga('send', 'pageview');
				</script>
			<!-- End Google Analytics -->
		</div> 
	</body>
</html>