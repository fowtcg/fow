<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Force of Will - FoW - News - Welcome</title>
		<meta charset="ISO-8859-1">	
		<meta name="author" content="Christian Schuler" />
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale = 0.1, maximum-scale=4.0, user-scalable=yes">
		<meta name="description" content="News for the Trading Card Game - TCG - Force of Will - FoW - News - Rules - Card Database - Errata - FAQ" />
		<meta name="google-site-verification" content="7FvDeHOwFe0pBCv0_ldL7umubH-DT6VbTxIteA4iRMA" />
		<link rel="shortcut icon" href="pics/favicon.ico" />
		<link rel="stylesheet" href="styles.css" type="text/css" />
		<script type="text/javascript" src="js/scripts.js"></script>
		<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
			<link rel="stylesheet" href="css/ie.css" type="text/css" />
		<![endif]-->
	</head>
	<body>
	
		<div class="wrapper">
			<a href="index.htm"><div class="banner"></div></a>
  
			<!-- Beginning main navigation -->
				<p class="me-trigger-main">
					<a id="mainmenu">Main Navigation</a>
				</p>
				<nav id="main">
					<ul>
						<li><a href="index.htm" class="active">News</a></li>
						<li><a href="site/rules/rules.htm">Rules</a></li>
						<li><a href="site/card-database/card-database.php">Card Database</a></li>
						<li><a href="forum/index.php">Forum</a></li>						
						<li><a href="site/errata/errata.htm">Errata</a></li>
						<li><a href="site/faq/faq.htm">FAQ</a></li>
						<!--<li><a href="../deck-construction/deck-construction.htm">Deck Construction</a></li> -->
						<li><a href="site/about-me/about-me.htm">About Me</a></li>
					</ul>
				</nav>
			<!-- End main navigation -->

			<!-- Beginning sub navigation -->
				<p class="me-trigger-sub">
					<a id="submenu">Sub Navigation</a>
				</p>
				<nav id="sub">
					<h2>News</h2>
					<ul>
						<li><a href="index.htm" class="active">News</a></li>
						<li><a href="site/news/changelog.htm">Changelog</a></li>
					</ul>
				</nav>
			<!-- End sub navigation-->

			<!-- Beginning content -->
				<div class="content">
					<h1>News - Welcome</h1>
					<img class="astema" src="pics/front.jpg" alt="picture of front" />
					<h2>New Errata online!</h2> 
					<ul>
						<li>New Errata online! 2-032 and 3-008</li>
						<li>It's possible now to see the Ruler and J-ruler at the same time</li>
						<li>Added a new row "Released" at the bottom of each card with the values 'Set 1' and 'Set 2' for the english market</li>
						<li>Added an FAQ section below each card with the most often asked questions I remebered</li>
						<li>The cardname is now also displayed in the title of the page, for example if you copy it to facebook</li>
						<li>Corrected all card names and texts that didn't match the picture</li>
						<li>Added missing pictures</li>
					</ul>
					<h2>New search features:</h2>
					<ul>
						<li>Added Symbol Skills, such as [Pierce][Flying],...</li>
						<li>Added the value "Starter" to rarity</li>
						<li>Added the value "Promo" to rarity (but the cards are not in the database yet)</li>
						<li>Added the value "Void" to attribute</li>
						<li>Corrected the value from "Darkness" to "Dark"</li>
						<li>Improved the "Rule Text" search
					</ul>					
					<h2>Your Help</h2>
					<p>
						If you have feedback for the page in general. For example, you're facing problems with a specific browser. You found a dead link or other technical stuff.<br />
						Just send me an <a href="mailto:xxxGrizzlxxx@gmail.com"><i>e-mail</i></a>!<br />
						<br />
						Even more important:<br />
						If you want to contribute something to the content of this page. For example if you have any questions regarding a game situation or specific cards; I would be thankfull to help you and include your questions in the FAQ section.
					</p>
					<p>
						Thanks a lot for your interest and now enjoy the world from Force of Will!
					</p>
					<br/>
					
					<!-- Beginning footer -->
						<footer>
							<ul>
								<li><a href="#" class="top">&uarr; Back to Top</a></li>
							</ul>
							<p class="copy">       
								<strong>| Copyright &#169; 2014 | Force-of-Will.com | Version 5.0 | by Christian Schuler</strong> <br /> 
								All card names, artwork, and intrinsic Force of Will game concepts are copyright to Force of Will Co., Ltd. and used with permission.
							</p>
						</footer>
					<!-- End footer -->
				</div>
			<!-- End content -->

			<!-- Beginning info area -->
				<aside>
					<div class="infobox">
						<h4>Info</h4>
						<h5>Last Update</h5>
						<p class="change">29.07.2014</p>
						<br />
							<h5>Quick search</h5>
						<!--<form action="../suche/suche.php" method="post">
							<input type="text" name="search" class="feld" placeholder="Search Terms...">
							<input type="submit" value="search" class="button">
						</form>-->
						<script>
							(function() {
							var cx = '004487255471104360343:9achku3grbs';
							var gcse = document.createElement('script');
							gcse.type = 'text/javascript';
							gcse.async = true;
							gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							'//www.google.com/cse/cse.js?cx=' + cx;
							var s = document.getElementsByTagName('script')[0];
							s.parentNode.insertBefore(gcse, s);
							})();
						</script>
						<gcse:search></gcse:search>
					</div>
					<div class="infobox">
						<?php
							define('IN_PHPBB', true);
							$phpbb_root_path = './';
							$phpEx = substr(strrchr(__FILE__, '.'), 1);
							include($phpbb_root_path . 'common.' . $phpEx);

							// Start session management
							$user->session_begin();
							$auth->acl($user->data);
							$user->setup();
							
							echo 'see if locked in:';
							
							if($user->data['is_registered'])
								echo 'locked in, he is'; //user is logged in
							else
								echo "locked in, he's not";	//user is not logged in  
						?>
					
						Please log in:<br />
						<form method="POST" action="./ucp.php?mode=login">
							<p>
								Username: <input type="text" name="username" size="40"><br />
								Password: <input type="password" name="password" size="40"><br />
								Remember Me?: <input type="checkbox" name="autologin"><br />
								<input type="submit" value="Submit" name="login">
							</p>
							<input type="hidden" name="redirect" value="index.php">
						</form>
					</div>
				</aside>
			<!-- End info area -->
			
			<!-- Beginning Google Analytics -->
				<script>
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  					})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

					ga('create', 'UA-52004644-1', 'force-of-will.com');
					ga('send', 'pageview');
				</script>
			<!-- End Google Analytics -->
		</div> 
	</body>
</html>