<?php exit; ?>
1434043446
SELECT s.style_id, c.theme_id, c.theme_data, c.theme_path, c.theme_name, c.theme_mtime, i.*, t.template_path FROM phpbb_styles s, phpbb_styles_template t, phpbb_styles_theme c, phpbb_styles_imageset i WHERE s.style_id = 2 AND t.template_id = s.template_id AND c.theme_id = s.theme_id AND i.imageset_id = s.imageset_id
71320
a:1:{i:0;a:11:{s:8:"style_id";s:1:"2";s:8:"theme_id";s:1:"2";s:10:"theme_data";s:70935:"/*  phpBB3 Style Sheet
    --------------------------------------------------------------
	Style name:			proDVGFX
	Based on style:		prosilver (the default phpBB 3.0.x style)
	Original author:	Tom Beddard ( http://www.subblue.com/ )
	Modified by:		Prosk8er ( http://gotskillslounge.com/ )
    --------------------------------------------------------------
*/

/* General Markup Styles */

* { margin: 0; padding: 0; }

html {
	height: 101%;
	overflow-y: scroll;
	background-color: #1F1F1F;
}

body {
	/* Text-Sizing with ems: http://www.clagnut.com/blog/348/ */
	font-family: Verdana, Helvetica, Arial, sans-serif;
	background-color: #191919;
	font-size: 62.5%;			 /*This sets the default font size to be equivalent to 10px */
	margin: 0;
	padding: 27px 0;
}

h1 {
	/* Forum name */
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	margin-right: 200px;
	margin-top: 15px;
	font-weight: bold;
	font-size: 2em;
}

h2 {
	/* Forum header titles */
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	font-weight: normal;
	font-size: 2em;
	margin: 0.8em 0 0.2em 0;
}

h2.solo {
	margin-bottom: 1em;
}

h3 {
	/* Sub-headers (also used as post headers, but defined later) */
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
	text-transform: uppercase;
	border-bottom: 1px solid;
	margin-bottom: 3px;
	padding-bottom: 2px;
	font-size: 1.05em;
	margin-top: 20px;
}

h4 {
	/* Forum and topic list titles */
	font-family: "Trebuchet MS", Verdana, Helvetica, Arial, Sans-serif;
	font-size: 1.3em;
}

p {
	line-height: 1.3em;
	font-size: 1.1em;
	margin-bottom: 1.5em;
}

img {
	border-width: 0;
}

hr {
	/* Also see tweaks.css */
	border: 0 none #FFFFFF;
	border-top: 1px solid #CCCCCC;
	height: 1px;
	margin: 5px 0;
	display: block;
	clear: both;
}

hr.dashed {
	border-top: 1px dashed #CCCCCC;
	margin: 10px 0;
}

hr.divider {
	display: none;
}

p.right {
	text-align: right;
}

/* Border
---------------------------------------- */
.border-left { 
    background: url("{T_THEME_PATH}/images/border_left.png") #191919 repeat-y 0 0;
}

.border-right {
    background: url("{T_THEME_PATH}/images/border_right.png") repeat-y 100% 0;
}

.border-top { 
    background: url("{T_THEME_PATH}/images/border_top.png") repeat-x 0 0; 
}

.border-top-left {
    background: url("{T_THEME_PATH}/images/border_top_left.png") no-repeat 0 0;
}

.border-top-right {
    background: url("{T_THEME_PATH}/images/border_top_right.png") no-repeat 100% 0;
    width: 100%;
}

.border-bottom { 
    background: url("{T_THEME_PATH}/images/border_bottom2.png") repeat-x 0 100%;
}

.border-bottom span {
    background: url("{T_THEME_PATH}/images/border_bottom.png") no-repeat 0 0;
    display: block;
    height: 18px; 
}

.border-bottom span span { 
    background-position: 100% -18px;
}

.inside { 
    padding: 15px 15px 5px 15px;
}

/* Main blocks */
#wrap {
	margin: 0 auto;
	padding: 0 20px;

}

#simple-wrap {
	padding: 6px 10px;
}

#page-body {
	clear: both;
	margin: 5px 0;
}

#page-footer {
	clear: both;
}

#page-footer h3 {
	margin-top: 20px;
}

#logo {
	float: left;
	width: auto;
	padding: 10px 13px 0 10px;
}

a#logo:hover {
	text-decoration: none;
}

/* Search box */
#search-box {
	display: block;
	float: right;
	margin-top: 30px;
	margin-right: 5px;
	position: relative;
	text-align: right;
	white-space: nowrap; /* For Opera */
}

#search-box #keywords {
	width: 95px;
	background-color: #FFF;
}

#search-box input {
	border: 1px solid #000;
}

/* .button1 style defined later, just a few tweaks for the search button version */
#search-box input.button1 {
	padding: 1px 5px;
}

#search-box li {
	text-align: right;
	margin-top: 4px;
}

#search-box img {
	vertical-align: middle;
	margin-right: 3px;
}

/* Site description and logo */
#site-description {
	float: left;
	width: 70%;
}

#site-description h1 {
	margin-right: 0;
}

/* Round cornered boxes and backgrounds */
.headerbar {
	background: none repeat-x 0 0;
	margin-bottom: 4px;
	padding: 0 5px;
}

.navbar {
	padding: 0 10px;
}

.forabg {
	background: none repeat-x 0 0;
	margin-bottom: 4px;
	padding: 0 5px;
	clear: both;
}

.forumbg {
	background: none repeat-x 0 0;
	margin-bottom: 4px;
	padding: 0 5px;
	clear: both;
}

.panel {
	margin-bottom: 4px;
	padding: 0 10px;
}

.post {
	background-position: 100% 0;
	background-repeat: no-repeat;
	padding: 0 10px;
	margin-bottom: 5px;
}

.post:target .content {}
.post:target h3 a {}
.bg1 {}
.bg2 {}
.bg3 {}

.rowbg {
	margin: 5px 5px 2px 5px;
}

.ucprowbg {}
.fieldsbg {}

span.corners-top, span.corners-bottom, span.corners-top span, span.corners-bottom span,
span.corners-top2, span.corners-bottom2, span.corners-top2 span, span.corners-bottom2 span {
	font-size: 1px;
	line-height: 1px;
	display: block;
	height: 5px;
	background-repeat: no-repeat;
}

span.corners-top, span.corners-top2 {
	background-image: none;
	background-position: 0 0;
	margin: 0 -5px;
}

span.corners-top span, span.corners-top2 span {
	background-image: none;
	background-position: 100% 0;
}

span.corners-bottom, span.corners-bottom2 {
	background-image: none;
	background-position: 0 100%;
	margin: 0 -5px;
	clear: both;
}

span.corners-bottom span, span.corners-bottom2 span {
	background-image: none;
	background-position: 100% 100%;
}

.headbg span.corners-bottom {
	margin-bottom: -1px;
}

.post span.corners-top, .post span.corners-bottom, .panel span.corners-top,
.panel span.corners-bottom, .navbar span.corners-top, .navbar span.corners-bottom,
.post span.corners-top2, .post span.corners-bottom2, .panel span.corners-top2,
.panel span.corners-bottom2, .navbar span.corners-top2, .navbar span.corners-bottom2 {
	margin: 0 -10px;
}

.rules span.corners-top, .rules span.corners-top2 {
	margin: 0 -10px 5px -10px;
}

.rules span.corners-bottom, .rules span.corners-bottom2 {
	margin: 5px -10px 0 -10px;
}

/* Horizontal lists */
ul.linklist {
	display: block;
	margin: 0;
}

ul.linklist li {
	display: block;
	list-style-type: none;
	float: left;
	width: auto;
	margin-right: 5px;
	font-size: 1.1em;
	line-height: 2.2em;
}

ul.linklist li.rightside, p.rightside {
	float: right;
	margin-right: 0;
	margin-left: 5px;
	text-align: right;
}

ul.navlinks {
	padding-bottom: 1px;
	margin-bottom: 1px;
	border-bottom: 1px solid;
/*	font-weight: bold; */
}

ul.leftside {
	float: left;
	margin-left: 0;
	margin-right: 5px;
	text-align: left;
}

ul.rightside {
	float: right;
	margin-left: 5px;
	margin-right: -5px;
	text-align: right;
}

/* Table styles */
table.table1 {
	/* See tweaks.css */
}

#ucp-main table.table1 {
	padding: 2px;
}

table.table1 thead th {
	font-size: 1em;
	font-weight: normal;
	line-height: 1.3em;
	padding: 0 0 4px 3px;
	text-transform: uppercase;
}

table.table1 thead th span {
	padding-left: 7px;
}

table.table1 tbody tr {
	border: 1px solid #cfcfcf;
}

table.table1 tbody tr:hover, table.table1 tbody tr.hover {}

table.table1 td {
	font-size: 1.1em;
}

table.table1 tbody td {
	padding: 5px;
	border-top: 1px solid;
}

table.table1 tbody th {
	border-bottom: 1px solid;
	padding: 5px;
	text-align: left;
}

/* Specific column styles */
table.table1 .name		{ text-align: left; }
table.table1 .posts		{ text-align: center !important; width: 7%; }
table.table1 .joined	{ text-align: left; width: 15%; }
table.table1 .active	{ text-align: left; width: 15%; }
table.table1 .mark		{ text-align: center; width: 7%; }
table.table1 .info		{ text-align: left; width: 30%; }
table.table1 .info div	{ overflow: hidden; white-space: normal; width: 100%; }
table.table1 .autocol	{ line-height: 2em; white-space: nowrap; }
table.table1 thead .autocol { padding-left: 1em; }
table.table1 span.rank-img	{ float: right; width: auto; }
table.info td		{ padding: 3px; }

table.info tbody th {
	font-weight: normal;
	padding: 3px;
	text-align: right;
	vertical-align: top;
}

.forumbg table.table1 {
	margin: 0;
}

.forumbg-table > .inner {
	margin: 0 -1px;
}

.forumbg-table > .inner > span.corners-top {
	margin: 0 -4px -1px -4px;
}

.forumbg-table > .inner > span.corners-bottom {
	margin: -1px -4px 0 -4px;
}

/* Misc layout styles */
/* column[1-2] styles are containers for two column layouts 
   Also see tweaks.css */
.column1 {
	clear: left;
	float: left;
	width: 49%;
}

.column2 {
	clear: right;
	float: right;
	width: 49%;
}

/* General classes for placing floating blocks */
.left-box {
	float: left;
	text-align: left;
	width: auto;
}

.right-box {
	float: right;
	text-align: right;
	width: auto;
}

dl.details {
	/*font-family: "Lucida Grande", Verdana, Helvetica, Arial, sans-serif;*/
	font-size: 1.1em;
}

dl.details dt {
	float: left;
	clear: left;
	width: 30%;
	text-align: right;
	color: #000000;
	display: block;
}

dl.details dd {
	margin-left: 0;
	padding-left: 5px;
	margin-bottom: 5px;
	color: #828282;
	float: left;
	width: 65%;
}

/* Pagination
---------------------------------------- */
.pagination {
	height: 1%; /* IE tweak (holly hack) */
	width: auto;
	text-align: right;
	margin-top: 5px;
	float: right;
}

.pagination span.page-sep {
	display: none;
}

li.pagination {
	margin-top: 0;
}

.pagination strong, .pagination b {
	font-weight: normal;
}

.pagination span strong {
	padding: 0 2px;
	margin: 0 2px;
	font-weight: normal;
	color: #FFF;
	background-color: #1F1F1F;
	border: 1px solid #000;
	font-size: 0.9em;
}

.pagination span a, .pagination span a:link, .pagination span a:visited, .pagination span a:active {
	font-weight: normal;
	text-decoration: none;
	margin: 0 2px;
	padding: 0 2px;
	background-color: #1F1F1F;
	border: 1px solid;
	font-size: 0.9em;
	line-height: 1.5em;
}

.pagination span a:hover {
	border-color: #000;
	background-color: #090909;
	color: #FFF;
	text-decoration: none;
}

.pagination img {
	vertical-align: middle;
}

/* Pagination in viewforum for multipage topics */
.row .pagination {
	display: block;
	float: right;
	width: auto;
	margin-top: 0;
	padding: 1px 0 1px 15px;
	font-size: 0.9em;
	background: none 0 50% no-repeat;
}

.row .pagination span a, li.pagination span a {
	background-color: #FFFFFF;
}

.row .pagination span a:hover, li.pagination span a:hover {
	background-color: #d2d2d2;
}

/* Miscellaneous styles */
#forum-permissions {
	float: right;
	margin-left: 5px;
	margin-top: 10px;
	padding-left: 5px;
	text-align: right;
	width: auto;
}

.copyright {
	font-size: 1em;
	margin: 0;
	padding: 5px 5px 0;
	text-align: center;
	width: 100%;
}

.small {
	font-size: 0.9em !important;
}

.titlespace {
	margin-bottom: 15px;
}

.headerspace {
	margin-top: 20px;
}

.error {
	color: #bcbcbc;
	font-weight: bold;
	font-size: 1em;
}

.reported {
	background-color: #f7f7f7;
}

li.reported:hover {
	background-color: #ececec;
}

div.rules {
	background-color: #ececec;
	color: #bcbcbc;
	padding: 0 10px;
	margin: 10px 0;
	font-size: 1.1em;
}

div.rules ul, div.rules ol {
	margin-left: 20px;
}

p.rules {
	background-color: #ececec;
	background-image: none;
	padding: 5px;
}

p.rules img {
	vertical-align: middle;
}

p.rules a {
	vertical-align: middle;
	padding-top: 5px;
	clear: both;
}

#top {
	position: absolute;
	top: -20px;
}

.clear {
	display: block;
	clear: both;
	font-size: 1px;
	line-height: 1px;
	background: transparent;
}
/*	--- Custom CSS for or added to proDVGFX --- */
/*	--- Save a copy before changing --- */
.postprofile {
	min-width: 20%;
}

.mainpostprofile {
	min-height: 58px;
}

.mainpostprofile-right {
	float: right;
	min-width: 200px;
	margin-right: 50px; /* fixes long timestamps being over online image */
	width: auto;
}

.tabs {
	float: left;
	margin: 5px 0;
	width: 100%;
}

ul.tabNav {
	background: url("{T_THEME_PATH}/images/bg_tabs3t.png") right 0 no-repeat;
	list-style: none;
	float: left;
	margin: 0;
	padding: 0;
	position: relative;
	width: 100%;
}

ul.tabNav li {
	background: url("{T_THEME_PATH}/images/bg_tabs3.png") left 0 no-repeat;
	display: inline;
	float: left;
	height: 25px;
	width: auto;
}

ul.tabNav li a:active, ul.tabNav li a:focus, ul.tabNav li a:link, ul.tabNav li a:visited {
	background: url("{T_THEME_PATH}/images/bg_tabs3.png") center 0 no-repeat;
	border-right: 1px solid #101010;
	border-left: 1px solid #1F1F1F;
	color: #CCC;
	display: block;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1em;
	font-weight: bold;
	line-height: 25px;
	outline: none;
	padding: 0 15px;
	text-align: center;
	text-decoration: none;
	text-shadow: -1px -1px 1px rgba(0,0,0,0.7);
	text-transform: uppercase;
}

ul.tabNav li a.selected, ul.tabNav li a:hover {
	color: #b20304;
	text-decoration: none;
}

ul.tabNav li a.first {
	background: url("{T_THEME_PATH}/images/bg_tabs3.png") left 0 no-repeat;
	border-left: none;
}

div.tabs > div {
	float: left;
	margin: 0;
}

div.tabs > div h3 {
	margin-top: 10px;
}

.tabBG {
	display: block;
	overflow: hidden;
	padding: 0;
	width: 100%;
}

.tabBG p {
	margin: 0;
	padding-bottom: 5px;
}

#insidetabBG ul, #insidetabBG2 ul, #insidetabBG3 ul, #insidetabBG4 ul, #insidetabBG5 ul {}

#insidetabBG ul li, #insidetabBG2 ul li, #insidetabBG3 ul li, #insidetabBG4 ul li, #insidetabBG5 ul li, #tab_button {
	padding-top: 5px;
}

#insidetabBG, #insidetabBG2, #insidetabBG3, #insidetabBG4, #insidetabBG5 {
	background: #101010;
	padding: 5px 5px 0 5px;
	width: auto;
}

/*#first, #second, #third, #fourth {}*/
/* Link Styles */

/* Links adjustment to correctly display an order of rtl/ltr mixed content */
a { direction: ltr; unicode-bidi: embed; }

a:link { text-decoration: none; }
a:visited { text-decoration: none; }
a:hover	{ text-decoration: underline; }
a:active { text-decoration: none; }
a:focus { outline: none; }

/* Coloured usernames */
.username-coloured {
	font-weight: bold;
	display: inline !important;
	padding: 0 !important;
}

/* Links on gradient backgrounds */
#search-box a:link, .navbg a:link, .forumbg .header a:link, .forabg .header a:link, th a:link {
	color: #FFFFFF;
	text-decoration: none;
}

#search-box a:visited, .navbg a:visited, .forumbg .header a:visited, .forabg .header a:visited, th a:visited {
	color: #FFFFFF;
	text-decoration: none;
}

#search-box a:hover, .navbg a:hover, .forumbg .header a:hover, .forabg .header a:hover, th a:hover {
	color: #ffffff;
	text-decoration: underline;
}

#search-box a:active, .navbg a:active, .forumbg .header a:active, .forabg .header a:active, th a:active {
	color: #ffffff;
	text-decoration: none;
}

/* Links for forum/topic lists */
a.forumtitle {
	font-family: "Trebuchet MS", Helvetica, Arial, Sans-serif;
	font-size: 1.2em;
	font-weight: bold;
	color: #D7D7D7;
	text-decoration: none;
}

a.forumtitle:visited {
	color: #D7D7D7; 
}

a.forumtitle:hover {
	color: #CCC;
	text-decoration: underline;
}

a.forumtitle:active {
	color: #D7D7D7;
}

a.topictitle {
	font-family: "Trebuchet MS", Helvetica, Arial, Sans-serif;
	font-size: 1.2em;
	font-weight: bold;
	color: #D7D7D7;
	text-decoration: none;
}

a.topictitle:visited {
	color: #D7D7D7;
}

a.topictitle:hover {
	color: #CCC;
	text-decoration: underline;
}

a.topictitle:active {
	color: #D7D7D7;
}

/* Post body links */
.postlink, .postlink:active {
	padding-bottom: 0;
	text-decoration: underline;
}

/* .postlink:visited { color: #bdbdbd; } */

.postlink:hover {
	text-decoration: none
}

.signature a, .signature a:visited, .signature a:hover, .signature a:active {
	border: none;
	text-decoration: underline;
	background-color: transparent;
}

/* Profile links */
.postprofile a:link, .postprofile a:visited, .postprofile dt.author a {
	font-weight: bold;
	color: #D7D7D7;
	text-decoration: none;
}

.postprofile a:hover, .postprofile dt.author a:hover {
	color: #C7C7C7;
	text-decoration: underline;
}

/* CSS spec requires a:link, a:visited, a:hover and a:active rules to be specified in this order. */
/* See http://www.phpbb.com/bugs/phpbb3/59685 */
.postprofile a:active {
	font-weight: bold;
	color: #898989;
	text-decoration: none;
}


/* Profile searchresults */	
.search .postprofile a {
	color: #898989;
	text-decoration: none; 
	font-weight: normal;
}

.search .postprofile a:hover {
	color: #d3d3d3;
	text-decoration: underline; 
}

/* Back to top of page */
.back2top {
	clear: both;
	height: 11px;
	text-align: right;
}

a.top {
	background: none no-repeat top left;
	text-decoration: none;
	width: {IMG_ICON_BACK_TOP_WIDTH}px;
	height: {IMG_ICON_BACK_TOP_HEIGHT}px;
	display: block;
	float: right;
	overflow: hidden;
	letter-spacing: 1000px;
	text-indent: 11px;
}

a.top2 {
	background: none no-repeat 0 50%;
	text-decoration: none;
	padding-left: 15px;
}

/* Arrow links  */
a.up		{ background: none no-repeat left center; }
a.down		{ background: none no-repeat right center; }
a.left		{ background: none no-repeat 3px 60%; }
a.right		{ background: none no-repeat 95% 60%; }

a.up, a.up:link, a.up:active, a.up:visited {
	padding-left: 10px;
	text-decoration: none;
	border-bottom-width: 0;
}

a.up:hover {
	background-position: left top;
	background-color: transparent;
}

a.down, a.down:link, a.down:active, a.down:visited {
	padding-right: 10px;
}

a.down:hover {
	background-position: right bottom;
	text-decoration: none;
}

a.left, a.left:active, a.left:visited {
	padding-left: 12px;
}

a.left:hover {
	color: #d2d2d2;
	text-decoration: none;
	background-position: 0 60%;
}

a.right, a.right:active, a.right:visited {
	padding-right: 12px;
}

a.right:hover {
	color: #d2d2d2;
	text-decoration: none;
	background-position: 100% 60%;
}

/* invisible skip link, used for accessibility  */
.skiplink {
	left: -999px;
	position: absolute;
	width: 990px;
}

/* Feed icon in forumlist_body.html */
a.feed-icon-forum {
	float: right;
	margin: 3px;
}
/* Content Styles
---------------------------------------- */

ul.topiclist {
	display: block;
	list-style-type: none;
	margin: 0;
}

ul.forums {
	background: #f9f9f9 none repeat-x 0 0;
}

ul.topiclist li {
	display: block;
	list-style-type: none;
	color: #777777;
	margin: 0;
}

ul.topiclist dl {
	position: relative;
}

ul.topiclist li.row dl {
	padding: 2px 0;
}

ul.topiclist dt {
	display: block;
	float: left;
	width: 50%;
	font-size: 1.1em;
	padding-left: 5px;
	padding-right: 5px;
}

ul.topiclist dd {
	border-left: 1px solid;
	display: block;
	float: left;
	padding: 4px 0;
}

ul.topiclist dfn {
	/* Labels for post/view counts */
	position: absolute;
	left: -999px;
	width: 990px;
}

ul.topiclist li.row dt a.subforum {
	background-image: none;
	background-position: 0 50%;
	background-repeat: no-repeat;
	position: relative;
	white-space: nowrap;
	padding: 0 0 0 12px;
}

.forum-image {
	float: left;
	padding-top: 5px;
	margin-right: 5px;
}

li.row {
	border-top: 1px solid;
	border-bottom: 1px solid;
}

li.row strong {
	font-weight: normal;
}

/*
li.row:hover {}
*/

li.row:hover dd {
	border-left-color: transparent;
}

li.header dt, li.header dd {
	line-height: 1em;
	border-left-width: 0;
	margin: 2px 0 4px 0;
	padding-top: 2px;
	padding-bottom: 2px;
	font-size: 1em;
	font-family: Arial, Helvetica, sans-serif;
	text-transform: uppercase;
}

li.header dt {
	font-weight: bold;
}

li.header dd {
	margin-left: 1px;
}

li.header dl.icon {
	min-height: 0;
}

li.header dl.icon dt {
	/* Tweak for headers alignment when folder icon used */
	padding-left: 0;
	padding-right: 50px;
}

/* Forum list column styles */
dl.icon {
	/* Position of folder icon */
	background-position: 10px 50%;
	background-repeat: no-repeat;
	min-height: 35px;
}

dl.icon dt {
	background-repeat: no-repeat;
	/* Position of topic icon */
	background-position: 5px 95%;
	/* Space for folder icon */
	padding-left: 45px;
}

dd.posts, dd.topics, dd.views {
	width: 8%;
	text-align: center;
	line-height: 2.2em;
	font-size: 1.2em;
}

/* List in forum description */
dl.icon dt ol,
dl.icon dt ul {
	list-style-position: inside;
	margin-left: 1em;
}

dl.icon dt li {
	display: list-item;
	list-style-type: inherit;
}

dd.lastpost {
	width: 25%;
	font-size: 1.1em;
}

dd.redirect {
	font-size: 1.1em;
	line-height: 2.5em;
}

dd.moderation {
	font-size: 1.1em;
}

dd.lastpost span, ul.topiclist dd.searchby span, ul.topiclist dd.info span, ul.topiclist dd.time span, dd.redirect span, dd.moderation span {
	display: block;
	padding-left: 10px;
}

dd.time {
	width: auto;
	line-height: 200%;
	font-size: 1.1em;
}

dd.extra {
	width: 12%;
	line-height: 200%;
	text-align: center;
	font-size: 1.1em;
}

dd.mark {
	float: right !important;
	width: 9%;
	text-align: center;
	line-height: 200%;
	font-size: 1.2em;
}

dd.info {
	width: 30%;
}

dd.option {
	width: 15%;
	line-height: 200%;
	text-align: center;
	font-size: 1.1em;
}

dd.searchby {
	width: 47%;
	font-size: 1.1em;
	line-height: 1em;
}

ul.topiclist dd.searchextra {
	margin-left: 5px;
	padding: 0.2em 0;
	font-size: 1.1em;
	border-left: none;
	clear: both;
	width: 98%;
	overflow: hidden;
}

/* Container for post/reply buttons and pagination */
.topic-actions {
	font-size: 1.1em;
	height: 28px;
	margin-bottom: 3px;
	min-height: 28px;
}
div[class].topic-actions {
	height: auto;
}

/* Post body styles */
.postbody {
	clear: both;
	float: left;
	line-height: 1.48em;
	padding: 0;
	width: 100%;
}

.postbody .ignore {
	font-size: 1.1em;
}

.postbody h3.first {
	/* The first post on the page uses this */
	font-size: 1.7em;
}

.postbody h3 {
	/* Postbody requires a different h3 format - so change it here */
	font-size: 1.5em;
	padding: 2px 0 0 0;
	margin: 0 0 0.3em 0 !important;
	text-transform: none;
	border: none;
	font-family: "Trebuchet MS", Verdana, Helvetica, Arial, sans-serif;
	line-height: 125%;
}

.postbody h3 img {
	/* Also see tweaks.css */
	vertical-align: bottom;
}

.postbody .content {
	font-size: 1.3em;
}

.search .postbody {
	width: 68%
}

/* Topic review panel */
#review {
	margin-top: 2em;
}

#topicreview {
	padding-right: 5px;
	overflow: auto;
	height: 300px;
}

#topicreview .postbody {
	width: auto;
	float: none;
	margin: 0;
	height: auto;
}

#topicreview .post {
	height: auto;
}

#topicreview h2 {
	border-bottom-width: 0;
}

.post-ignore .postbody {
	display: none;
}

/* MCP Post details
----------------------------------------*/
#post_details
{
	/* This will only work in IE7+, plus the others */
	overflow: auto;
	max-height: 300px;
}

#expand
{
	clear: both;
}

/* Content container styles */
.content {
	min-height: 3em;
	overflow: hidden;
	line-height: 1.4em;
	font-family: "Lucida Grande", "Trebuchet MS", Verdana, Helvetica, Arial, sans-serif;
	font-size: 1em;
	padding-bottom: 1px;
}

.content h2, .panel h2 {
	font-weight: normal;
	color: #989898;
	border-bottom: 1px solid #CCCCCC;
	font-size: 1.6em;
	margin-top: 0.5em;
	margin-bottom: 0.5em;
	padding-bottom: 0.5em;
}

.panel h3 {
	margin: 0.5em 0;
}

.panel p {
	font-size: 1.2em;
	margin-bottom: 1em;
	line-height: 1.4em;
}

.content p {
	font-family: "Lucida Grande", "Trebuchet MS", Verdana, Helvetica, Arial, sans-serif;
	font-size: 1.2em;
	margin-bottom: 1em;
	line-height: 1.4em;
}

dl.faq {
	font-family: "Lucida Grande", Verdana, Helvetica, Arial, sans-serif;
	font-size: 1.1em;
	margin-top: 1em;
	margin-bottom: 2em;
	line-height: 1.4em;
}

dl.faq dt {
	font-weight: bold;
	color: #333333;
}

.content dl.faq {
	font-size: 1.2em;
	margin-bottom: 0.5em;
}

.content li {
	list-style-type: inherit;
}

.content ul, .content ol {
	margin-bottom: 1em;
	margin-left: 3em;
}

.posthilit {
	background-color: #f3f3f3;
	color: #BCBCBC;
	padding: 0 2px 1px 2px;
}

.announce, .unreadpost {
	/* Highlight the announcements & unread posts box */
	border-left-color: #BCBCBC;
	border-right-color: #BCBCBC;
}

/* Post author */
p.author {
	margin: 0 15em 0.6em 0;
	padding: 0 0 5px 0;
	font-family: Verdana, Helvetica, Arial, sans-serif;
	font-size: 1em;
	line-height: 1.2em;
}

/* Post signature */
.signature {
	border-top: 1px solid;
	clear: left;
	font-size: 1.1em;
	line-height: 140%;
	overflow: hidden;
	margin-top: 1.5em;
	padding-top: 0.2em;
	width: 100%;
}

dd .signature {
	margin: 0;
	padding: 0;
	clear: none;
	border: none;
}

.signature li {
	list-style-type: inherit;
}

.signature ul, .signature ol {
	margin-bottom: 1em;
	margin-left: 3em;
}

/* Post noticies */
.notice {
	font-family: "Lucida Grande", Verdana, Helvetica, Arial, sans-serif;
	width: auto;
	margin-top: 1.5em;
	padding-top: 0.2em;
	font-size: 1em;
	border-top: 1px dashed #CCCCCC;
	clear: left;
	line-height: 130%;
}

/* Jump to post link for now */
ul.searchresults {
	list-style: none;
	text-align: right;
	clear: both;
}

/* BB Code styles
----------------------------------------*/
/* Quote block */
blockquote {
	background: #ebebeb none 6px 8px no-repeat;
	border: 1px solid #dbdbdb;
	font-size: 0.95em;
	margin: 0.5em 1px 0 25px;
	overflow: hidden;
	padding: 5px;
}

blockquote blockquote {
	/* Nested quotes */
	background-color: #bababa;
	font-size: 1em;
	margin: 0.5em 1px 0 15px;	
}

blockquote blockquote blockquote {
	/* Nested quotes */
	background-color: #e4e4e4;
}

blockquote cite {
	/* Username/source of quoter */
	font-style: normal;
	font-weight: bold;
	margin-left: 20px;
	display: block;
	font-size: 0.9em;
}

blockquote cite cite {
	font-size: 1em;
}

blockquote.uncited {
	padding-top: 25px;
}

/* Code block */
dl.codebox {
	padding: 3px;
	background-color: #FFFFFF;
	border: 1px solid #d8d8d8;
	font-size: 1em;
}

dl.codebox dt {
	text-transform: uppercase;
	border-bottom: 1px solid #CCCCCC;
	margin-bottom: 3px;
	font-size: 0.8em;
	font-weight: bold;
	display: block;
}

blockquote dl.codebox {
	margin-left: 0;
}

dl.codebox code {
	/* Also see tweaks.css */
	overflow: auto;
	display: block;
	height: auto;
	max-height: 200px;
	white-space: normal;
	padding-top: 5px;
	font: 0.9em Monaco, "Andale Mono","Courier New", Courier, mono;
	line-height: 1.3em;
	color: #8b8b8b;
	margin: 2px 0;
}

.syntaxbg		{ color: #FFFFFF; }
.syntaxcomment	{ color: #000000; }
.syntaxdefault	{ color: #bcbcbc; }
.syntaxhtml		{ color: #000000; }
.syntaxkeyword	{ color: #585858; }
.syntaxstring	{ color: #a7a7a7; }

/* Attachments
----------------------------------------*/
.attachbox {
	float: left;
	width: auto; 
	margin: 5px 5px 5px 0;
	padding: 6px;
	background-color: #FFFFFF;
	border: 1px dashed #d8d8d8;
	clear: left;
}

.pm-message .attachbox {
	background-color: #f3f3f3;
}

.attachbox dt {
	font-family: Arial, Helvetica, sans-serif;
	text-transform: uppercase;
}

.attachbox dd {
	margin-top: 4px;
	padding-top: 4px;
	clear: left;
	border-top: 1px solid #d8d8d8;
}

.attachbox dd dd {
	border: none;
}

.attachbox p {
	line-height: 110%;
	color: #666666;
	font-weight: normal;
	clear: left;
}

.attachbox p.stats
{
	line-height: 110%;
	color: #666666;
	font-weight: normal;
	clear: left;
}

.attach-image {
	margin: 3px 0;
	width: 100%;
	max-height: 350px;
	overflow: auto;
}

.attach-image img {
	border: 1px solid #999999;
/*	cursor: move; */
	cursor: default;
}

/* Inline image thumbnails */
div.inline-attachment dl.thumbnail, div.inline-attachment dl.file {
	display: block;
	margin-bottom: 4px;
}

div.inline-attachment p {
	font-size: 100%;
}

dl.file {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	display: block;
}

dl.file dt {
	text-transform: none;
	margin: 0;
	padding: 0;
	font-weight: bold;
	font-family: Verdana, Arial, Helvetica, sans-serif;
}

dl.file dd {
	color: #666666;
	margin: 0;
	padding: 0;	
}

dl.thumbnail img {
	padding: 3px;
	border: 1px solid #666666;
	background-color: #FFF;
}

dl.thumbnail dd {
	color: #666666;
	font-style: italic;
	font-family: Verdana, Arial, Helvetica, sans-serif;
}

.attachbox dl.thumbnail dd {
	font-size: 100%;
}

dl.thumbnail dt a:hover {
	background-color: #EEEEEE;
}

dl.thumbnail dt a:hover img {
	border: 1px solid;
}

/* Post poll styles */
fieldset.polls {
	font-family: "Trebuchet MS", Verdana, Helvetica, Arial, sans-serif;
}

fieldset.polls dl {
	border-top: 1px solid;
	line-height: 120%;
	margin-top: 5px;
	padding: 5px 0 0 0;
}

fieldset.polls dl.voted {
	font-weight: bold;
}

fieldset.polls dt {
	border-right: none;
	display: block;
	float: left;
	font-size: 1.1em;
	margin: 0;
	padding: 0;
	text-align: left;
	width: 30%;
}

fieldset.polls dd {
	border-left: none;
	float: left;
	font-size: 1.1em;
	margin-left: 0;
	padding: 0 5px;
	width: 10%;
}

fieldset.polls dd.resultbar {
	width: 50%;
}

fieldset.polls dd input {
	margin: 2px 0;
}

fieldset.polls dd div {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
	min-width: 2%;
	overflow: visible;
	padding: 0 2px;
	text-align: right;
}

/* Poster profile block */
.mainpostprofile {
	border-bottom: 1px dotted;
	float: left;
	margin-bottom: 8px !important;
	/*min-height: 80px;*/
	padding-bottom: 5px;
	width: 100%;
}

.postprofile {
	/* Also see tweaks.css */
	float: left;
}
.pm .postprofile {
	border-left: 1px solid transparent;
}

.postprofile dd, .postprofile dt {
	line-height: 1.2em;
	margin-left: 5px;
	width: auto;
}

/* Avatar position */
.postprofile img {
	background-position: top left !important;
	float: left;
	padding-right: 3px;
	width: auto;
}

.postprofile strong {
	font-weight: 600;
}

.avatar {
	border: none;
	margin-bottom: 3px;
}

.online {
	background-image: none;
	background-position: 100% 0;
	background-repeat: no-repeat;
}

/* Poster profile used by search*/
.search .postprofile {
	width: 30%;
}

/* pm list in compose message if mass pm is enabled */
dl.pmlist dt {
	width: 60% !important;
}

dl.pmlist dt textarea {
	width: 95%;
}

dl.pmlist dd {
	margin-left: 61% !important;
	margin-bottom: 2px;
}
/* Button Styles */

/* Rollover buttons
   Based on: http://wellstyled.com/css-nopreload-rollovers.html
*/
.buttons {
	float: left;
	width: auto;
	height: auto;
}

/* Rollover state */
.buttons div {
	float: left;
	margin: 0 5px 0 0;
	background-position: 0 100%;
}

/* Rolloff state */
.buttons div a {
	display: block;
	width: 100%;
	height: 100%;
	background-position: 0 0;
	position: relative;
	overflow: hidden;
}

/* Hide <a> text and hide off-state image when rolling over (prevents flicker in IE) */
/*.buttons div span		{ display: none; }*/
/*.buttons div a:hover	{ background-image: none; }*/
.buttons div span			{ position: absolute; width: 100%; height: 100%; cursor: pointer;}
.buttons div a:hover span	{ background-position: 0 100%; }

/* Big button images */
.reply-icon span	{ background: transparent none 0 0 no-repeat; }
.post-icon span		{ background: transparent none 0 0 no-repeat; }
.locked-icon span	{ background: transparent none 0 0 no-repeat; }
.pmreply-icon span	{ background: none 0 0 no-repeat; }
.newpm-icon span 	{ background: none 0 0 no-repeat; }
.forwardpm-icon span 	{ background: none 0 0 no-repeat; }

/* Set big button dimensions */
.buttons div.reply-icon		{ width: {IMG_BUTTON_TOPIC_REPLY_WIDTH}px; height: {IMG_BUTTON_TOPIC_REPLY_HEIGHT}px; }
.buttons div.post-icon		{ width: {IMG_BUTTON_TOPIC_NEW_WIDTH}px; height: {IMG_BUTTON_TOPIC_NEW_HEIGHT}px; }
.buttons div.locked-icon	{ width: {IMG_BUTTON_TOPIC_LOCKED_WIDTH}px; height: {IMG_BUTTON_TOPIC_LOCKED_HEIGHT}px; }
.buttons div.pmreply-icon	{ width: {IMG_BUTTON_PM_REPLY_WIDTH}px; height: {IMG_BUTTON_PM_REPLY_HEIGHT}px; }
.buttons div.newpm-icon		{ width: {IMG_BUTTON_PM_NEW_WIDTH}px; height: {IMG_BUTTON_PM_NEW_HEIGHT}px; }
.buttons div.forwardpm-icon	{ width: {IMG_BUTTON_PM_FORWARD_WIDTH}px; height: {IMG_BUTTON_PM_FORWARD_HEIGHT}px; }

/* Sub-header (navigation bar)
--------------------------------------------- */
a.print, a.sendemail, a.fontsize {
	display: block;
	overflow: hidden;
	height: 18px;
	text-indent: -5000px;
	text-align: left;
	background-repeat: no-repeat;
}

a.print {
	background-image: none;
	width: 22px;
}

a.sendemail {
	background-image: none;
	width: 22px;
}

a.fontsize {
	background-image: none;
	background-position: 0 -1px;
	width: 29px;
}

a.fontsize:hover {
	background-position: 0 -20px;
	text-decoration: none;
}

/* Icon images
---------------------------------------- */
.sitehome, .icon-faq, .icon-members, .icon-home, .icon-ucp, .icon-register, .icon-logout,
.icon-bookmark, .icon-bump, .icon-subscribe, .icon-unsubscribe, .icon-pages, .icon-search {
	background-position: 0 50%;
	background-repeat: no-repeat;
	background-image: none;
	padding: 1px 0 0 17px;
}

/* Poster profile icons
----------------------------------------*/
ul.profile-icons {
	padding-top: 10px;
	list-style: none;
}

/* Rollover state */
ul.profile-icons li {
	float: left;
	margin: 0 6px 3px 0;
	background-position: 0 100%;
}

/* Rolloff state */
ul.profile-icons li a {
	display: block;
	width: 100%;
	height: 100%;
	background-position: 0 0;
}

/* Hide <a> text and hide off-state image when rolling over (prevents flicker in IE) */
ul.profile-icons li span { display:none; }
ul.profile-icons li a:hover { background: none; }

/* Positioning of moderator icons */
.postbody ul.profile-icons {
	float: right;
	width: auto;
	padding: 0;
}

.postbody ul.profile-icons li {
	margin: 0 3px;
}

/* Profile & navigation icons */
.email-icon, .email-icon a		{ background: none top left no-repeat; }
.aim-icon, .aim-icon a			{ background: none top left no-repeat; }
.yahoo-icon, .yahoo-icon a		{ background: none top left no-repeat; }
.web-icon, .web-icon a			{ background: none top left no-repeat; }
.msnm-icon, .msnm-icon a			{ background: none top left no-repeat; }
.icq-icon, .icq-icon a			{ background: none top left no-repeat; }
.jabber-icon, .jabber-icon a		{ background: none top left no-repeat; }
.pm-icon, .pm-icon a				{ background: none top left no-repeat; }
.quote-icon, .quote-icon a		{ background: none top left no-repeat; }

/* Moderator icons */
.report-icon, .report-icon a		{ background: none top left no-repeat; }
.warn-icon, .warn-icon a			{ background: none top left no-repeat; }
.edit-icon, .edit-icon a			{ background: none top left no-repeat; }
.delete-icon, .delete-icon a		{ background: none top left no-repeat; }
.info-icon, .info-icon a			{ background: none top left no-repeat; }

/* Set profile icon dimensions */
ul.profile-icons li.email-icon		{ width: {IMG_ICON_CONTACT_EMAIL_WIDTH}px; height: {IMG_ICON_CONTACT_EMAIL_HEIGHT}px; }
ul.profile-icons li.aim-icon	{ width: {IMG_ICON_CONTACT_AIM_WIDTH}px; height: {IMG_ICON_CONTACT_AIM_HEIGHT}px; }
ul.profile-icons li.yahoo-icon	{ width: {IMG_ICON_CONTACT_YAHOO_WIDTH}px; height: {IMG_ICON_CONTACT_YAHOO_HEIGHT}px; }
ul.profile-icons li.web-icon	{ width: {IMG_ICON_CONTACT_WWW_WIDTH}px; height: {IMG_ICON_CONTACT_WWW_HEIGHT}px; }
ul.profile-icons li.msnm-icon	{ width: {IMG_ICON_CONTACT_MSNM_WIDTH}px; height: {IMG_ICON_CONTACT_MSNM_HEIGHT}px; }
ul.profile-icons li.icq-icon	{ width: {IMG_ICON_CONTACT_ICQ_WIDTH}px; height: {IMG_ICON_CONTACT_ICQ_HEIGHT}px; }
ul.profile-icons li.jabber-icon	{ width: {IMG_ICON_CONTACT_JABBER_WIDTH}px; height: {IMG_ICON_CONTACT_JABBER_HEIGHT}px; }
ul.profile-icons li.pm-icon		{ width: {IMG_ICON_CONTACT_PM_WIDTH}px; height: {IMG_ICON_CONTACT_PM_HEIGHT}px; }
ul.profile-icons li.quote-icon	{ width: {IMG_ICON_POST_QUOTE_WIDTH}px; height: {IMG_ICON_POST_QUOTE_HEIGHT}px; }
ul.profile-icons li.report-icon	{ width: {IMG_ICON_POST_REPORT_WIDTH}px; height: {IMG_ICON_POST_REPORT_HEIGHT}px; }
ul.profile-icons li.edit-icon	{ width: {IMG_ICON_POST_EDIT_WIDTH}px; height: {IMG_ICON_POST_EDIT_HEIGHT}px; }
ul.profile-icons li.delete-icon	{ width: {IMG_ICON_POST_DELETE_WIDTH}px; height: {IMG_ICON_POST_DELETE_HEIGHT}px; }
ul.profile-icons li.info-icon	{ width: {IMG_ICON_POST_INFO_WIDTH}px; height: {IMG_ICON_POST_INFO_HEIGHT}px; }
ul.profile-icons li.warn-icon	{ width: {IMG_ICON_USER_WARN_WIDTH}px; height: {IMG_ICON_USER_WARN_HEIGHT}px; }

/* Fix profile icon default margins */
ul.profile-icons li.edit-icon	{ margin: 0 0 0 3px; }
ul.profile-icons li.quote-icon	{ margin: 0 0 0 10px; }
ul.profile-icons li.info-icon, ul.profile-icons li.report-icon	{ margin: 0 3px 0 0; }
/* Control Panel Styles */
/* Main CP box */
#cp-menu {
	float: left;
	margin-bottom: 5px;
	margin-right: 5px;
	margin-top: 1em;
	width: 15%;
}

#cp-main {
	float: left;
	width: 84%;
}

#cp-main .content {
	padding: 0;
}

#cp-main h3, #cp-main hr, #cp-menu hr {
	border-color: #000;
}

#cp-main .panel p {
	font-size: 1.1em;
}

#cp-main .panel ol {
	margin-left: 2em;
	font-size: 1.1em;
}

#cp-main .panel li.row {
	border-bottom: 1px solid;
	border-top: 1px solid;
}

ul.cplist {
	margin-bottom: 5px;
	border-top: 1px solid;
}

#cp-main .panel li.header dd, #cp-main .panel li.header dt {
	margin-bottom: 2px;
}

#cp-main table.table1 {
	margin-bottom: 1em;
}

#cp-main table.table1 thead th {
	color: #333333;
	font-weight: bold;
	border-bottom: 1px solid #333333;
	padding: 5px;
}

#cp-main table.table1 tbody th {
	font-style: italic;
	background-color: transparent !important;
	border-bottom: none;
}

#cp-main .pagination {
	float: right;
	width: auto;
	padding-top: 1px;
}

#cp-main .postbody p {
	font-size: 1.1em;
}

#cp-main .pm-message {
	border: 1px solid #e2e2e2;
	margin: 10px 0;
	background-color: #FFFFFF;
	width: auto;
	float: none;
}

.pm-message h2 {
	padding-bottom: 5px;
}

#cp-main .postbody h3, #cp-main .box2 h3 {
	margin-top: 0;
}

#cp-main .buttons {
	margin-left: 0;
}

#cp-main ul.linklist {
	margin: 0;
}

/* MCP Specific tweaks */
.mcp-main .postbody {
	width: 100%;
}

.tabs-container h2 {
	float: left;
	margin-bottom: 0px;
}

.tabs-container #minitabs {
	float: right;
	margin-top: 19px;
}

.tabs-container:after {
	display: block;
	clear: both;
	content: '';
}

/* CP tabbed menu
----------------------------------------*/
#tabs {
	line-height: normal;
	margin: 20px 0 -1px 7px;
	min-width: 570px;
}

#tabs ul {
	margin:0;
	padding: 0;
	list-style: none;
}

#tabs li {
	display: inline;
	margin: 0;
	padding: 0;
	font-size: 1em;
	font-weight: bold;
}

#tabs a {
	float: left;
	background: none no-repeat 0% -35px;
	margin: 0 1px 0 0;
	padding: 0 0 0 5px;
	text-decoration: none;
	position: relative;
	cursor: pointer;
}

#tabs a span {
	float: left;
	display: block;
	background: none no-repeat 100% -35px;
	padding: 6px 10px 6px 5px;
	color: #828282;
	white-space: nowrap;
}

#tabs a:hover span {
	color: #bcbcbc;
}

#tabs .activetab a {
	background-position: 0 0;
	border-bottom: 1px solid #ebebeb;
}

#tabs .activetab a span {
	background-position: 100% 0;
	padding-bottom: 7px;
	color: #333333;
}

#tabs a:hover {
	background-position: 0 -70px;
}

#tabs a:hover span {
	background-position:100% -70px;
}

#tabs .activetab a:hover {
	background-position: 0 0;
}

#tabs .activetab a:hover span {
	color: #000000;
	background-position: 100% 0;
}

/* Mini tabbed menu used in MCP
----------------------------------------*/
#minitabs {
	line-height: normal;
	margin: -20px 7px 0 0;
}

#minitabs ul {
	margin:0;
	padding: 0;
	list-style: none;
}

#minitabs li {
	display: block;
	float: right;
	padding: 0 10px 4px 10px;
	font-size: 1em;
	font-weight: bold;
	background-color: #f2f2f2;
	margin-left: 2px;
}

#minitabs a {
}

#minitabs a:hover {
	text-decoration: none;
}

#minitabs li.activetab {
	background-color: #F9F9F9;
}

#minitabs li.activetab a, #minitabs li.activetab a:hover {
	color: #333333;
}

/* UCP navigation menu
----------------------------------------*/
/* Container for sub-navigation list */
#navigation {
	width: 100%;
	padding-top: 36px;
}

#navigation ul {
	list-style:none;
}

/* Default list state */
#navigation li {
	margin: 1px 0;
	padding: 0;
	font-weight: bold;
	display: inline;
}

/* Link styles for the sub-section links */
#navigation a {
	display: block;
	padding: 5px;
	margin: 1px 0;
	text-decoration: none;
	font-weight: bold;
	color: #333;
	background: #cfcfcf none repeat-y 100% 0;
}

#navigation a:hover {
	text-decoration: none;
	background-color: #c6c6c6;
	color: #bcbcbc;
	background-image: none;
}

#navigation #active-subsection a {
	display: block;
	color: #d3d3d3;
	background-color: #F9F9F9;
	background-image: none;
}

#navigation #active-subsection a:hover {
	color: #d3d3d3;
}

/* Preferences pane layout
----------------------------------------*/
#cp-main h2 {
	border-bottom: none;
	padding: 0;
	margin-left: 10px;
	color: #333333;
}

#cp-main .panel {
	background-color: #F9F9F9;
}

#cp-main .pm {
	background-color: #FFFFFF;
}

#cp-main span.corners-top, #cp-menu span.corners-top {
	background-image: none;
}

#cp-main span.corners-top span, #cp-menu span.corners-top span {
	background-image: none;
}

#cp-main span.corners-bottom, #cp-menu span.corners-bottom {
	background-image: none;
}

#cp-main span.corners-bottom span, #cp-menu span.corners-bottom span {
	background-image: none;
}

/* Topicreview */
#cp-main .panel #topicreview span.corners-top, #cp-menu .panel #topicreview span.corners-top {
	background-image: none;
}

#cp-main .panel #topicreview span.corners-top span, #cp-menu .panel #topicreview span.corners-top span {
	background-image: none;
}

#cp-main .panel #topicreview span.corners-bottom, #cp-menu .panel #topicreview span.corners-bottom {
	background-image: none;
}

#cp-main .panel #topicreview span.corners-bottom span, #cp-menu .panel #topicreview span.corners-bottom span {
	background-image: none;
}

/* Friends list */
.cp-mini {
	padding: 0 5px;
	margin: 10px 0;
}

.cp-mini span.corners-top, .cp-mini span.corners-bottom {
	margin: 0 -5px;
}

dl.mini dt {
	font-weight: bold;
}

dl.mini dd {
	padding-top: 4px;
}

.friend-online {
	font-weight: bold;
}

.friend-offline {
	font-style: italic;
}

/* PM Styles */
#pm-menu {
	line-height: 2.5em;
}

/* PM panel adjustments */
.reply-all a.left {
	background-position: 3px 60%;
}

.reply-all a.left:hover {
	background-position: 0px 60%;
}

.reply-all {
	font-size: 11px;
	padding-top: 5px;
}

/* PM Message history */
.current {}

/* Defined rules list for PM options */
ol.def-rules {
	padding-left: 0;
}

ol.def-rules li {
	line-height: 180%;
	padding: 1px;
}

/* PM marking colours */
.pmlist li.bg1 {
	padding: 0 3px;
}

.pmlist li.bg2 {
	padding: 0 3px;
}

.pmlist li.pm_message_reported_colour, .pm_message_reported_colour {}

.pmlist li.pm_marked_colour, .pm_marked_colour {
	padding: 0;
	border: solid 3px #ffffff;
	border-width: 0 3px;
}

.pmlist li.pm_replied_colour, .pm_replied_colour {
	padding: 0;
	border: solid 3px #c2c2c2;
	border-width: 0 3px;
}

.pmlist li.pm_friend_colour, .pm_friend_colour {
	padding: 0;
	border: solid 3px #bdbdbd;
	border-width: 0 3px;
}

.pmlist li.pm_foe_colour, .pm_foe_colour {
	padding: 0;
	border: solid 3px #000000;
	border-width: 0 3px;
}

.pm-legend {
	border-left-width: 10px;
	border-left-style: solid;
	border-right-width: 0;
	margin-bottom: 3px;
	padding-left: 3px;
}

/* Avatar gallery */
#gallery label {
	position: relative;
	float: left;
	margin: 10px;
	padding: 5px;
	width: auto;
	background: #FFFFFF;
	border: 1px solid #CCC;
	text-align: center;
}

#gallery label:hover {
	background-color: #EEE;
}
/* Form Styles
---------------------------------------- */

/* General form styles
----------------------------------------*/
fieldset {
	border-width: 0;
	font-family: Verdana, Helvetica, Arial, sans-serif;
	font-size: 1.1em;
}

input {
	font-weight: normal;
	cursor: pointer;
	vertical-align: middle;
	padding: 0 3px;
	font-size: 1em;
	font-family: Verdana, Helvetica, Arial, sans-serif;
}

select {
	font-family: Verdana, Helvetica, Arial, sans-serif;
	font-weight: normal;
	cursor: pointer;
	vertical-align: middle;
	border: 1px solid #666666;
	padding: 1px;
	background-color: #FAFAFA;
	font-size: 1em;
}

option {
	padding-right: 1em;
}

option.disabled-option {
	color: graytext;
}

textarea {
	font-family: "Lucida Grande", Verdana, Helvetica, Arial, sans-serif;
	width: 60%;
	padding: 2px;
	font-size: 1em;
	line-height: 1.4em;
}

label {
	cursor: default;
	padding-right: 5px;
	color: #676767;
}

label input {
	vertical-align: middle;
}

label img {
	vertical-align: middle;
}

/* Definition list layout for forms
---------------------------------------- */
fieldset dl {
	padding: 4px 0;
}

fieldset dt {
	float: left;	
	width: 40%;
	text-align: left;
	display: block;
}

fieldset dd {
	margin-left: 41%;
	vertical-align: top;
	margin-bottom: 3px;
}

/* Specific layout 1 */
fieldset.fields1 dt {
	width: 15em;
	border-right-width: 0;
}

fieldset.fields1 dd {
	margin-left: 15em;
	border-left-width: 0;
}

fieldset.fields1 {
	background-color: transparent;
}

fieldset.fields1 div {
	margin-bottom: 3px;
}

/* Set it back to 0px for the reCaptcha divs: PHPBB3-9587 */
fieldset.fields1 #recaptcha_widget_div div {
	margin-bottom: 0;
}

/* Specific layout 2 */
fieldset.fields2 dt {
	width: 15em;
	border-right-width: 0;
}

fieldset.fields2 dd {
	margin-left: 16em;
	border-left-width: 0;
}

/* Form elements */
dt label {
	font-weight: bold;
	text-align: left;
}

dd label {
	white-space: nowrap;
	color: #333;
}

dd input, dd textarea {
	margin-right: 3px;
}

dd select {
	width: auto;
}

dd textarea {
	width: 85%;
}

/* Hover effects */
fieldset dl:hover dt label {
	color: #000000;
}

fieldset.fields2 dl:hover dt label {
	color: inherit;
}

#timezone {
	width: 95%;
}

* html #timezone {
	width: 50%;
}

/* Quick-login on index page */
fieldset.quick-login {
	margin-top: 5px;
}

fieldset.quick-login input {
	width: auto;
}

fieldset.quick-login input.inputbox {
	width: 15%;
	vertical-align: middle;
	margin-right: 5px;
	background-color: #f3f3f3;
}

fieldset.quick-login label {
	white-space: nowrap;
	padding-right: 2px;
}

/* Display options on viewtopic/viewforum pages  */
fieldset.display-options {
	text-align: center;
	margin: 3px 0 5px 0;
}

fieldset.display-options label {
	white-space: nowrap;
	padding-right: 2px;
}

fieldset.display-options a {
	margin-top: 3px;
}

/* Display actions for ucp and mcp pages */
fieldset.display-actions {
	text-align: right;
	line-height: 2em;
	white-space: nowrap;
	padding-right: 1em;
}

fieldset.display-actions label {
	white-space: nowrap;
	padding-right: 2px;
}

fieldset.sort-options {
	line-height: 2em;
}

/* MCP forum selection*/
fieldset.forum-selection {
	margin: 5px 0 3px 0;
	float: right;
}

fieldset.forum-selection2 {
	margin: 13px 0 3px 0;
	float: right;
}

/* Jumpbox */
fieldset.jumpbox {
	text-align: right;
	margin-top: 15px;
	height: 2.5em;
}

fieldset.quickmod {
	width: 50%;
	float: right;
	text-align: right;
	height: 2.5em;
}

/* Submit button fieldset */
fieldset.submit-buttons {
	text-align: center;
	vertical-align: middle;
	margin: 5px 0;
}

fieldset.submit-buttons input {
	vertical-align: middle;
	padding-top: 3px;
	padding-bottom: 3px;
}

/* Posting page styles
----------------------------------------*/

/* Buttons used in the editor */
#format-buttons {
	margin: 15px 0 2px 0;
}

#format-buttons input, #format-buttons select {
	vertical-align: middle;
}

/* Main message box */
#message-box {
	width: 80%;
}

#message-box textarea {
	font-family: "Trebuchet MS", Verdana, Helvetica, Arial, sans-serif;
	width: 450px;
	height: 270px;
	min-width: 100%;
	max-width: 100%;
	font-size: 1.2em;
	color: #333333;
}

/* Emoticons panel */
#smiley-box {
	width: 18%;
	float: right;
}

#smiley-box img {
	margin: 3px;
}

/* Input field styles
---------------------------------------- */
.inputbox {
	background-color: #FFFFFF;
	border: 1px solid #c0c0c0;
	color: #333333;
	padding: 2px;
	cursor: text;
}

.inputbox:hover {
	border: 1px solid #eaeaea;
}

.inputbox:focus {
	border: 1px solid #eaeaea;
	color: #4b4b4b;
}

input.inputbox	{ width: 85%; }
input.medium	{ width: 50%; }
input.narrow	{ width: 25%; }
input.tiny		{ width: 125px; }

textarea.inputbox {
	width: 85%;
}

.autowidth {
	width: auto !important;
}

/* Form button styles
---------------------------------------- */
input.button1, input.button2 {
	font-size: 1em;
}

a.button1, input.button1, input.button3, a.button2, input.button2 {
	width: auto !important;
	padding-top: 1px;
	padding-bottom: 1px;
	font-family: "Lucida Grande", Verdana, Helvetica, Arial, sans-serif;
	color: #000;
	background: #FAFAFA none repeat-x top left;
}

a.button1, input.button1 {
	font-weight: bold;
	border: 1px solid #666666;
}

input.button3 {
	padding: 0;
	margin: 0;
	line-height: 5px;
	height: 12px;
	background-image: none;
	font-variant: small-caps;
}

/* Alternative button */
a.button2, input.button2, input.button3 {
	border: 1px solid #666666;
}

/* <a> button in the style of the form buttons */
a.button1, a.button1:link, a.button1:visited, a.button1:active, a.button2, a.button2:link, a.button2:visited, a.button2:active {
	text-decoration: none;
	color: #000000;
	padding: 2px 8px;
	line-height: 250%;
	vertical-align: text-bottom;
	background-position: 0 1px;
}

/* Hover states */
a.button1:hover, input.button1:hover, a.button2:hover, input.button2:hover, input.button3:hover {
	border: 1px solid #BCBCBC;
	background-position: 0 100%;
	color: #BCBCBC;
}

input.disabled {
	font-weight: normal;
	color: #666666;
}

/* Topic and forum Search */
.search-box {
	margin-top: 3px;
	margin-left: 5px;
	float: left;
}

.search-box input {
}

input.search {
	background-image: none;
	background-repeat: no-repeat;
	background-position: left 1px;
	padding-left: 17px;
}

.full { width: 95%; }
.medium { width: 50%;}
.narrow { width: 25%;}
.tiny { width: 10%;}
/* Style Sheet Tweaks

These style definitions are mainly IE specific 
tweaks required due to its poor CSS support.
-------------------------------------------------*/

* html table, * html select, * html input { font-size: 100%; }
* html hr { margin: 0; }
* html span.corners-top, * html span.corners-bottom { background-image: url("{T_THEME_PATH}/images/corners_left.gif"); }
* html span.corners-top span, * html span.corners-bottom span { background-image: url("{T_THEME_PATH}/images/corners_right.gif"); }

table.table1 {
	width: 99%;		/* IE < 6 browsers */
	/* Tantek hack */
	voice-family: "\"}\"";
	voice-family: inherit;
	width: 100%;
}
html>body table.table1 { width: 100%; }	/* Reset 100% for opera */

* html ul.topiclist li { position: relative; }
* html .postbody h3 img { vertical-align: middle; }

/* Form styles */
html>body dd label input { vertical-align: text-bottom; }	/* Align checkboxes/radio buttons nicely */

* html input.button1, * html input.button2 {
	padding-bottom: 0;
	margin-bottom: 1px;
}

/* Misc layout styles */
* html .column1, * html .column2 { width: 45%; }

/* Nice method for clearing floated blocks without having to insert any extra markup (like spacer above)
   From http://www.positioniseverything.net/easyclearing.html 
#tabs:after, #minitabs:after, .post:after, .navbar:after, fieldset dl:after, ul.topiclist dl:after, ul.linklist:after, dl.polls:after {
	content: "."; 
	display: block; 
	height: 0; 
	clear: both; 
	visibility: hidden;
}*/

.clearfix, #tabs, #minitabs, fieldset dl, ul.topiclist dl, dl.polls {
	height: 1%;
	overflow: hidden;
}

/* viewtopic fix */
* html .post {
	height: 25%;
	overflow: hidden;
}

/* navbar fix */
* html .clearfix, * html .navbar, ul.linklist {
	height: 4%;
	overflow: hidden;
}

/* Simple fix so forum and topic lists always have a min-height set, even in IE6
	From http://www.dustindiaz.com/min-height-fast-hack */
dl.icon {
	min-height: 35px;
	height: auto !important;
	height: 35px;
}

* html li.row dl.icon dt {
	height: 35px;
	overflow: visible;
}

* html #search-box {
	width: 25%;
}

/* Correctly clear floating for details on profile view */
*:first-child+html dl.details dd {
	margin-left: 30%;
	float: none;
}

* html dl.details dd {
	margin-left: 30%;
	float: none;
}

/* Headerbar height fix for IE7 and below */
* html #site-description p {
	margin-bottom: 1.0em;
}

*:first-child+html #site-description p {
	margin-bottom: 1.0em;
}

/* #minitabs fix for IE */
.tabs-container {
	zoom: 1;
}

#minitabs {
	white-space: nowrap;
	*min-width: 50%;
}
/* Colours and backgrounds for common.css */

html, body {
	background-color: #1F1F1F;
	color: #D3D3D3;
}

#simple-wrap {
	background-color: #191919;
}

h1 {
	color: #FFFFFF;
}

h2 {
	color: #C7CFCF;
}

h3 {
	color: #CCC;
}

hr {
	border-color: #000000;
	border-top-color: #CCCCCC;
}

hr.dashed {
	border-top-color: #000;
}

/* Search box */

#search-box {
	color: #FFF;
}

#search-box #keywords {
	background-color: #FFF;
}

#search-box input {
	border-color: #090909;
}

/* Round cornered boxes and backgrounds */
.headerbar {
	background: #060606 url("{T_THEME_PATH}/images/bg_header.png") repeat-x;
	color: #F9FAF9;
}

.navbar {
	background: #2A2A2A;
	padding: 0 10px;
}

.statbar {
	background: #070707;
}
.forabg, .forumbg {
	background: #101010 url("{T_THEME_PATH}/images/bg_list.png") repeat-x;
}

.forumbg {
	background: #101010 url("{T_THEME_PATH}/images/bg_list.png") repeat-x /*#090909 url("{T_THEME_PATH}/images/f_bg.png") repeat-x*/;
}

.panel {
	background-color: #191919;
	color: #D9D4D4;
}

.post:target .content {
	color: #D7D4D4;
}

.post:target h3 a {
	color: #D7D4D4;
}

.bg1, .bg2, .bg3 {
	background: #2A2A2A url("{T_THEME_PATH}/images/p_bg.png") repeat-x;
}

.ucprowbg {
	background-color: #1F1F1F;
}

.fieldsbg {
	background-color: #2A2A2A;
}

span.corners-top					{ background-image: url("{T_THEME_PATH}/images/corners_left.png"); }
span.corners-top span				{ background-image: url("{T_THEME_PATH}/images/corners_right.png"); }
span.corners-bottom					{ background-image: url("{T_THEME_PATH}/images/corners_left.png"); }
span.corners-bottom span				{ background-image: url("{T_THEME_PATH}/images/corners_right.png"); }
div.headerbar span.corners-top		{ background-image: url("{T_THEME_PATH}/images/corners_left2.png"); }
div.headerbar span.corners-top span	{ background-image: url("{T_THEME_PATH}/images/corners_right2.png"); }

/* Alternate/used for control panel (lighter images) */
span.corners-top2 { background-image: url("{T_THEME_PATH}/images/2corners_left.png"); }
span.corners-top2 span { background-image: url("{T_THEME_PATH}/images/2corners_right.png"); }
span.corners-bottom2 { background-image: url("{T_THEME_PATH}/images/2corners_left.png"); }
span.corners-bottom2 span { background-image: url("{T_THEME_PATH}/images/2corners_right.png"); }
div.headerbar span.corners-top2 { background-image: url("{T_THEME_PATH}/images/2corners_left2.png"); }
div.headerbar span.corners-top2 span { background-image: url("{T_THEME_PATH}/images/2corners_right2.png"); }

/* Horizontal lists */

ul.navlinks {
	border-bottom-color: #000;
}

/* Table styles */
table.table1 thead th {
	color: #F9FAF9;
}

table.table1 tbody tr {
	border-color: #000;
}

table.table1 tbody tr:hover, table.table1 tbody tr.hover {
	background-color: #303030;
	color: #F9FAF9;
}

table.table1 td {
	color: #D9D4D4;
}

table.table1 tbody td {
	border-top-color: #000;
}

table.table1 tbody th {
	background-color: #191919;
	border-bottom-color: #000;
	color: #F9FAF9;
}

table.info tbody th {
	color: #C7CFCF;
}

/* Misc layout styles */
dl.details dt {
	color: #D7D4D4;
}

dl.details dd {
	color: #D7D4D4;
}

.sep {
	color: #1198D9;
}

/* Pagination */

.pagination span strong {
	color: #FFFFFF;
}

.pagination span a, .pagination span a:link, .pagination span a:visited {
	background: #000;
	border-color: #000;
	color: #b20304;
}

.pagination span a:hover {
	background: #1F1F1F;
	border-color: #000;
	color: #FFFFFF;
}

.pagination span a:active {
	background: #000;
	border-color: #000;
	color: #b20304;
}

/* Pagination in viewforum for multipage topics */
.row .pagination {
	background-image: url("{T_THEME_PATH}/images/icon_pages.png");
}

.row .pagination span a, li.pagination span a {
	background-color: transparent;
}

.row .pagination span a:hover, li.pagination span a:hover {
	background-color: transparent;
}

/* Miscellaneous styles */

.copyright {
	color: #F9FAF9;
}

.copyright a {
	color: #b20304;
}

.error {
	color: #A80011;
}

.reported,
li.reported:hover {
	background-color: transparent;
}

/* you can add a background for stickies and announcements*/
.sticky {}
.announce {}

div.rules {
	background-color: #2A2A2A;
	color: #D7D4D4;
}

p.rules {
	background-color: #2A2A2A;
	background-image: url("{T_THEME_PATH}/images/p_bg.png");
}

/* Colours and backgrounds for links.css */

a:link, a:focus, a:visited, a:active {
	color: #898989;
}

a:hover {
	color: #B20304;
	text-decoration: none;
}

/* Links on gradient backgrounds */
#search-box a:link, .navbg a:link, .forumbg .header a:link, .forabg .header a:link, th a:link {
	color: #FFF;
}

#search-box a:visited, .navbg a:visited, .forumbg .header a:visited, .forabg .header a:visited, th a:visited {
	color: #FFF;
}

#search-box a:hover, .navbg a:hover, .forumbg .header a:hover, .forabg .header a:hover, th a:hover {
	color: #F9FAF9;
}

#search-box a:active, .navbg a:active, .forumbg .header a:active, .forabg .header a:active, th a:active {
	color: #FFF;
}

/* Links for forum/topic lists */
a.forumtitle {
	color: #D7D7D7;
}

a.forumtitle:visited {
	color: #D7D7D7;
}

a.forumtitle:hover {
	color: #CCC;
}

a.forumtitle:active {
	color: #D7D7D7;
}

a.topictitle {
	color: #D7D7D7;
}

a.topictitle:visited {
	color: #D7D7D7;
}

a.topictitle:hover {
	color: #CCC;
}

a.topictitle:active {
	color: #D7D7D7;
}

/* Post body links */
.postlink, .postlink:active, .postlink:focus, .postlink:visited {
	color: #F9FAF9;
}

.postlink:hover {
	color: #FFF;
	text-decoration: none;
}

.signature a, .signature a:visited, .signature a:hover, .signature a:active {
	background-color: transparent;
}

/* Profile links */
.postprofile a:link, .postprofile a:visited, .postprofile dt.author a {
	color: #D7D7D7;
}

.postprofile a:hover, .postprofile dt.author a:hover {
	color: #C7C7C7;
}

.postprofile a:active {
	color: #D7D7D7;
}

/* Profile searchresults */
.search .postprofile a {
	color: #D7D7D7;
}

.search .postprofile a:hover {
	color: #C7C7C7;
}

/* Back to top of page */
a.top, a.top2	{ background-image: url("{IMG_ICON_BACK_TOP_SRC}"); }
/* Arrow links  */
a.up		{ background-image: url("{T_THEME_PATH}/images/arrow_up.gif"); }
a.down		{ background-image: url("{T_THEME_PATH}/images/arrow_down.gif"); }
a.left		{ background-image: url("{T_THEME_PATH}/images/arrow_left.gif"); }
a.right		{ background-image: url("{T_THEME_PATH}/images/arrow_right.gif"); }
a.up:hover	{ background-color: transparent; }
a.left:hover	{ color: #980000; }
a.right:hover	{ color: #980000; }

/* Colours and backgrounds for content.css */

ul.forums {
	background-color: transparent;
}

ul.topiclist li {
	color: #F9FAF9;
}

ul.topiclist dd {
	border-left-color: #191919;
}

.rtl ul.topiclist dd {
	border-right-color: #191919;
	border-left-color: transparent;
}

ul.topiclist li.row dt a.subforum.read {
	background-image: url("{IMG_SUBFORUM_READ_SRC}");
}

ul.topiclist li.row dt a.subforum.unread {
	background-image: url("{IMG_SUBFORUM_UNREAD_SRC}");
}

li.row {
	background: transparent /* #070707 url("{T_THEME_PATH}/images/n_h.png") repeat-x*/;
	border: 1px solid transparent;
}

li.row:hover {
	background: transparent /* #303030 url("{T_THEME_PATH}/images/n_h2.png") repeat-x*/;
	border: 1px solid transparent;
}

li.row strong {
	color: #F9FAF9;
}

li.row:hover {
	background-color: transparent;
}

/* Row hover effect change or remove here */
li.row:hover dd {
	border-left: 1px solid #333;
}
/*End*/

.rtl li.row:hover dd {
	border-left-color: transparent;
	border-right-color: #FFF;
}

li.header dt, li.header dd {
	color: #FFF;
}

/* Forum list column styles */
ul.topiclist dd.searchextra {
	color: #333;
}

/* Post body styles */
.postbody {
	color: #D7D4D4;
}

/* Content container styles */
.content {
	color: #F9FAF9;
}

.content h2, .panel h2 {
	border-bottom-color:  transparent;
	color: #DFD4D4;
}

dl.faq dt {
	color: #F9FAF9;
}

.posthilit {
	background-color: #F3BFCC;
	color: #BC2A4D;
}

/* Post signature */
.signature {
	border-top-color: #555;
}

/* Post noticies */
.notice {
	border-top-color:  #CCC;
}

/* BB Code styles */
/* Quote block */
blockquote {
	background-color: #191919;
	background-image: url("{T_THEME_PATH}/images/quote.png");
	border-color: #010101;
}

.rtl blockquote {
	background-image: url("{T_THEME_PATH}/images/quote_rtl.gif");
}

/* Nested quotes */
blockquote blockquote {
	background-color: #212121;
}

/* Nested quotes */
blockquote blockquote blockquote {
	background-color: #333;
}

/* Code block */
dl.codebox {
	background-color: #191919;
	border-color: #070708;
}

dl.codebox dt {
	border-bottom-color:  #000;
}

dl.codebox code {
	color: #AA0000;
}

.syntaxbg		{ color: #FFFFFF; }
.syntaxcomment	{ color: #FF8000; }
.syntaxdefault	{ color: #0000BB; }
.syntaxhtml		{ color: #000000; }
.syntaxkeyword	{ color: #007700; }
.syntaxstring	{ color: #DD0000; }

/* Attachments */
.attachbox {
	background-color: #252525;
	border-color:  #000;
}

.pm-message .attachbox {
	background-color: #252525;
}

.attachbox dd {
	border-top-color: #000;
}

.attachbox p {
	color: #C7CCC7;
}

.attachbox p.stats {
	color: #D9D4D4;
}

.attach-image img {
	border-color: #343434;
}

/* Inline image thumbnails */

dl.file dd {
	color: #D9D4D4;
}

dl.thumbnail img {
	background-color: transparent;
	border-color: #000;
}

dl.thumbnail dd {
	color: #666;
}

dl.thumbnail dt a:hover {
	background-color: #030303;
}

dl.thumbnail dt a:hover img {
	border-color: #010101;
}

/* Post poll styles */

fieldset.polls dl {
	border-top-color: transparent;
	color: #C7C3C7;
}

fieldset.polls dl.voted {
	color: #F9FAF9;
}

fieldset.polls dd div {
	color: #FFF;
}

.rtl .pollbar1, .rtl .pollbar2, .rtl .pollbar3, .rtl .pollbar4, .rtl .pollbar5 {
	border-right-color: transparent;
}

.pollbar1	{ background: url("{T_THEME_PATH}/images/pollbg_01.png"); border: 1px solid #000; }
.rtl .pollbar1	{ border: 1px solid #000; }
.pollbar2	{ background: url("{T_THEME_PATH}/images/pollbg_01.png"); border: 1px solid #000; }
.rtl .pollbar2	{ border: 1px solid #000; }
.pollbar3	{ background: url("{T_THEME_PATH}/images/pollbg_01.png"); border: 1px solid #000; }
.rtl .pollbar3	{ border: 1px solid #000; }
.pollbar4	{ background: url("{T_THEME_PATH}/images/pollbg_01.png"); border: 1px solid #000; }
.rtl .pollbar4	{ border: 1px solid #000; }
.pollbar5	{ background: url("{T_THEME_PATH}/images/pollbg_01.png"); border: 1px solid #000; }
.rtl .pollbar5	{ border: 1px solid #000; }

/* Poster profile block */
.postprofile {
	border-bottom-color: #666;
	color: #CCC;
}

.rtl .postprofile {
	border-left-color: transparent;
	border-right-color: #FFF;
}

.pm .postprofile {
	border-left-color: transparent;
}

.rtl .pm .postprofile {
	border-left-color: transparent;
	border-right-color: #CCC;
}

.postprofile strong {
	color: #FFF;
}

.online {
	background-image: url("{IMG_ICON_USER_ONLINE_SRC}");
	background-position: top right;
	background-repeat: no-repeat;
	z-index: 999;
}

/* Colours and backgrounds for buttons.css */

/* Big button images */
.reply-icon span	{ background-image: url("{IMG_BUTTON_TOPIC_REPLY_SRC}"); }
.post-icon span		{ background-image: url("{IMG_BUTTON_TOPIC_NEW_SRC}"); }
.locked-icon span	{ background-image: url("{IMG_BUTTON_TOPIC_LOCKED_SRC}"); }
.pmreply-icon span	{ background-image: url("{IMG_BUTTON_PM_REPLY_SRC}"); }
.newpm-icon span	{ background-image: url("{IMG_BUTTON_PM_NEW_SRC}"); }
.forwardpm-icon span	{ background-image: url("{IMG_BUTTON_PM_FORWARD_SRC}"); }

a.print 		{ background-image: url("{T_THEME_PATH}/images/icon_print.png"); }
a.sendemail 		{ background-image: url("{T_THEME_PATH}/images/icon_sendemail.png"); }
a.fontsize		{ background-image: url("{T_THEME_PATH}/images/icon_fontsize.png"); }

/* Icon images */
.sitehome						{ background-image: url("{T_THEME_PATH}/images/icon_home.png"); }
.icon-faq						{ background-image: url("{T_THEME_PATH}/images/icon_faq.png"); }
.icon-members					{ background-image: url("{T_THEME_PATH}/images/icon_members.png"); }
.icon-home						{ background-image: url("{T_THEME_PATH}/images/icon_home.png"); }
.icon-ucp						{ background-image: url("{T_THEME_PATH}/images/icon_ucp.png"); }
.icon-register					{ background-image: url("{T_THEME_PATH}/images/icon_register.png"); }
.icon-logout					{ background-image: url("{T_THEME_PATH}/images/icon_logout.png"); }
.icon-bookmark					{ background-image: url("{T_THEME_PATH}/images/icon_bookmark.png"); }
.icon-bump						{ background-image: url("{T_THEME_PATH}/images/icon_bump.png"); }
.icon-subscribe					{ background-image: url("{T_THEME_PATH}/images/icon_subscribe.png"); }
.icon-unsubscribe				{ background-image: url("{T_THEME_PATH}/images/icon_unsubscribe.png"); }
.icon-pages						{ background-image: url("{T_THEME_PATH}/images/icon_pages.png"); }
.icon-search					{ background-image: url("{T_THEME_PATH}/images/icon_search.png"); }

/* Profile & navigation icons */
.email-icon, .email-icon a		{ background-image: url("{IMG_ICON_CONTACT_EMAIL_SRC}"); }
.aim-icon, .aim-icon a			{ background-image: url("{IMG_ICON_CONTACT_AIM_SRC}"); }
.yahoo-icon, .yahoo-icon a		{ background-image: url("{IMG_ICON_CONTACT_YAHOO_SRC}"); }
.web-icon, .web-icon a			{ background-image: url("{IMG_ICON_CONTACT_WWW_SRC}"); }
.msnm-icon, .msnm-icon a			{ background-image: url("{IMG_ICON_CONTACT_MSNM_SRC}"); }
.icq-icon, .icq-icon a			{ background-image: url("{IMG_ICON_CONTACT_ICQ_SRC}"); }
.jabber-icon, .jabber-icon a		{ background-image: url("{IMG_ICON_CONTACT_JABBER_SRC}"); }
.pm-icon, .pm-icon a				{ background-image: url("{IMG_ICON_CONTACT_PM_SRC}"); }
.quote-icon, .quote-icon a		{ background-image: url("{IMG_ICON_POST_QUOTE_SRC}"); }

/* Moderator icons */
.report-icon, .report-icon a		{ background-image: url("{IMG_ICON_POST_REPORT_SRC}"); }
.edit-icon, .edit-icon a			{ background-image: url("{IMG_ICON_POST_EDIT_SRC}"); }
.delete-icon, .delete-icon a		{ background-image: url("{IMG_ICON_POST_DELETE_SRC}"); }
.info-icon, .info-icon a			{ background-image: url("{IMG_ICON_POST_INFO_SRC}"); }
.warn-icon, .warn-icon a			{ background-image: url("{IMG_ICON_USER_WARN_SRC}"); }

/* Colours and backgrounds for cp.css */
/* Main CP box */

#cp-main h3, #cp-main hr, #cp-menu hr {
	border-color: #000;
}

#cp-main .panel li.row {
	border-bottom-color: #000;
	border-top-color: #000;
}

ul.cplist {
	border-top-color: #000;
}

#cp-main .panel li.header dd, #cp-main .panel li.header dt {
	color: #D7D4D4;
}

#cp-main table.table1 thead th {
	color: #D7D4D4;
	border-bottom-color: #000;
}

#cp-main .pm-message {
	border-color: #000;
	background-color: #303030;
}

/* CP tabbed menu*/
#tabs a {
	background-image: url("{T_THEME_PATH}/images/bg_tabs1.png");
}

#tabs a span {
	background-image: url("{T_THEME_PATH}/images/bg_tabs2.png");
	color: #b20304;
}

#tabs a:hover span {
	color: #CCC;
}

#tabs .activetab a {
	border-bottom-color: #000;
}

#tabs .activetab a span {
	color: #b20304;
}

#tabs .activetab a:hover span {
	color: #CCC;
}

/* Mini tabbed menu used in MCP */
#minitabs li {
	background-color: #191919;
}

#minitabs li.activetab {
	background-color: #191919;
}

#minitabs li.activetab a, #minitabs li.activetab a:hover {
	color: #b20304;
}

/* UCP navigation menu */

/* Link styles for the sub-section links */
#navigation a {
	background-color: #191919;
	/*background-image: url("{T_THEME_PATH}/images/bg_menu.png");*/
	color: #F9FAF9;
}

.rtl #navigation a {
	background-image: url("{T_THEME_PATH}/images/bg_menu_rtl.png");
	background-position: 0 100%;
}

#navigation a:hover {
	background-color: #333;
	background-image: none;
	color: #D7D4D4;
}

#navigation #active-subsection a {
	background-color: #212121;
	color: #b20304;
}

#navigation #active-subsection a:hover {
	color: #D7D4D4;
}

/* Preferences pane layout */
#cp-main h2 {
	color: #D7D4D4;
}

#cp-main .panel {
	background-color: #191919;
}

#cp-main .pm {
	background-color: #191919;
}

#cp-main span.corners-top, #cp-menu span.corners-top {
	background-image: url("{T_THEME_PATH}/images/corners_left2.png");
}

#cp-main span.corners-top span, #cp-menu span.corners-top span {
	background-image: url("{T_THEME_PATH}/images/corners_right2.png");
}

#cp-main span.corners-bottom, #cp-menu span.corners-bottom {
	background-image: url("{T_THEME_PATH}/images/corners_left2.png");
}

#cp-main span.corners-bottom span, #cp-menu span.corners-bottom span {
	background-image: url("{T_THEME_PATH}/images/corners_right2.png");
}

/* Topicreview */
#cp-main .panel #topicreview span.corners-top, #cp-menu .panel #topicreview span.corners-top {
	background-image: url("{T_THEME_PATH}/images/corners_left.png");
}

#cp-main .panel #topicreview span.corners-top span, #cp-menu .panel #topicreview span.corners-top span {
	background-image: url("{T_THEME_PATH}/images/corners_right.png");
}

#cp-main .panel #topicreview span.corners-bottom, #cp-menu .panel #topicreview span.corners-bottom {
	background-image: url("{T_THEME_PATH}/images/corners_left.png");
}

#cp-main .panel #topicreview span.corners-bottom span, #cp-menu .panel #topicreview span.corners-bottom span {
	background-image: url("{T_THEME_PATH}/images/corners_right.png");
}

/* Friends list */
.cp-mini {
	background-color: #191919;
}

dl.mini dt {
	color: #F9FAF9;
}

/* PM Styles */
/* PM Message history */
.current {
	color: #000000;
}

/* PM marking colours */
.pmlist li.pm_message_reported_colour, .pm_message_reported_colour {
	border-left-color: #000;
	border-right-color: #000;
}

.pmlist li.pm_marked_colour, .pm_marked_colour {
	border-color: #000;
}

.pmlist li.pm_replied_colour, .pm_replied_colour {
	border-color: #222;
}

.pmlist li.pm_friend_colour, .pm_friend_colour {
	border-color: #565656;
}

.pmlist li.pm_foe_colour, .pm_foe_colour {
	border-color: #980000;
}

/* Avatar gallery */
#gallery label {
	background-color: #191919;
	border-color: #000;
}

#gallery label:hover {
	background-color: #2A2A2A;
}

/* Colours and backgrounds for forms.css */
/* General form styles */
select {
	border-color: #666;
	background-color: #FAFAFA;
	color: #000;
}

label {
	color: #898989;
	text-shadow: -1px -1px 2px rgba(0,0,0,0.8);
}

option.disabled-option {
	color: graytext;
	text-decoration: line-through;
}

/* Definition list layout for forms */
dd label {
	color: #D9D4D4;
}

/* Hover effects */
fieldset dl:hover dt label {
	color: #D9D4D4;
	text-shadow: -1px -1px 2px rgba(0,0,0,0.8);
}

fieldset.fields2 dl:hover dt label {
	color: inherit;
}

/* Quick-login on index page */
fieldset.quick-login input.inputbox {
	background-color: #F2F3F3;
}

/* Posting page styles */

#message-box textarea {
	color: #333;
}

/* Input field styles */
.inputbox {
	background-color: #FFF;
	border-color: #2a2a2a;
	color: #333;
}

.inputbox:hover {
	border-color: #121212;
}

.inputbox:focus {
	border-color: #212121;
	color: #666;
}

/* Form button styles */

a.button1, input.button1, a.button2, input.button2, input.button3 {
	background: #2A2A2A/*  url("{T_THEME_PATH}/images/bg_button.gif") */;
	color: #F9FAF9;
	border-radius: 5px;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
}

a.button1, input.button1 {
	border-color: #191919;
}

input.button3 {
	background-image: none;
}

/* Alternative button */
a.button2, input.button2, input.button3 {
	border-color: #191919;
}

/* <a> button in the style of the form buttons */
a.button1, a.button1:link, a.button1:visited, a.button1:active, a.button2, a.button2:link, a.button2:visited, a.button2:active {
	color: #F9FAF9;
	outline: none;
}

/* Hover states */
a.button1:hover, input.button1:hover, a.button2:hover, input.button2:hover, input.button3:hover {
	border-color: #000;
	color: #F9FAF9;
}

input.search {
	background-image: url("{T_THEME_PATH}/images/icon_textbox_search.png");
}

input.disabled {
	color: #666;
}";s:10:"theme_path";s:8:"proDVGFX";s:10:"theme_name";s:8:"proDVGFX";s:11:"theme_mtime";s:10:"1406474256";s:11:"imageset_id";s:1:"2";s:13:"imageset_name";s:8:"proDVGFX";s:18:"imageset_copyright";s:21:"&copy; Prosk8er, 2013";s:13:"imageset_path";s:8:"proDVGFX";s:13:"template_path";s:8:"proDVGFX";}}