<?php if (!defined('IN_PHPBB')) exit; $this->_tpl_include('overall_header.html'); ?>


<script type="text/javascript">
// <![CDATA[

/**
 * The following javascripts are derived from phpMyAdmin's methods
 */

/**
 * This array is used to remember mark status of rows in browse mode
 */
var marked_row = new Array;

/**
 * enables highlight and marking of rows in data tables
 *
 */
function mark_rows_init()
{
	// for every table row ...
	var rows = document.getElementsByTagName('tr');
	for ( var i = 0; i < rows.length; i++ )
	{
		// ... with the class 'odd' or 'even' ...
		if ( 'row1' != rows[i].className.substr(0,4) && 'row2' != rows[i].className.substr(0,4) )
		{
			continue;
		}
		// ... add event listeners ...
		// ... to highlight the row on mouseover ...
		rows[i].onmouseover = function() {
			this.className += ' row3';
		}
		rows[i].onmouseout = function() {
			this.className = this.className.replace( ' row3', '' );
		}
		// ... and to mark the row on click ...
		rows[i].onmousedown = function() {
			var unique_id;
			var checkbox;

			checkbox = this.getElementsByTagName( 'input' )[0];
			if ( checkbox && checkbox.type == 'checkbox' )
			{
				unique_id = checkbox.name + checkbox.value;
			}
			else if ( this.id.length > 0 )
			{
				unique_id = this.id;
			}
			else
			{
				return;
			}

			if ( typeof(marked_row[unique_id]) == 'undefined' || !marked_row[unique_id] )
			{
				marked_row[unique_id] = true;
			}
			else
			{
				marked_row[unique_id] = false;
			}

			if ( checkbox && checkbox.disabled == false )
			{
				checkbox.checked = marked_row[unique_id];
			}
		}

		// .. and checkbox clicks
		var checkbox = rows[i].getElementsByTagName('input')[0];
		if ( checkbox )
		{
			checkbox.onclick = function() {
				// opera does not recognize return false;
				this.checked = ! this.checked;
			}
		}
	}
}

window.onload = mark_rows_init;

/**
 * We must use phpMyAdmin derived mark all, unmark all rows 
 * for compatability with the table row clicking function above
 */

function mark_all_rows(container_id, overhead)
{
	var rows = (!overhead) ? document.getElementById(container_id).getElementsByTagName('tr') : document.getElementById(container_id).getElementsByClassName('overhead');

	var unique_id;
	var checkbox;

	for ( var i = 0; i < rows.length; i++ )
	{
		checkbox = rows[i].getElementsByTagName( 'input' )[0];

		if ( checkbox && checkbox.type == 'checkbox' )
		{
			unique_id = checkbox.name + checkbox.value;
			if ( checkbox.disabled == false )
			{
				checkbox.checked = true;
				if ( typeof(marked_row[unique_id]) == 'undefined' || !marked_row[unique_id] )
				{
					marked_row[unique_id] = true;
				}
			}
		}
	}

	return true;
}

function unmark_all_rows(container_id)
{
	var rows = document.getElementById(container_id).getElementsByTagName('tr');
	var unique_id;
	var checkbox;

	for ( var i = 0; i < rows.length; i++ )
	{
		checkbox = rows[i].getElementsByTagName( 'input' )[0];

		if ( checkbox && checkbox.type == 'checkbox' )
		{
			unique_id = checkbox.name + checkbox.value;
			checkbox.checked = false;
			marked_row[unique_id] = false;
		}
	}

	return true;
}

// ]]>
</script>

<a name="maincontent"></a>

	<h1><?php echo ((isset($this->_rootref['L_ACP_DATABASE_OR'])) ? $this->_rootref['L_ACP_DATABASE_OR'] : ((isset($user->lang['ACP_DATABASE_OR'])) ? $user->lang['ACP_DATABASE_OR'] : '{ ACP_DATABASE_OR }')); ?></h1>

	<p><?php echo ((isset($this->_rootref['L_ACP_DATABASE_OR_EXPLAIN'])) ? $this->_rootref['L_ACP_DATABASE_OR_EXPLAIN'] : ((isset($user->lang['ACP_DATABASE_OR_EXPLAIN'])) ? $user->lang['ACP_DATABASE_OR_EXPLAIN'] : '{ ACP_DATABASE_OR_EXPLAIN }')); ?></p>

	<div class="errorbox">
		<h3><?php echo ((isset($this->_rootref['L_WARNING'])) ? $this->_rootref['L_WARNING'] : ((isset($user->lang['WARNING'])) ? $user->lang['WARNING'] : '{ WARNING }')); ?></h3>
		<p><?php echo ((isset($this->_rootref['L_WARNING_EXPLAIN'])) ? $this->_rootref['L_WARNING_EXPLAIN'] : ((isset($user->lang['WARNING_EXPLAIN'])) ? $user->lang['WARNING_EXPLAIN'] : '{ WARNING_EXPLAIN }')); ?></p>
	</div>

	<form id="tables" method="post" action="<?php echo (isset($this->_rootref['U_ACTION'])) ? $this->_rootref['U_ACTION'] : ''; ?>">

	<fieldset>
		<legend><?php echo ((isset($this->_rootref['L_OR_OPTIONS'])) ? $this->_rootref['L_OR_OPTIONS'] : ((isset($user->lang['OR_OPTIONS'])) ? $user->lang['OR_OPTIONS'] : '{ OR_OPTIONS }')); ?></legend>
	<dl>
		<dt><label for="type"><?php echo ((isset($this->_rootref['L_DISABLE_BOARD'])) ? $this->_rootref['L_DISABLE_BOARD'] : ((isset($user->lang['DISABLE_BOARD'])) ? $user->lang['DISABLE_BOARD'] : '{ DISABLE_BOARD }')); ?>:</label><br /><span><?php echo ((isset($this->_rootref['L_DISABLE_BOARD_EXPLAIN'])) ? $this->_rootref['L_DISABLE_BOARD_EXPLAIN'] : ((isset($user->lang['DISABLE_BOARD_EXPLAIN'])) ? $user->lang['DISABLE_BOARD_EXPLAIN'] : '{ DISABLE_BOARD_EXPLAIN }')); ?></span></dt>
		<dd>
			<label><input type="radio" class="radio" name="disable_board" value="1" checked="checked" /> <?php echo ((isset($this->_rootref['L_YES'])) ? $this->_rootref['L_YES'] : ((isset($user->lang['YES'])) ? $user->lang['YES'] : '{ YES }')); ?></label>
			<label><input type="radio" class="radio" name="disable_board" value="0" /> <?php echo ((isset($this->_rootref['L_NO'])) ? $this->_rootref['L_NO'] : ((isset($user->lang['NO'])) ? $user->lang['NO'] : '{ NO }')); ?></label>
		</dd>
	</dl>
	<dl>
		<dt><label for="type"><?php echo ((isset($this->_rootref['L_ACTION'])) ? $this->_rootref['L_ACTION'] : ((isset($user->lang['ACTION'])) ? $user->lang['ACTION'] : '{ ACTION }')); ?>:</label></dt>
		<dd>
			<label><input id="type" type="radio" class="radio" name="type" value="optimize" checked="checked" /> <?php echo ((isset($this->_rootref['L_OPTIMIZE'])) ? $this->_rootref['L_OPTIMIZE'] : ((isset($user->lang['OPTIMIZE'])) ? $user->lang['OPTIMIZE'] : '{ OPTIMIZE }')); ?></label>
			<label><input type="radio" class="radio" name="type" value="repair" /> <?php echo ((isset($this->_rootref['L_REPAIR'])) ? $this->_rootref['L_REPAIR'] : ((isset($user->lang['REPAIR'])) ? $user->lang['REPAIR'] : '{ REPAIR }')); ?></label>
			<label><input type="radio" class="radio" name="type" value="check" /> <?php echo ((isset($this->_rootref['L_CHECK'])) ? $this->_rootref['L_CHECK'] : ((isset($user->lang['CHECK'])) ? $user->lang['CHECK'] : '{ CHECK }')); ?></label>
		</dd>
	</dl>

	<table cellspacing="1" id="table_data">
	<thead>
	<tr>
		<th><?php echo ((isset($this->_rootref['L_TH_NAME'])) ? $this->_rootref['L_TH_NAME'] : ((isset($user->lang['TH_NAME'])) ? $user->lang['TH_NAME'] : '{ TH_NAME }')); ?></th>
		<th><?php echo ((isset($this->_rootref['L_TH_TYPE'])) ? $this->_rootref['L_TH_TYPE'] : ((isset($user->lang['TH_TYPE'])) ? $user->lang['TH_TYPE'] : '{ TH_TYPE }')); ?></th>
		<th style="text-align:right;"><?php echo ((isset($this->_rootref['L_TH_SIZE'])) ? $this->_rootref['L_TH_SIZE'] : ((isset($user->lang['TH_SIZE'])) ? $user->lang['TH_SIZE'] : '{ TH_SIZE }')); ?></th>
		<th style="text-align:right;"><?php echo ((isset($this->_rootref['L_TH_OVERHEAD'])) ? $this->_rootref['L_TH_OVERHEAD'] : ((isset($user->lang['TH_OVERHEAD'])) ? $user->lang['TH_OVERHEAD'] : '{ TH_OVERHEAD }')); ?></th>
		<th style="text-align:center;"><?php echo ((isset($this->_rootref['L_MARK'])) ? $this->_rootref['L_MARK'] : ((isset($user->lang['MARK'])) ? $user->lang['MARK'] : '{ MARK }')); ?></th>
	</tr>
	</thead>
	<tbody>
<?php if (sizeof($this->_tpldata['tables'])) {  $_tables_count = (isset($this->_tpldata['tables'])) ? sizeof($this->_tpldata['tables']) : 0;if ($_tables_count) {for ($_tables_i = 0; $_tables_i < $_tables_count; ++$_tables_i){$_tables_val = &$this->_tpldata['tables'][$_tables_i]; if (!($_tables_val['S_ROW_COUNT'] & 1)  ) {  ?><tr class="row1<?php echo $_tables_val['HAS_OVERHEAD']; ?>"><?php } else { ?><tr class="row2<?php echo $_tables_val['HAS_OVERHEAD']; ?>"><?php } ?>

		<td><?php echo $_tables_val['TABLE_NAME']; ?></td>
		<td><?php echo $_tables_val['TABLE_TYPE']; ?></td>
		<td style="text-align:right;"><?php echo $_tables_val['DATA_SIZE']; ?></td>
		<td style="text-align:right;"><?php echo $_tables_val['DATA_FREE']; ?></td>
		<td style="text-align:center;">&nbsp;<input type="checkbox" class="radio" name="mark[]" value="<?php echo $_tables_val['TABLE_NAME']; ?>" id="<?php echo $_tables_val['TABLE_NAME']; ?>" />&nbsp;</td>
	</tr>
	<?php }} ?>

	<tr>
		<th><?php echo ((isset($this->_rootref['L_TH_TOTAL'])) ? $this->_rootref['L_TH_TOTAL'] : ((isset($user->lang['TH_TOTAL'])) ? $user->lang['TH_TOTAL'] : '{ TH_TOTAL }')); ?></th>
		<th>&nbsp;</th>
		<th style="text-align:right;"><?php echo (isset($this->_rootref['TOTAL_DATA_SIZE'])) ? $this->_rootref['TOTAL_DATA_SIZE'] : ''; ?></th>
		<th style="text-align:right;"><?php echo (isset($this->_rootref['TOTAL_DATA_FREE'])) ? $this->_rootref['TOTAL_DATA_FREE'] : ''; ?></th>
		<th>&nbsp;</th>
	</tr>
<?php } else { ?>

	<tr><td colspan="5"><?php echo ((isset($this->_rootref['L_TABLE_EMPTY'])) ? $this->_rootref['L_TABLE_EMPTY'] : ((isset($user->lang['TABLE_EMPTY'])) ? $user->lang['TABLE_EMPTY'] : '{ TABLE_EMPTY }')); ?></td></tr>
<?php } ?>

	</tbody>
	</table>

	<fieldset class="quick">
		<p class="small"><a href="#" onclick="mark_all_rows('table_data', false); return false;"><?php echo ((isset($this->_rootref['L_MARK_ALL'])) ? $this->_rootref['L_MARK_ALL'] : ((isset($user->lang['MARK_ALL'])) ? $user->lang['MARK_ALL'] : '{ MARK_ALL }')); ?></a> &bull; <a href="#" onclick="unmark_all_rows('table_data'); return false;"><?php echo ((isset($this->_rootref['L_UNMARK_ALL'])) ? $this->_rootref['L_UNMARK_ALL'] : ((isset($user->lang['UNMARK_ALL'])) ? $user->lang['UNMARK_ALL'] : '{ UNMARK_ALL }')); ?></a><?php if ($this->_rootref['TOTAL_DATA_FREE'] != ('0 B')) {  ?> &bull; <a href="#" onclick="unmark_all_rows('table_data'); mark_all_rows('table_data', true); return false;"><?php echo ((isset($this->_rootref['L_MARK_OVERHEAD'])) ? $this->_rootref['L_MARK_OVERHEAD'] : ((isset($user->lang['MARK_OVERHEAD'])) ? $user->lang['MARK_OVERHEAD'] : '{ MARK_OVERHEAD }')); ?></a><?php } ?></p>
	</fieldset>

	<p id="buttons" class="submit-buttons">
		<input class="button1" type="submit" id="submit" name="submit" value="<?php echo ((isset($this->_rootref['L_SUBMIT'])) ? $this->_rootref['L_SUBMIT'] : ((isset($user->lang['SUBMIT'])) ? $user->lang['SUBMIT'] : '{ SUBMIT }')); ?>" onclick="JavaScript:document.getElementById('buttons').style.display='none';document.getElementById('processing').style.display='block';" />&nbsp;
		<input class="button2" type="reset" id="reset" name="reset" value="<?php echo ((isset($this->_rootref['L_RESET'])) ? $this->_rootref['L_RESET'] : ((isset($user->lang['RESET'])) ? $user->lang['RESET'] : '{ RESET }')); ?>" />
	</p>
	<p id="processing" class="submit-buttons" style="display:none;">
		<input class="button1" type="button" value="<?php echo ((isset($this->_rootref['L_PROCESSING'])) ? $this->_rootref['L_PROCESSING'] : ((isset($user->lang['PROCESSING'])) ? $user->lang['PROCESSING'] : '{ PROCESSING }')); ?>" disabled="disabled" />
	</p>
	<?php echo (isset($this->_rootref['S_FORM_TOKEN'])) ? $this->_rootref['S_FORM_TOKEN'] : ''; ?>


	</fieldset>
	</form>

<?php $this->_tpl_include('overall_footer.html'); ?>