<?php if (!defined('IN_PHPBB')) exit; if ($this->_rootref['S_KEYCAPTCHA_AVAILABLE']) {  ?>

	<table class="KeyCAPTCHA_def_tpl">
		<tr class="KeyCAPTCHA_def_tpl">
			<b class="KeyCAPTCHA_def_tpl"><?php echo ((isset($this->_rootref['L_KEYCAPTCHA_TASK_HEADER'])) ? $this->_rootref['L_KEYCAPTCHA_TASK_HEADER'] : ((isset($user->lang['KEYCAPTCHA_TASK_HEADER'])) ? $user->lang['KEYCAPTCHA_TASK_HEADER'] : '{ KEYCAPTCHA_TASK_HEADER }')); ?>: &nbsp; <?php echo ((isset($this->_rootref['L_KEYCAPTCHA_TASK_EXPLAIN'])) ? $this->_rootref['L_KEYCAPTCHA_TASK_EXPLAIN'] : ((isset($user->lang['KEYCAPTCHA_TASK_EXPLAIN'])) ? $user->lang['KEYCAPTCHA_TASK_EXPLAIN'] : '{ KEYCAPTCHA_TASK_EXPLAIN }')); ?></b>
		</tr>
		<tr class="KeyCAPTCHA_def_tpl">
			<?php echo (isset($this->_rootref['KEYCAPTCHA_CODE'])) ? $this->_rootref['KEYCAPTCHA_CODE'] : ''; ?>

		</tr>
	</table class="KeyCAPTCHA_def_tpl">
	<input type="hidden" name="kc_response_field" id="kc_response_field" value="false" />
	<noscript>
		<?php echo ((isset($this->_rootref['L_KEYCAPTCHA_MESSAGE_NOSCRIPT'])) ? $this->_rootref['L_KEYCAPTCHA_MESSAGE_NOSCRIPT'] : ((isset($user->lang['KEYCAPTCHA_MESSAGE_NOSCRIPT'])) ? $user->lang['KEYCAPTCHA_MESSAGE_NOSCRIPT'] : '{ KEYCAPTCHA_MESSAGE_NOSCRIPT }')); ?>

	</noscript>
<?php } else { ?>

	<?php echo ((isset($this->_rootref['L_KEYCAPTCHA_MESSAGE_NA'])) ? $this->_rootref['L_KEYCAPTCHA_MESSAGE_NA'] : ((isset($user->lang['KEYCAPTCHA_MESSAGE_NA'])) ? $user->lang['KEYCAPTCHA_MESSAGE_NA'] : '{ KEYCAPTCHA_MESSAGE_NA }')); ?>

<?php } ?>