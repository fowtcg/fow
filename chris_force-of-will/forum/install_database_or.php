<?php
/**
*
* @package Database Optimize & Repair Tool
* @version $Id$
* @copyright (c) 2010 Matt Friedman
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
define('UMIL_AUTO', true);
define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);

$user->session_begin();
$auth->acl($user->data);
$user->setup();

if (!file_exists($phpbb_root_path . 'umil/umil_auto.' . $phpEx))
{
	trigger_error('Please download the latest UMIL (Unified MOD Install Library) from: <a href="http://www.phpbb.com/mods/umil/">phpBB.com/mods/umil</a>', E_USER_ERROR);
}

$mod_name = 'UMIL_DATABASE_OR';
$version_config_name = 'database_or_version';
$language_file = 'mods/info_acp_database_or';

$versions = array(
	// Version 0.0.1
	'0.0.1'	=> array(
		// Add the Optimize & Repair module to the ACP -> Maintenance -> Database section
		'module_add' => array(
			array('acp', 'ACP_CAT_DATABASE', array(
					'module_basename'	=> 'database_or',
					'module_langname'	=> 'ACP_DATABASE_OR',
					'module_mode'		=> 'view',
					'module_auth'		=> 'acl_a_backup',
					'after'				=> 'ACP_RESTORE', // Will be placed after ACP_RESTORE
				),
			),
		),

		// purge the cache
		'cache_purge' => array(),
	),

	// Version 0.0.2
	'0.0.2'	=> array(),

	// Version 0.0.3
	'0.0.3'	=> array(),

	// Version 0.0.4
	'0.0.4'	=> array(),

	// Version 0.0.5
	'0.0.5'	=> array(),

	// Version 0.0.6
	'0.0.6'	=> array(),

	// Version 0.0.7
	'0.0.7'	=> array(),

	// Version 0.0.8
	'0.0.8'	=> array(),

	// Version 1.0.0
	'1.0.0'	=> array(),

	// Version 1.0.1
	'1.0.1'	=> array(),

	// Version 1.0.2
	'1.0.2'	=> array(),

);

include($phpbb_root_path . 'umil/umil_auto.' . $phpEx);

?>