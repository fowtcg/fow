<?php
/**
*
* info_acp_database_or [Polish]
*
* @package Database Optimize & Repair Tool
* @version $Id$
* @copyright (c) 2010 Matt Friedman
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'UMIL_DATABASE_OR'			=> 'Narzędzie Optymalizacja &amp; Naprawa Bazy Danych',
	'ACP_DATABASE_OR'			=> 'Optymalizacja &amp; Naprawa',
	'ACP_DATABASE_OR_EXPLAIN'	=> 'Tutaj możesz optymalizować i naprawiać powiązane tabele phpBB. W przypadku dużych baz danych, może to potrwać kilka minut. <strong>OPTYMALIZACJA</strong> będzie defragmentować Twoją bazę danych i może zaoferować jej wyższą wydajność. <strong>NAPRAWA</strong> powinna być stosowana tylko gdy masz powody sądzić, że baza danych uległa awarii lub ma uszkodzone tabele. Uwaga: tabele <strong> InnoDB </ strong> nie obsługują napraw.',
	'OR_OPTIONS'				=> 'Opcje dostępu do bazy',
	'DISABLE_BOARD'				=> 'Wyłącz forum',
	'DISABLE_BOARD_EXPLAIN'		=> 'Powinieneś wyłączyć forum w trakcie tego procesu. Forum będzie dostępne pod koniec procesu.',
	'OPTIMIZE'			=> 'Optymalizacja',
	'OPTIMIZE_SUCCESS'	=> 'Optymalizacja wybranej (ych) tabeli jest zakończona.',
	'OPTIMIZE_LOG'		=> '<strong>Tabele bazy danych zoptymalizowane</strong><br />» %s',
	'REPAIR'			=> 'Naprawa',
	'REPAIR_SUCCESS'	=> 'Naprawa wybranej (ych) tabeli jest zakończona.',
	'REPAIR_LOG'		=> '<strong>Tabele bazy danych naprawione</strong><br />» %s',
	'CHECK'				=> 'Sprawdź',
	'CHECK_SUCCESS'		=> 'Sprawdzanie zakończone.<br />Jeśli nie masz statusu "OK ", lub "Tabela jest już aktualna" zazwyczaj należy przeprowadzić naprawę tabeli.',
	'WARNING'			=> 'Ostrzeżenie',
	'WARNING_EXPLAIN'	=> 'Narzędzie to jest BEZ GWARANCJI i użytkownicy tego narzędzia, w przypadku awarii, powinni wykonać kopię zapasową całej bazy danych.<br /><br />Before continuing, make sure you have a database backup!',
	'WARNING_MYSQL'		=> 'Ta funkcja działa tylko z bazami danych MySQL.',
	'MARK_OVERHEAD'		=> 'Zaznacz tabelę uwzględniając przeciążenie',
	'PROCESSING'		=> 'Przetwarzania żądania ... Proszę czekać ...',
	'TH_NAME'			=> 'Nazwa tabeli',
	'TH_TYPE'			=> 'Rodzaj',
	'TH_SIZE'			=> 'Rozmiar',
	'TH_TOTAL'			=> 'Podsumowania',
	'TH_OVERHEAD'		=> 'Przeciążenie',
	'TABLE_ERROR'		=> 'Musisz wybrać co najmniej jedną tabelę.',
	'TABLE_EMPTY'		=> 'Silnik przechowywania tabeli nie jest obsługiwany.',
));

?>