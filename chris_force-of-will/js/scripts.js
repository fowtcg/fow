window.onload = function() {
  document.getElementById('mainmenu').onmousedown = function() {
    showHideMenus(document.getElementById('main'));    
  }
  if (null != document.getElementById('submenu')) {
    document.getElementById('submenu').onmousedown = function() {
      showHideMenus(document.getElementById('sub'));    
    }
  }
  if(null != document.getElementById('newsmenu')) {
    document.getElementById('newsmenu').onmousedown = function() {
      showHideMenus(document.getElementById('news'));    
    }
  }
}  

function showHideMenus(ele) {
  if ('block' == ele.style.display) {
    ele.style.display = 'none';
  }
  else {
    ele.style.display = 'block';
  }
}